/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.lang.Double;


/**
 * This class constains the main method hpcc(), which returns the final
 * classification and the list of total variance at each merge (which aids
 * determination of the correct number of clusters without use of a penalty
 * function).
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class HPCC {

	/** Some parameter defaults (Java lacks this property) */
	public static final int DEFAULT_DEGREES_OF_FREEDOM = 5;
	/** Some parameter defaults (Java lacks this property) */
	public static final double DEFAULT_ALPHA = 0.4;
	/** Some parameter defaults (Java lacks this property) */
	public static final boolean DEFAULT_FORCE = false;
	/** Some parameter defaults (Java lacks this property) */
	public static final int DEFAULT_FORCENUM = 1;

	/** Parameter to control whether to output trace information or not */
	public static boolean TRACE = false;


	/** Show trace information
	 *
	 * @param message The message to show
	 */
	public static void trace(String function, String message) {
		if (TRACE) {
			Logger.DEBUG("HPCC." + function, message);
		}
	}


	/** Result of the computation of HPCC */
	public static class Result {
		/** Result value */
		public double[][] x;
		/** Result value */
		public double[] totalVarTrack;
		/** Result value */
		public int[] classes;

		/** Default constructor */
		public Result() { }

		/** Constructor with initializers */
		public Result(double[][] x, double[] totalVarTrac, int[] classes) {
			this.x = x;
			this.totalVarTrack = totalVarTrack;
			this.classes = classes;
		}
	}


	/** A penalty can be specified here.
	 *
	 * @param k is the number of clusters.
	 *
	 * @return The penalty
	 */
	public static double penalty(double k) {
		double total = 0.0;
		return total;
	}

	/** Distance function.
	 *
	 * @param a Point a to calculate the distance
	 * @param b Point b to calculate the distance
	 *
	 * @return The distance
	 */
	public static double vdist(double[][] a, double[][] b) {
		if (a.length != b.length
		    || a.length < 1
		    || b.length < 1
		    || a[0].length != b[0].length) {
			throw new RuntimeException("HPPC.vdist(): given arrays have different sizes");
		}

		double total = 0.0;
		for (int i = 0; i < a.length; ++i) {
			for (int j = 0; j < a[i].length; ++j) {
				total += Util.power2(a[i][j] - b[i][j]);
			}
		}
		return total;
	}

	/** Spline function.
	 *
	 * @param x Set of points
	 * @param dg Degrees of freedom (same as hpcc())
	 * @param alpha Modification parameter (same as hpcc())
	 *
	 * @return The value of the calculation
	 */
	public static double clustVarSpline(double[][] x, int dg /* =5 */, double alpha /* =0.4 */) {
		double total = 0.0;
		double vabout = 0.0;
		double valong = 0.0;

		// if there are fewer than 7 points in a cluster, fit a
		// principal component line instead of a principal curve
		//
		// mafm: not implemented at the moment since apparently it's not
		// needed
		if (x.length < 7) {
			throw new RuntimeException("HPCC.clustVarSpline(): branch not expected");

			/*
			  #Added by LSB 15/11/2007 See below
			    p<-length(x[1,])

			  #This is the function which calls principal.curve
			  #
			  #If plot.me is T, then the iterative plots of the principal curve
			  #will be displayed as they are being fit.

			  #if there are fewer than 7 points in a cluster, we fit a principal component
			  #line instead of a principal curve.


			  #fewer than seven points
			       if(length(x[,1])<7) {

				  temp<-prcomp(cbind(x[,1],x[,2]))
				  temp.slope<-temp$rot[2,1]/temp$rot[1,1]
				  temp.int<-mean(x[,2])-(temp.slope)*(mean(x[,1]))
				  temp.coef<-c(temp.int,temp.slope)
				  a<-projpoints(x,temp.coef)

				  if(plot.me) {plot(x[,1],x[,2])
					       abline(temp.coef)
					       points(a,pch=2)
					       }

				  vabout<-vdist(x,a)
				  nn<-length(x[,1])
				  epsilon<-a[1:(nn-1),]-a[2:nn,]
				  mu<-apply(epsilon,2,mean)
				  mu<-matrix(mu,byrow=T,ncol=2,nrow=nn-1)
				  valong<-(.5)*vdist(epsilon,mu)
			*/
		} else {
			// call to principal curve
			//
			// mafm: important! I first thought that 'df=dg' was
			// ignored, since principal.curve.R:principal.curve()
			// doesn't acknowledge that parameter. It turns out that
			// when passing '...' as a parameter for smooth.spline()
			// and the others, the function sort of inherits the
			// environment, so it can use the variables from the
			// caller without being passed explicitly.  This is 'df'
			// 'maxit' not because it would have to be encapsulated
			// in a list for 'spar' options, but maybe there are
			// more things going on that I don't fully grasp.

			PrincipalCurve.Result temp = PrincipalCurve.principalCurve(x,
										   dg,
										   PrincipalCurve.DEFAULT_MAX_ITERATIONS);
			vabout = temp.dist;

			// mafm: should be nn=numPoints, temp.s.length=x.length,
			// so eliminated
			//
			//int nn = temp.s.length;

			// For closed principal curves, the nn index in epsilon
			// should wrap around, so length(epsilon) = nn.  For
			// open curves, we have nn-1 segments.  We must put the
			// points in the s matrix in the correct order before
			// calculating variance along the curve.  The ordering
			// is provided by the $tag value from principal.curve()

			//news<-temp$s[temp$tag,]
			//
			// news has the same contents than temp.s, but the
			// points are ordered as given by temp.tag.
			double[][] news = new double[temp.s.length][temp.s[0].length];
			for (int i = 0; i < temp.s.length; ++i) {
				news[i] = temp.s[temp.tag[i]];
			}

			//epsilon<-news[1:(nn-1),]-news[2:nn,]
			//
			// epsilon has the same structure as news and temp.s but
			// with a row less, and the contents are the differences
			// of the rows.
			double[][] epsilon = new double[news.length-1][news[0].length];
			for (int i = 0; i < epsilon.length; ++i) {
				for (int j = 0; j < epsilon[i].length; ++j) {
					epsilon[i][j] = news[i][j] - news[i+1][j];
				}
			}

			//mu<-apply(epsilon,2,mean)
			//
			// get the mean of columns of epsilon
			double[] mu = Util.meanOfColumns(epsilon);

			//mu<-matrix(mu,byrow=T,ncol=p,nrow=nn-1)
			//
			// convert mu into a matrix, copying rows as needed
			double[][] muMatrix = new double[epsilon.length][epsilon[0].length];
			for (int row = 0; row < muMatrix.length; ++row) {
				muMatrix[row] = mu;
			}

			//valong<-(.5)*vdist(epsilon,mu)
			valong = 0.5 * vdist(epsilon, muMatrix);
		}

		total = (alpha * vabout) + valong;
		return total;
	}


	/** This function returns the final classification and the list of total
	 * variance at each merge (which aids determination of the correct
	 * number of clusters without use of a penalty function).
	 *
	 * @param x or points, is the data in two columns (each row is a data
	 * point)
	 *
	 * @param initialClustering has the same length as the number of rows in
	 * x/points, with each entry giving the number of the cluster to which
	 * that point initially belongs.
	 *
	 * @param alpha controls the relative weight of variance along the curve
	 * and variance about the curve
	 *
	 * @param df specifies the number of degrees of freedom to use in each
	 * feature cluster
	 *
	 * @param force when true, causes clustering to continue until the
	 * number of clusters is equal to forcenum, instead of stopping when the
	 * total variance increases
	 *
	 * @param forcenum see "force"
	 *
	 *
	 * @return The value of the calculation
	 */
	public static Result hpcc(double[][] x,
				  int[] initialClustering,
				  double alpha /* =0.4 */,
				  int dg /* =5 */,
				  boolean force /* =false */,
				  int forceNumberOfClusters /* =1 */) {

		if (x.length != initialClustering.length) {
			throw new RuntimeException("HPPC.hpcc(): given arrays have different sizes");
		}

		/*
		  #uses clust.init as initial clustering.                                                                                                                 class1<-clust.init

		  #initialize variables.
		  lastmerge<-c(0,100)
		  clvars<-0
		  mergevars<-0
		  totalvar.track<-rep(0,50)
		  totalvar.counter<-0
		  done<-F
		*/
		int[] classArray = initialClustering;
		int[] classes = null;
		double[] clusterVariances = null;
		double[][] mergeVariances = null;
		int[] lastMerge = { 0, 100 };
		Vector<Double> totalVarTrack = new Vector<Double>();

		boolean done = false;
		while (!done) {
			/* classes<-unique(class1)
			   clnum<-length(classes) */
			classes = Util.unique(classArray);

			/* lastm<-min(lastmerge)
			   lastn<-max(lastmerge)
			   oldclvars<-clvars
			   clvars<-rep(0,clnum)	*/
			int lastMergeMin = Util.min(lastMerge);
			int lastMergeMax = Util.max(lastMerge);

			double[] oldClusterVariances = clusterVariances;
			clusterVariances = new double[classes.length];

			// find variance of each cluster
			for (int i = 0; i < classes.length; ++i) {
				if (i < lastMergeMin) {
					// mafm: oldClusterVariances null in 1st
					// iter.
					clusterVariances[i] = oldClusterVariances[i];
				} else if (i > lastMergeMax) {
					// mafm: i+1 might be OutOfBounds... is
					// guaranteed that
					// oldClusterVariances.length >
					// clusterVariances.length?
					clusterVariances[i] = oldClusterVariances[i+1];
				} else {
					// singletons are labeled 0 by mclust.
					// We arbitrarily give them a large
					// variance to force their inclusion in
					// other clusters.
					if (classes[i] == 0) {
						clusterVariances[i] = 100.0;
					} else {
						// x[class1==classes[i],]
						int[] targetClasses = { classes[i] };
						double[][] a = Util.getRowsOfGivenClasses(x,
											  classArray,
											  targetClasses);
						clusterVariances[i] = clustVarSpline(a, dg, alpha);
					}
				}

				trace("hpcc()", "Clvars for cluster " + i + ": " + clusterVariances[i]);
			}

			/* Calculate variance for each possible merge.
			 * Alternate approaches:
			 *
			 * 1. calculate merge variances only until one is found
			 * that will reduce total variance (could be adapted to
			 * simulated annealing for large numbers of clusters)
			 *
			 * 2. could randomly choose merges to evaluate
			 *
			 * 3. only look at merges of clusters which are "close"
			 */
			double[][] oldMergeVariances = mergeVariances;
			mergeVariances = new double[classes.length][classes.length];
			for (int i = 0; i < classes.length; ++i) {
				for (int j = i; j < classes.length; ++j) {
					// mafm: note that j is initialized to
					// 'i', is this correct?
					if (i == j) {
						mergeVariances[i][j] = clusterVariances[i];
					} else {
						if (i<lastMergeMin && j<lastMergeMin) {
							// mafm:
							// oldMergeVariances is
							// null in the first
							// iteration...
							mergeVariances[i][j] = oldMergeVariances[i][j];
						} else if (i>lastMergeMax && j>lastMergeMax) {
							// mafm: i+1/j+1 might
							// be OutOfBounds...
							mergeVariances[i][j] = oldMergeVariances[i+1][j+1];
						} else {
							//rbind(x[class1==classes[i],],x[class1==classes[j],])
							int[] targetClasses = { classes[i], classes[j] };
							double[][] a = Util.getRowsOfGivenClasses(x,
												  classArray,
												  targetClasses);
							mergeVariances[i][j] = clustVarSpline(a,
											      dg,
											      alpha);
						}
					}
					trace("hpcc()", "mergevars[" +i+ "][" +j+ "]: " + mergeVariances[i][j]);
				}
			}

			/* Choose merge to minimize total variance and update
			 * classArray.  If no merge will reduce variance and
			 * force=F, we are done */
			double sumOfClusterVariances = Util.sum(clusterVariances);
			double noMergeVariance = sumOfClusterVariances + penalty(classes.length);
			// variance, clust num 1, clust num 2
			class CurrentMinimumVariance {
				// mafm: was variance=1000, changed with
				// following comment
				//
				// LSB: Too low a maximum. Very important when
				// working on 10^8 instances
				public double variance = Double.POSITIVE_INFINITY; // just for initialization
				public int cluster1 = -1;
				public int cluster2 = -1;
			}
			CurrentMinimumVariance currentMinVariance = new CurrentMinimumVariance();
			// the total variances (not used)
			//double[][] totalVariances = new double[classes.length][classes.length];

			for (int i = 0; i < classes.length; ++i) {
				for (int j = i; j < classes.length; ++j) {
					// mafm: note that j is initialized to
					// 'i', is this correct?
					if (i==j) {
						// totalVariances[i][j] = noMergeVariance;
						continue;
					} else {
						double totalVariance = mergeVariances[i][j]
							+ sumOfClusterVariances
							- clusterVariances[i]
							- clusterVariances[j]
							+ penalty(classes.length-1);
						if (totalVariance < currentMinVariance.variance) {
							currentMinVariance.variance = totalVariance;
							currentMinVariance.cluster1 = i;
							currentMinVariance.cluster2 = j;
						}
					}
				}
			}

			totalVarTrack.add(noMergeVariance);

			trace("hpcc()", "Current minimum: " + currentMinVariance.variance);
			trace("hpcc()", "Total variance before merges: " + noMergeVariance);

			if (currentMinVariance.variance > noMergeVariance) {
				if (force == false
				    || classes.length == forceNumberOfClusters) {
					trace("hpcc()", "Princlust Done!");
					done = true;
				} else {
					trace("hpcc()", "Forcing a merge:"
					      + "\n - If we stop merging now the total variance is: " + noMergeVariance
					      + "\n - Merging " + currentMinVariance.cluster1
					      + " with " + currentMinVariance.cluster2
					      + " will yield total variance of " + currentMinVariance.variance);
					lastMerge[0] = currentMinVariance.cluster1;
					lastMerge[1] = currentMinVariance.cluster2;

					// class1[class1==classes[currentmin[2]]]<-classes[currentmin[3]];
					for (int i = 0; i < classArray.length; ++i) {
						if (classArray[i] == classes[currentMinVariance.cluster1]) {
							classArray[i] = classes[currentMinVariance.cluster2];
						}
					}
				}
			} else {
				if (classes.length == forceNumberOfClusters) {
					trace("hpcc()", "Princlust Done!");
					done = true;
				} else {
					trace("hpcc()", "Merge (not forced):"
					      + "\n - If we stop merging now the total variance is: " + noMergeVariance
					      + "\n - Merging " + currentMinVariance.cluster1
					      + " with " + currentMinVariance.cluster2
					      + " will yield total variance of " + currentMinVariance.variance);
					lastMerge[0] = currentMinVariance.cluster1;
					lastMerge[1] = currentMinVariance.cluster2;

					// class1[class1==classes[currentmin[2]]]<-classes[currentmin[3]];
					for (int i = 0; i < classArray.length; ++i) {
						if (classArray[i] == classes[currentMinVariance.cluster1]) {
							classArray[i] = classes[currentMinVariance.cluster2];
						}
					}
				}
			}

			/* mafm: alternate version of code above, not tested but
			 * apparently more elegant and straightforward...

			if (currentMinVariance.variance < noMergeVariance
			    || currentMinVariance.variance >= noMergeVariance && force) {
				System.err.println("Merging (forcing: "
						   + (currentMinVariance.variance >= noMergeVariance && force)
						   + "):");
				System.err.println(" - If we stop merging now "
						   + "the total variance is: "
						   + noMergeVariance);
				System.err.println(" - Merging " + currentMinVariance.cluster1
						   + " with " + currentMinVariance.cluster2
						   + " will yield total variance of "
						   + currentMinVariance.variance);
				lastMerge[0] = currentMinVariance.cluster1;
				lastMerge[1] = currentMinVariance.cluster2;

				// class1[class1==classes[currentmin[2]]]<-classes[currentmin[3]];
				for (int i = 0; i < classArray.length; ++i) {
					if (classArray[i] == classes[currentMinVariance.cluster1]) {
						classArray[i] = classes[currentMinVariance.cluster2];
					}
				}

				if (classes.length <= forceNumberOfClusters) {
					System.err.println("Princlust Done!");
					done = true;
				}
			} else {
				System.err.println("No suitable merging and not "
						   + "forcing a merge, finishing...");
				done = true;
			}
			*/
		}

		double[] tmpTotalVarTrack = new double[totalVarTrack.size()];
		for (int i = 0; i < totalVarTrack.size(); ++i) {
			tmpTotalVarTrack[i] = totalVarTrack.get(i).doubleValue();
		}

		return new Result(x, tmpTotalVarTrack, classArray);
	}
}
