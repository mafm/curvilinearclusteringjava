/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.lang.Math;
// svd() operation
import org.apache.commons.math.linear.SingularValueDecompositionImpl;
import org.apache.commons.math.linear.Array2DRowRealMatrix;


/** Principal Curve class
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class PrincipalCurve {

	/** Some parameter defaults (Java lacks this property) */
	private static final double DEFAULT_THRESHOLD = 0.001;
	/** Some parameter defaults (Java lacks this property) */
	private static final double DEFAULT_STRETCH = 2.0;

	/** Some parameter defaults (Java lacks this property) */
	public static final int DEFAULT_MAX_ITERATIONS = 10;

	/** Whether to use RWrapper for everything, or just the necessary */
	public static boolean USE_ONLY_RWRAPPER = false;

	/** Whether to use Apache Commons Math for SVD, or R's */
	public static boolean USE_APACHE_COMMONS_MATH_SVD = true;

	/** Whether to use SimpleSmoothing for smooth.splines(), or R's */
	public static boolean USE_SIMPLE_SMOOTH_SPLINES = false;

	/** Parameter to control whether to output trace information or not */
	public static boolean TRACE = false;


	/** Show trace information
	 *
	 * @param message The message to show
	 */
	public static void trace(String function, String message) {
		if (TRACE) {
			Logger.DEBUG("PrincipalCurve." + function, message);
		}
	}


	/** Result of the computation of a Principal Curve */
	public static class Result {
		/** Result value */
		public double[][] s;
		/** Result value */
		public double[] lambda;
		/** Result value */
		public int[] tag;
		/** Result value */
		public double dist;

		/** Default constructor */
		public Result() { }

		/** Constructor with initializers */
		public Result(double[][] s, double[] lambda, int[] tag, double dist) {
			this.s = s;
			this.lambda = lambda;
			this.tag = tag;
			this.dist = dist;
		}
	}

	/** Result for R's svd() operation (Singular Value Decomposition)
	 *
	 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
	 */
	public static class SVDResult {
		/** Value */
		double[] d;
		/** Value */
		double[][] u;
		/** Value */
		double[][] v;
	}


	/** Wrapper for GetLambda class/method call
	 *
	 * Original code:

	 "get.lam" <- function(x, s, tag, stretch = 2)
	 {
	   storage.mode(x) <- "double"
	   storage.mode(s) <- "double"
	   storage.mode(stretch) <- "double"
	   if(!missing(tag))
	     s <- s[tag,  ]
	   np <- dim(x)
	   if(length(np) != 2)
	     stop("get.lam needs a matrix input")
	   n <- np[1]
	   p <- np[2]
	   tt <- .Fortran("getlam",
			  n,
			  p,
			  x,
			  s = x,
			  lambda = double(n),
			  tag = integer(n),
			  dist = double(n),
			  as.integer(nrow(s)),
			  s,
			  stretch,
			  double(p),
			  double(p),
			  PACKAGE = "princurve")[c("s", "tag", "lambda", "dist")]
	   tt$dist <- sum(tt$dist)
	   class(tt) <- "principal.curve"
	   tt
	 }
	*/
	public static Result getLambdaWrapper(double[][] x,
					      double[][] s,
					      int[] tag,
					      double stretch) {
		// mafm: use RWrapper only
		if (USE_ONLY_RWRAPPER) {
			return RWrapper.getLambdaWrapper(x, s, tag, stretch);
		}

		int numberOfPoints = x.length;
		int numberOfDimensions = x[0].length;

		if (tag != null && tag.length != s.length) {
			throw new RuntimeException("getLambdaWrapper(): 's' and 'tag' have different sizes");
		}
		//if(!missing(tag))
		//  s <- s[tag,  ]

		//UtilDebug.print("getLambdaWrapper(), s before sorting", s);
		if (tag != null) {
			double[][] s2 = new double[s.length][s[0].length];
			for (int i = 0; i < s.length; ++i) {
				//s2[tag[i]] = Arrays.copyOf(s[i], s[i].length);
				s2[i] = s[tag[i]];
			}
			s = s2;
			//UtilDebug.print("getLambdaWrapper(), s sorted", s);
			//UtilDebug.print("getLambdaWrapper(), s sorted, tag", tag);
		} else {
			//UtilDebug.print("getLambdaWrapper(), s unsorted", s);
			//UtilDebug.print("getLambdaWrapper(), s unsorted, tag", tag);
		}

		// initially, a copy of x
		double[][] sx = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				sx[i][j] = x[i][j];
			}
		}

		// to be filled up (Java cannot return multiple values)
		int[] tagResult = new int[numberOfPoints];
		double[] lambda = new double[numberOfPoints];
		double[] dist = new double[numberOfPoints];

		GetLambda.getLambda(numberOfPoints,
				    numberOfDimensions,
				    x,
				    sx,
				    s,
				    stretch,
				    tagResult,
				    lambda,
				    dist);

		// mafm: result, new s is old sx (referencing, no need to copy)
		return new Result(sx, lambda, tagResult, Util.sum(dist));
	}


	/** Main method, to calculate principal curves
	 *
	 * Note: Only implementing the code that we're going to use, not other
	 * parts, because it's quite tricky and it would go untested...
	 *
	 * Another note: There's a newer versions of the code (2.11 at the time
	 * of writing this), this translation is based on older code, but the
	 * changes are small enough, sometimes just renaming variables and
	 * comments, or changes in parts that aren't implemented by us.
	 *
	 * Original code:

	 "principal.curve" <-
	   function(x, start = NULL, thresh = 0.001, plot.true = FALSE, maxit = 10,
		   stretch = 2, smoother = "smooth.spline", trace = FALSE, ...)
	 {
	   smooth.list <- c("smooth.spline", "lowess", "periodic.lowess")
	   smoother <- match.arg(smoother, smooth.list)
	   stretches <- c(2, 2, 0)
	   if(missing(stretch))
	     stretch <- stretches[match(smoother, smooth.list)]
	   this.call <- match.call()
	   dist.old <- sum(diag(var(x)))
	   d <- dim(x)
	   n <- d[1]
	   p <- d[2]
	 # You can give starting values for the curve
	   if(missing(start)) {
	 # use largest principal component
	     if(smoother == "periodic.lowess") start <- startCircle(x)
	     else {
	       xbar <- apply(x, 2, "mean")
	       xstar <- scale(x, xbar, FALSE)
	       svd.xstar <- svd(xstar)
	       dd <- svd.xstar$d
	       lambda <- svd.xstar$u[, 1] * dd[1]
	       tag <- order(lambda)
	       s <- scale(outer(lambda, svd.xstar$v[, 1]),  - xbar, FALSE)
	       dist <- sum((dd^2)[-1]) * n
	       start <- list(s = s, tag = tag, lambda = lambda, dist
			    = dist)
	     }
	   }
	   else if(!inherits(start, "principal.curve")) {
	 # use given starting curve
	     if(is.matrix(start))
	       start <- get.lam(x, start, stretch = stretch)
	     else stop("Invalid starting curve: should be a matrix or principal.curve")
	   }
	   pcurve <- start
	   if(plot.true) {
	     plot(x[, 1:2], xlim = adjust.range(x[, 1], 1.3999999999999999),
		 ylim = adjust.range(x[, 2], 1.3999999999999999))
	     lines(pcurve$s[pcurve$tag, 1:2])
	   }
	   it <- 0
	   if(trace)
	     cat("Starting curve---distance^2: ", pcurve$dist, "\n")
	   while(abs((dist.old - pcurve$dist)/dist.old) > thresh & it < maxit) {
	     it <- it + 1
	     s <- NULL
	     for(j in 1:p) {
	       sj <- switch(smoother,
			   lowess = lowess(pcurve$lambda, x[, j], ...)$y,
			   smooth.spline = smooth.spline(pcurve$lambda,
			       x[, j], ...)$y,
			   periodic.lowess = periodic.lowess(pcurve$lambda,
			       x[, j], ...)$y)
	       s <- cbind(s, sj)
	     }
	     dist.old <- pcurve$dist
	     pcurve <- get.lam(x, s, stretch = stretch)
	     if(smoother == "periodic.lowess")
	       pcurve <- bias.correct.curve(x, pcurve, ...)
	     if(plot.true) {
	       plot(x[, 1:2], xlim = adjust.range(x[, 1], 1.3999999999999999),
		   ylim = adjust.range(x[, 2], 1.3999999999999999))
	       lines(pcurve$s[pcurve$tag, 1:2])
	     }
	     if(trace)
	       cat("Iteration ", it, "---distance^2: ", pcurve$dist, "\n")
	   }
	   structure(list(s = pcurve$s, tag = pcurve$tag, lambda = pcurve$lambda,
			 dist = pcurve$dist, call = this.call),
		    class = "principal.curve")
	 }
	 */
	public static Result principalCurve(double[][] x,
					    int degreesOfFreedom,
					    int maxIterations) {
		// mafm: use RWrapper only
		if (USE_ONLY_RWRAPPER) {
			return RWrapper.principalCurve(x, degreesOfFreedom, maxIterations);
		}

		/* mafm: only one smoother is used (smooth.splines), so only one
		 * is implemented and this code is not translated.

		  smooth.list <- c("smooth.spline", "lowess", "periodic.lowess")
		  smoother <- match.arg(smoother, smooth.list)
		  stretches <- c(2, 2, 0)
		  if(missing(stretch))
		  stretch <- stretches[match(smoother, smooth.list)]
		  this.call <- match.call()
		  dist.old <- sum(diag(var(x)))
		  d <- dim(x)
		  n <- d[1]
		  p <- d[2]
		*/
		int numberOfPoints = x.length;
		int numberOfDimensions = x[0].length;
		double distOld = Util.sum(Util.diagVar(x));

		/* mafm: 'start' is always missing in our calls, so of the
		 * following conditional call, only the first part is
		 * implemented, and we won't use the periodic.lowess smoother so
		 * there it goes another chunk of code.

		# You can give starting values for the curve
		  if(missing(start)) {
		# use largest principal component
		    if(smoother == "periodic.lowess") start <- startCircle(x)
		    else {
		      xbar <- apply(x, 2, "mean")
		      xstar <- scale(x, xbar, FALSE)
		      svd.xstar <- svd(xstar)
		      dd <- svd.xstar$d
		      lambda <- svd.xstar$u[, 1] * dd[1]
		      tag <- order(lambda)
		      s <- scale(outer(lambda, svd.xstar$v[, 1]),  - xbar, FALSE)
		      dist <- sum((dd^2)[-1]) * n
		      start <- list(s = s, tag = tag, lambda = lambda, dist = dist)
		    }
		  }
		  else if(!inherits(start, "principal.curve")) {
		  # use given starting curve
		      if(is.matrix(start))
			start <- get.lam(x, start, stretch = stretch)
		      else stop("Invalid starting curve: should be a matrix or principal.curve")
		  }
		*/

		// xbar is the mean of the columns
		double[] xbar = Util.meanOfColumns(x);

		// xstar is the scaled ("centred", as it does not apply actual
		// scaling) version of x, subtracting the mean of the column (as
		// calculated into xbar) from each element of the column
		double[][] xstar = new double[x.length][x[0].length];
		for (int j = 0; j < numberOfDimensions; ++j) {
			for (int i = 0; i < numberOfPoints; ++i) {
				xstar[i][j] = x[i][j] - xbar[j];
			}
		}

		// SVD implementation: R or Apache Commons Math
		SVDResult svd_xstar = new SVDResult();
		if (USE_APACHE_COMMONS_MATH_SVD) {
			SingularValueDecompositionImpl svd_xstar_apache = new SingularValueDecompositionImpl(new Array2DRowRealMatrix(xstar));
			svd_xstar.d = svd_xstar_apache.getSingularValues();
			svd_xstar.u = svd_xstar_apache.getU().getData();
			svd_xstar.v = svd_xstar_apache.getV().getData();
		} else {
			svd_xstar = RWrapper.svd(xstar);
		}

		double[] lambda = new double[svd_xstar.u.length];
		for (int i = 0; i < svd_xstar.u.length; ++i) {
			lambda[i] = svd_xstar.u[i][0] * svd_xstar.d[0];
		}

		// tag will take the order of the ordered values ("lambda" is
		// not modified)
		int[] tag = QuicksortWithOrder.quicksortWithOrder(lambda);

		// s is the scaled/centred outer, see 'xstar' for description
		// (this time is +xbar because in scale() call it's -xbar)
		double[][] outer = Util.outer(lambda, Util.getColumnAsArray(svd_xstar.v, 0));
		double[][] s = new double[outer.length][outer[0].length];
		for (int j = 0; j < s[0].length; ++j) {
			for (int i = 0; i < s.length; ++i) {
				s[i][j] = outer[i][j] + xbar[j];
			}
		}

		//dist <- sum((dd^2)[-1]) * n
		double dist = Util.sum(Util.power2(Arrays.copyOfRange(svd_xstar.d, 1, svd_xstar.d.length)))
			* numberOfPoints;

		Result start = new Result(s, lambda, tag, dist);

		/*
		  pcurve <- start
		  if(plot.true) {
		    plot(x[, 1:2], xlim = adjust.range(x[, 1], 1.3999999999999999),
			 ylim = adjust.range(x[, 2], 1.3999999999999999))
		    lines(pcurve$s[pcurve$tag, 1:2])
		  }
		  it <- 0
		  if(trace)
		    cat("Starting curve---distance^2: ", pcurve$dist, "\n")
		  while(abs((dist.old - pcurve$dist)/dist.old) > thresh & it < maxit) {
		    it <- it + 1
		    s <- NULL
		    for(j in 1:p) {
		      sj <- switch(smoother,
				   lowess = lowess(pcurve$lambda, x[, j], ...)$y,
				   smooth.spline = smooth.spline(pcurve$lambda,
				       x[, j], ...)$y,
				   periodic.lowess = periodic.lowess(pcurve$lambda,
				       x[, j], ...)$y)
		      s <- cbind(s, sj)
		    }
		    dist.old <- pcurve$dist
		    pcurve <- get.lam(x, s, stretch = stretch)
		    if(smoother == "periodic.lowess")
		      pcurve <- bias.correct.curve(x, pcurve, ...)
		    if(plot.true) {
		      plot(x[, 1:2], xlim = adjust.range(x[, 1], 1.3999999999999999),
			   ylim = adjust.range(x[, 2], 1.3999999999999999))
		      lines(pcurve$s[pcurve$tag, 1:2])
		    }
		    if(trace)
		      cat("Iteration ", it, "---distance^2: ", pcurve$dist, "\n")
		  }
		  structure(list(s = pcurve$s, tag = pcurve$tag, lambda = pcurve$lambda,
				 dist = pcurve$dist, call = this.call),
			    class = "principal.curve")
		*/
		Result pcurve = start;
		trace("principalCurve()", "Starting curve---distance^2: " + pcurve.dist);
		int it = 0;
		while (it < maxIterations
		       && Math.abs((distOld - pcurve.dist)/distOld) > DEFAULT_THRESHOLD) {

			++it;
			s = new double[numberOfPoints][numberOfDimensions];
			for (int j = 0; j < numberOfDimensions; ++j) {
				/* mafm: newer versions of princurve in R
				 * contain this code

				smooth.spline = function(lambda, xj, ..., df=5) {
					o <- order(lambda);
					lambda <- lambda[o];
					xj <- xj[o];
					fit <- smooth.spline(lambda, xj, ..., df=df, keep.data=FALSE);
					predict(fit, x=lambda)$y;
				},
				*/
				int[] tagSpline = QuicksortWithOrder.quicksortWithOrder(pcurve.lambda);

				double[] lambdaSpline = Util.getSorted(pcurve.lambda, tagSpline);
				double[] xjSpline = Util.getSorted(Util.getColumnAsArray(x, j), tagSpline);

				double[] fit = null;
				if (USE_SIMPLE_SMOOTH_SPLINES) {
					fit = SimpleSmoothing.smooth(lambdaSpline, xjSpline);
				} else {
					fit = RWrapper.smoothSpline(lambdaSpline, xjSpline, degreesOfFreedom);
				}

				// mafm: about predict(), from code comments:
				//
				// BUG FIX: smooth.spline(x,y) will only use *and* return values for
				// "unique" {x}:s. This means that the fitted {y}:s maybe be fewer than
				// the input vector. In order to control for this, we use predict().
				//
				// mafm: we implement it in the call of R, if we
				// find a substitute for smooth.splines in Java
				// we'll have to find something appropriate
				if (fit.length != xjSpline.length) {
					throw new RuntimeException("principalCurve(): Result from smooth-spline shorter than input");
				}

				for (int i = 0; i < numberOfPoints; ++i) {
					s[i][j] = fit[i];
				}
			}

			distOld = pcurve.dist;
			pcurve = getLambdaWrapper(x, s, null, DEFAULT_STRETCH);

			trace("principalCurve()", "Iteration " + it + ", ---distance^2: " + pcurve.dist);
		}

		return pcurve;
	}
}
