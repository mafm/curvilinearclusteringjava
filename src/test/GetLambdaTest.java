/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class GetLambdaTest extends TestCase {

	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test GetLambda.lamix() */
	public void test_lamix() {
		int numberOfPoints = 3;
		int numberOfDimensions = 5;
		//Logger.DEBUG("test_lamix()", " - numberOfPoints: " + numberOfPoints);
		//Logger.DEBUG("test_lamix()", " - numberOfDimensions: " + numberOfDimensions);

		double[][] sx = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				sx[i][j] = i * Math.sin(i*j) + j;
				//Logger.DEBUG("test_lamix()", " - sx[" + i + "][" + j +"]: " + sx[i][j]);
			}
		}

		double[] vecx = new double[numberOfDimensions];
		vecx = sx[1];
		LamixResult lamixResult = new LamixResult();
		double[] tempsx = new double[numberOfDimensions];
		GetLambda.lamix(numberOfPoints, numberOfDimensions, vecx, sx, lamixResult, tempsx);

		/* expected results

		   Testing lamix()
		   - numberOfPoints: 3
		   - numberOfDimensions: 5
		   - sx[0][0]: 0.0
		   - sx[0][1]: 1.0
		   - sx[0][2]: 2.0
		   - sx[0][3]: 3.0
		   - sx[0][4]: 4.0
		   - sx[1][0]: 0.0
		   - sx[1][1]: 1.8414709848078965
		   - sx[1][2]: 2.909297426825682
		   - sx[1][3]: 3.1411200080598674
		   - sx[1][4]: 3.2431975046920716
		   - sx[2][0]: 0.0
		   - sx[2][1]: 2.8185948536513634
		   - sx[2][2]: 0.4863950093841436
		   - sx[2][3]: 2.4411690036021483
		   - sx[2][4]: 5.978716493246764
		   - vecx[0]: 0.0
		   - vecx[1]: 1.8414709848078965
		   - vecx[2]: 2.909297426825682
		   - vecx[3]: 3.1411200080598674
		   - vecx[4]: 3.2431975046920716
		   - Final lambda: 2.0
		   - Final dismin: 0.0
		   - Final tempsx[0]: 0.0
		   - Final tempsx[1]: 1.8414709848078965
		   - Final tempsx[2]: 2.909297426825682
		   - Final tempsx[3]: 3.1411200080598674
		   - Final tempsx[4]: 3.2431975046920716
		*/

		double lambdaRounded = Util.roundWithFormat(lamixResult.lambda, "#.################");
		double lambdaExpected = 2.0;
		Comparison.assertDouble("test_lamix(), lambda",
					lambdaExpected, lambdaRounded, Comparison.Error.ZERO);

		double disminRounded = Util.roundWithFormat(lamixResult.distmin, "#.################");
		double disminExpected = 0.0;
		Comparison.assertDouble("test_lamix(), dismin",
					disminExpected, disminRounded, Comparison.Error.ZERO);

		double[] tempsxRounded = Util.roundWithFormat(tempsx, "#.################");
		double[] tempsxExpected = {
			0.0,
			1.8414709848078965,
			2.909297426825682,
			3.1411200080598674,
			3.2431975046920716
		};
		Comparison.assertArrayDouble("test_lamix() tempsx",
					     tempsxExpected, tempsxRounded, Comparison.Error.ZERO);
	}

	/** Test GetLambda.getNewLambda() */
	public void test_getNewLambda() {
		int numberOfPoints = 3;
		int numberOfDimensions = 5;
		//Logger.DEBUG("test_getNewLambda()", " - numberOfPoints: " + numberOfPoints);
		//Logger.DEBUG("test_getNewLambda()", " - numberOfDimensions: " + numberOfDimensions);

		double[] lambda = new double[numberOfPoints];
		for (int i = 0; i < numberOfPoints; ++i) {
			lambda[i] = Math.sin(i+1)+1.0;
			//Logger.DEBUG("test_getNewLambda()", " - lambda[" + i + "]: " + lambda[i]);
		}

		double[][] sx = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				sx[i][j] = i * Math.sin(i*j) + j;
				//Logger.DEBUG("test_getNewLambda()", " - sx[" + i + "][" + j +"]: " + sx[i][j]);
			}
		}

		// emulate the call from GetLambda.getLambda()
		int[] tag = QuicksortWithOrder.quicksortWithOrder(lambda);
		GetLambda.getNewLambda(numberOfPoints,
				       numberOfDimensions,
				       sx,
				       lambda,
				       tag);

		/* expected results

		   Testing getNewLambda()
		   - numberOfPoints: 3
		   - numberOfDimensions: 5
		   - lambda[0]: 1.8414709848078965
		   - lambda[1]: 1.9092974268256817
		   - lambda[2]: 1.1411200080598671
		   - tag[0]: 0
		   - tag[1]: 1
		   - tag[2]: 2
		   - sx[0][0]: 0.0
		   - sx[0][1]: 1.0
		   - sx[0][2]: 2.0
		   - sx[0][3]: 3.0
		   - sx[0][4]: 4.0
		   - sx[1][0]: 0.0
		   - sx[1][1]: 1.8414709848078965
		   - sx[1][2]: 2.909297426825682
		   - sx[1][3]: 3.1411200080598674
		   - sx[1][4]: 3.2431975046920716
		   - sx[2][0]: 0.0
		   - sx[2][1]: 2.8185948536513634
		   - sx[2][2]: 0.4863950093841436
		   - sx[2][3]: 2.4411690036021483
		   - sx[2][4]: 5.978716493246764
		   - Final lambda[0]: 3.1346289018839597
		   - Final lambda[1]: 4.593244719117717
		   - Final lambda[2]: 0.0
		   - Final order[0]: 2
		   - Final order[1]: 0
		   - Final order[2]: 1
		   - Final lambdaOrdered[0]: 0.0
		   - Final lambdaOrdered[1]: 3.1346289018839597
		   - Final lambdaOrdered[2]: 4.593244719117717
		*/

		double[] lambdaRounded = Util.roundWithFormat(lambda, "#.################");
		double[] expectedLambda = {
			3.1346289018839597,
			4.593244719117717,
			0.0
		};
		Comparison.assertArrayDouble("test_getNewLambda() lambda",
					     expectedLambda, lambdaRounded, Comparison.Error.ZERO);

		int[] expectedOrder = { 2, 0, 1};
		Comparison.assertArrayInt("test_getNewLambda() order", expectedOrder, tag);

		double[] lambdaOrdered = Util.getSorted(lambda, tag);
		double[] expectedLambdaOrdered = {
			0.0,
			3.1346289018839597,
			4.593244719117717
		};
		Comparison.assertArrayDouble("test_getNewLambda() lambdaOrdered",
					     expectedLambdaOrdered, lambdaOrdered, Comparison.Error.ZERO);
	}

	/** Test GetLambda.getLambda() */
	public void test_getLambda() {
		double[][] x = {
			{ -7.1465695, -7.52670617 },
			{ -7.09588461, -7.41266517 },
			{ -.00568917143, -.0139301036 },
			{ -6.95107063, -7.08683373 },
			{ -6.84246016, -6.84246016 },
			{ -.711688014, -1.85413456 },
			{ -4.30821565, -1.99571755 },
			{ -6.0821868, -5.13184512 },
			{ 254.904865, 93.638522 },
			{ -3.0410934, 1.71061504 },
		};
		double[][] s = {
			{ -6.7465695, -6.92670617 },
			{ -6.49588461, -6.61266517 },
			{ .794310829, .986069896 },
			{ -5.95107063, -5.88683373 },
			{ -5.64246016, -5.44246016 },
			{ .688311986, -.254134562 },
			{ -2.70821565, -.195717545 },
			{ -4.2821868, -3.13184512 },
			{ 256.904865, 95.838522 },
			{ -.841093402, 4.11061504 },
		};
		double stretch = 2.0;

		int numberOfPoints = x.length;
		int numberOfDimensions = x[0].length;

		double[][] sx = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				sx[i][j] = x[i][j];
			}
		}

		double[][] xCopy = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				xCopy[i][j] = x[i][j];
			}
		}

		PrincipalCurve.Result result = new PrincipalCurve.Result();
		result.s = s;
		result.tag = new int[numberOfPoints];
		result.lambda = new double[numberOfPoints];

		double[] dist = new double[numberOfPoints];

		GetLambda.getLambda(numberOfPoints,
				    numberOfDimensions,
				    xCopy,
				    sx,
				    s,
				    stretch,
				    result.tag,
				    result.lambda,
				    dist);

		result.dist = Util.sum(dist);

		// expected results
		double[] lambdaRounded = Util.roundWithFormat(result.lambda, "#.#########");
		double[] expectedLambda = {
			0.0,
			0.12074720070761343,
			10.373595113065385,
			0.4657395311965929,
			0.7244837790633272,
			24.979536632793227,
			21.865360524782634,
			18.19562682026988,
			297.8476925183574,
			571.6571990677303
		};
		Comparison.assertArrayDouble("test_getLambda() lambda",
					     expectedLambda, lambdaRounded, Comparison.Error.ONE_IN_MILLION);

		int[] expectedOrder = { 0, 1, 3, 4, 2, 7, 6, 5, 8, 9 };
		Comparison.assertArrayInt("test_getLambda() order", expectedOrder, result.tag);

		double[] lambdaOrdered = Util.getSorted(lambdaRounded, result.tag);
		double[] expectedLambdaOrdered = {
			0.0,
			0.12074720070761343,
			0.4657395311965929,
			0.7244837790633272,
			10.373595113065385,
			18.19562682026988,
			21.865360524782634,
			24.979536632793227,
			297.8476925183574,
			571.6571990677303
		};
		Comparison.assertArrayDouble("test_getLambda() lambdaOrdered",
					     expectedLambdaOrdered, lambdaOrdered, Comparison.Error.ONE_IN_MILLION);

		double[] distRounded = Util.roundWithFormat(dist, "#.#########");
		double[] expectedDist = {
			0.0038074517481346,
			9.102591887973E-4,
			0.0132298081573481,
			0.0035912195038033,
			0.0162561874946973,
			0.00495048093115,
			0.3132879797876287,
			0.2898412209998912,
			1.8186636062018806,
			2.3209082426765235
		};
		Comparison.assertArrayDouble("test_getLambda() dist", expectedDist, distRounded, Comparison.Error.ONE_IN_MILLION);

		double[][] expectedS = {
			{   -7.247939,   -7.554788 },
			{   -6.495885,   -6.612665 },
			{    0.794311,    0.986070 },
			{   -5.951071,   -5.886834 },
			{   -5.642460,   -5.442460 },
			{    0.688312,   -0.254135 },
			{   -2.708216,   -0.195718 },
			{   -4.282187,   -3.131845 },
			{  256.904865,   95.838522 },
			{ -516.333010, -179.345199 }
		};
		Comparison.assertMatrixDouble("test_getLambda() s", expectedS, result.s, Comparison.Error.FIVE_IN_MILLION);

		double[][] expectedSx = {
			{  -7.194794, -7.488211 },
			{  -7.119464, -7.393843 },
			{  -0.088689,  0.065699 },
			{  -6.904236, -7.124220 },
			{  -6.742815, -6.922003 },
			{  -0.736619, -1.788340 },
			{  -3.814906, -2.260166 },
			{  -5.642460, -5.442460 },
			{ 254.427011, 94.899600 },
			{  -3.551884,  3.145885 }
		};
		Comparison.assertMatrixDouble("test_getLambda() sx", expectedSx, sx, Comparison.Error.ONE_IN_TEN_THOUSAND);
	}

	/** Test GetLambda.getLambda() */
	public void test_getLambda2() {
		double[][] x = {
			{ 6.623, 6.977 },
			{ 5.088, 5.399 },
			{ 3.391, 3.645 },
			{ 0.708, 0.827 }
		};
		double[][] s = {
			{ -5.623, -5.477 },
			{ -3.588, -3.399 },
			{ 5.391,  6.145 },
			{ 1.792,  2.173 }
		};
		double stretch = 2.0;

		int numberOfPoints = x.length;
		int numberOfDimensions = x[0].length;

		double[][] sx = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				sx[i][j] = x[i][j];
			}
		}

		double[][] xCopy = new double[numberOfPoints][numberOfDimensions];
		for (int i = 0; i < numberOfPoints; ++i) {
			for (int j = 0; j < numberOfDimensions; ++j) {
				xCopy[i][j] = x[i][j];
			}
		}

		PrincipalCurve.Result result = new PrincipalCurve.Result();
		result.s = s;
		result.tag = new int[numberOfPoints];
		result.lambda = new double[numberOfPoints];

		double[] dist = new double[numberOfPoints];

		GetLambda.getLambda(numberOfPoints,
				    numberOfDimensions,
				    xCopy,
				    sx,
				    s,
				    stretch,
				    result.tag,
				    result.lambda,
				    dist);

		result.dist = Util.sum(dist);

		// expected results
		double[] lambdaRounded = Util.roundWithFormat(result.lambda, "#.#########");
		double[] expectedLambda = {
			0.0,
			0.7562705332489711,
			3.195523222266485,
			7.08530278248837
		};
		Comparison.assertArrayDouble("test_getLambda2() lambda",
					     expectedLambda, lambdaRounded, Comparison.Error.FIVE_IN_MILLION);

		int[] expectedOrder = { 0, 1, 2, 3 };
		Comparison.assertArrayInt("test_getLambda2() order", expectedOrder, result.tag);

		double[] lambdaOrdered = Util.getSorted(lambdaRounded, result.tag);
		double[] expectedLambdaOrdered = {
			0.0,
			0.7562705332489711,
			3.195523222266485,
			7.08530278248837
		};
		Comparison.assertArrayDouble("test_getLambda2() lambdaOrdered",
					     expectedLambdaOrdered, lambdaOrdered, Comparison.Error.FIVE_IN_MILLION);

		double[] distRounded = Util.roundWithFormat(dist, "#.#########");
		double[] expectedDist = {
			2.210047999999972,
			0.0763798805393116,
			0.0386313359556016,
			0.0100974804625977
		};
		Comparison.assertArrayDouble("test_getLambda2() dist",
					     expectedDist, distRounded, Comparison.Error.ONE_IN_MILLION);

		double[][] expectedS = {
			{ -9.693000, -9.633000 },
			{ -3.588000, -3.399000 },
			{  5.391000,  6.145000 },
			{ -5.406000, -5.771000 }
		};
		Comparison.assertMatrixDouble("test_getLambda2() s",
					      expectedS, result.s, Comparison.Error.ONE_IN_MILLION);

		double[][] expectedSx = {
			{ 5.391000, 6.145000 },
			{ 4.883198, 5.584570 },
			{ 3.245349, 3.776974 },
			{ 0.633535, 0.894472 }
		};
		Comparison.assertMatrixDouble("test_getLambda2() sx",
					      expectedSx, sx, Comparison.Error.FIVE_IN_MILLION);
	}
}
