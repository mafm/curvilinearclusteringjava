/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.*;
import junit.textui.*;
import org.junit.Assert;
import java.io.*;
import java.util.*;
import java.text.*;


/** Result printer
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class ResultPrinterVerbose extends ResultPrinter {

	/** Test class name, helper to modify a bit the output */
	private String testClassName = null;

	/** Failure message on last test, if any */
	private String failureMsg = null;

	/** Timing of each test */
	private Timer timer = null;
	/** Timing of each test class */
	private Timer timerClass = null;

	/** Length of printed function */
	private int lengthPrinted = 0;
	/** Length of line */
	private final int lineLength = 80;


	/** @see ResultPrinter */
	public ResultPrinterVerbose() {
		super(new PrintStream(System.out));
	}

	/** @see ResultPrinter */
	public void startTest(Test test) {
		// class containing the test
		String tmp_testClassName = test.toString().replaceAll(".+\\(", "").replaceAll("\\)", "");
		if (testClassName == null || !tmp_testClassName.equals(testClassName)) {
			// print time for past class with tests
			if (testClassName != null) {
				long runTime = timerClass.elapsedTimeMillis();
				getWriter().println("  Time for " + testClassName + ": "
						    + timerClass.getPrettyFormatting(runTime)
						    + " (" + timerClass.getPrettySeconds(runTime) + ")");
			}

			// print class header
			testClassName = tmp_testClassName;
			getWriter().println("\n  .: " + testClassName + " :.");
			timerClass = new Timer();
		}

		// print class name
		String s = "   · " + test.toString().replaceAll("\\(.+\\)", "\\(\\)");
		lengthPrinted = s.length();
		getWriter().print(s);
		// mafm: doesn't flush until newline or special printf()/format() calls
		//getWriter().flush();
		timer = new Timer();
	}

	/** @see ResultPrinter */
	public void endTest(Test test) {
		// in two steps because the time taken to format stuff takes a
		// few milliseconds itself...
		long runTime = timer.elapsedTimeMillis();

		// mafm: not the fanciest way, but this version doesn't seem to
		// allow a simple way to insert a repeated sequence of
		// chars... " (#####.###s)" is the seconds in the end, which
		// "capacity" enough for more than 27 hours... so should be
		// enough at least for our tests
		//
		// add elements to the line backwards, until reaching the test
		// name itself
		String s = " (" + timer.getPrettySeconds(runTime) + ")";
		while (s.length() < " (#####.###s)".length()) {
			s = " " + s;
		}
		s = timer.getPrettyFormatting(runTime) + s;
		while (s.length() < (lineLength - lengthPrinted)) {
			s = " " + s;
		}
		getWriter().println(s);

		// print failure message, if any
		if (failureMsg != null) {
			getWriter().println("\n" + failureMsg + "\n");
			failureMsg = null;
		}
	}

	/** @see ResultPrinter */
	public void addError(Test test, Throwable t) {
		failureMsg = "      !! ERROR: " + t.toString();
	}

	/** @see ResultPrinter */
	public void addFailure(Test test, AssertionFailedError t) {
		failureMsg = "      !! FAILURE: " + t.toString();
	}

	/** @see ResultPrinter */
	public void printHeader(long runTime) {
		// debugging
		//super.printHeader(runTime);

		// print time for last class with tests (no other way to do it
		// unless with this ugly hack, it seems)
		if (testClassName != null) {
			long runTimeLastClass = timerClass.elapsedTimeMillis();
			getWriter().println("  Time for " + testClassName + ": "
					    + timerClass.getPrettyFormatting(runTimeLastClass)
					    + " (" + timerClass.getPrettySeconds(runTimeLastClass) + ")");
			testClassName = null;
		}

		// printing
		getWriter().println("\nTime for this Test Suite: " + timer.getPrettyFormatting(runTime)
				    + " (" + timer.getPrettySeconds(runTime) + ")");
	}
}


/** Test Suite
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class MainTestSuite {
	/** Test Runner */
	private static TestRunner testRunner = null;

	/** File to be written */
	private static String errorLog = null;

	/** Run the test suite using the textual runner */
	public static void main(String[] args) {
		testRunner = new TestRunner(new ResultPrinterVerbose());

		// error log
		DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd_HHmmss",
								 Locale.getDefault());
		errorLog = "error_" + dateFormatter.format(Calendar.getInstance().getTime()) + ".log";
		File f = new File(errorLog);

		// set error printer for comparisons
		try {
			Comparison.setPrintWriter(new PrintWriter(f));
			Logger.INFO("MainTestSuite",
				    "Tests won't abort at the first comparison mismatch, they will be saved in log:\n  "
				    + f.getAbsolutePath() + "\n");
		} catch (Exception e) {
			Logger.FATAL("MainTestSuite", "Cannot write assertion errors to:\n  "
				     + f.getAbsolutePath() + "\n");
			return;
		}

		// parse arguments
		if (args.length == 0) {
			// equivalent
			args = new String[] { "all" };
		} else if (args.length > 1) {
			Logger.FATAL("MainTestSuite",
				     "PROGRAM [basic|pcurve_rwrapper|pcurve_java], or no argument");
			return;
		}

		if (args.length == 1) {
			if (args[0].equals("basic")) {
				testRunner.doRun(suiteBasic());
			} else if (args[0].equals("pcurve_java_only")) {
				testRunner.doRun(suitePrincipalCurveJavaOnly());
			} else if (args[0].equals("pcurve_java+apache_svd+r_smoothing")) {
				testRunner.doRun(suitePrincipalCurveJavaApacheSVDRSmoothing());
			} else if (args[0].equals("pcurve_java+r_svd+r_smoothing")) {
				testRunner.doRun(suitePrincipalCurveJavaRSVDRSmoothing());
			} else if (args[0].equals("pcurve_rwrapper")) {
				testRunner.doRun(suitePrincipalCurveRWrapper());
			} else if (args[0].equals("all")) {

				testRunner.doRun(suiteBasic());
				testRunner.doRun(suitePrincipalCurveJavaOnly());
				testRunner.doRun(suitePrincipalCurveJavaApacheSVDRSmoothing());
				testRunner.doRun(suitePrincipalCurveJavaRSVDRSmoothing());
				testRunner.doRun(suitePrincipalCurveRWrapper());

			} else {
				Logger.FATAL("MainTestSuite",
					     "Usage: PROGRAM_INVOCATION -Dtest.suite.arg=TEST_NAME, or no argument\n"
					     + "  (valid test names: basic, pcurve_rwrapper, pcurve_java+r_svd, pcurve_java+apache_svd, pcurve_java)");
				return;
			}
		}

		// check whether there were errors or not
		if (!f.exists() || (f.exists() && f.length() == 0)) {
			Logger.INFO("MainTestSuite", "No errors!");
		} else {
			Assert.fail("Errors found, log of the comparisons available in:\n  "
				    + f.getAbsolutePath());
		}
	}

	/** Set up the suite */
	private static Test suiteBasic() {
		// sleep a bit so logger messages don't mix with other printings
		try { Thread.sleep(100); } catch (Exception e) { }

		String s = "Testing basic functions (not PrincipalCurve, HPCC and CEMPCC)";
		Logger.INFO("MainTestSuite", s);
		Comparison.setTestTitle(s);

		TestSuite suite = new TestSuite();

		suite.addTestSuite(UtilTest.class);
		suite.addTestSuite(QuicksortWithOrderTest.class);

		return suite;
	}

	/** Set up the suite */
	private static Test suitePrincipalCurveJavaOnly() {
		// sleep a bit so logger messages don't mix with other printings
		try { Thread.sleep(100); } catch (Exception e) { }

		String s = "Testing HPCC and CEMPCC with Java-only code";
		Logger.INFO("MainTestSuite", s);
		Comparison.setTestTitle(s);

		// Test with PrincipalCurve Java version
		PrincipalCurve.USE_ONLY_RWRAPPER = false;
		// Test with Apache Commons Math SVD enabled
		PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD = true;
		// Test with SSJ Smooth Splines enabled
		PrincipalCurve.USE_SIMPLE_SMOOTH_SPLINES = true;

		TestSuite suite = new TestSuite();

		suite.addTestSuite(GetLambdaTest.class);
		// mafm: don't bother, curves are different...
		//suite.addTestSuite(PrincipalCurveTest.class);
		suite.addTestSuite(HPCCTest.class);
		suite.addTestSuite(CEMPCCTest.class);

		return suite;
	}

	/** Set up the suite */
	private static Test suitePrincipalCurveJavaApacheSVDRSmoothing() {
		// sleep a bit so logger messages don't mix with other printings
		try { Thread.sleep(100); } catch (Exception e) { }

		String s = "Testing HPCC and CEMPCC with Java version of PrincipalCurve, Apache Common Math svd() and R smooth_spline()";
		Logger.INFO("MainTestSuite", s);
		Comparison.setTestTitle(s);

		// Test with PrincipalCurve Java version
		PrincipalCurve.USE_ONLY_RWRAPPER = false;
		// Test with Apache Commons Math SVD disabled
		PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD = true;
		// Test with Simple Smooth Splines disabled
		PrincipalCurve.USE_SIMPLE_SMOOTH_SPLINES = false;

		TestSuite suite = new TestSuite();

		suite.addTestSuite(GetLambdaTest.class);
		// mafm: don't bother, curves are different...
		//suite.addTestSuite(PrincipalCurveTest.class);
		suite.addTestSuite(HPCCTest.class);
		suite.addTestSuite(CEMPCCTest.class);

		return suite;
	}

	/** Set up the suite */
	private static Test suitePrincipalCurveJavaRSVDRSmoothing() {
		// sleep a bit so logger messages don't mix with other printings
		try { Thread.sleep(100); } catch (Exception e) { }

		String s = "Testing HPCC and CEMPCC with Java version of PrincipalCurve, R svd() and R smooth_spline()";
		Logger.INFO("MainTestSuite", s);
		Comparison.setTestTitle(s);

		// Test with PrincipalCurve Java version
		PrincipalCurve.USE_ONLY_RWRAPPER = false;
		// Test with Apache Commons Math SVD disabled
		PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD = false;
		// Test with Simple Smooth Splines disabled
		PrincipalCurve.USE_SIMPLE_SMOOTH_SPLINES = false;

		TestSuite suite = new TestSuite();

		suite.addTestSuite(GetLambdaTest.class);
		suite.addTestSuite(PrincipalCurveTest.class);
		suite.addTestSuite(HPCCTest.class);
		suite.addTestSuite(CEMPCCTest.class);

		return suite;
	}

	/** Set up the suite */
	private static Test suitePrincipalCurveRWrapper() {
		// sleep a bit so logger messages don't mix with other printings
		try { Thread.sleep(100); } catch (Exception e) { }

		String s = "Testing HPCC and CEMPCC with RWrapper version of PrincipalCurve";
		Logger.INFO("MainTestSuite", s);
		Comparison.setTestTitle(s);

		// Test with PrincipalCurve R version
		PrincipalCurve.USE_ONLY_RWRAPPER = true;

		TestSuite suite = new TestSuite();

		suite.addTestSuite(PrincipalCurveTest.class);
		suite.addTestSuite(HPCCTest.class);
		suite.addTestSuite(CEMPCCTest.class);

		return suite;
	}
}
