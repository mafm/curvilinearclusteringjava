/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class PrincipalCurveTest extends TestCase {

	/** Base path for filenames */
	private static final String basePath = "data/test/principalcurve/";


	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test PrincipalCurve.getLambdaWrapper() */
	public void test_getLambdaWrapper1() {
		// input data
		double[][] x = {
			{ 1.1,  1.2 },
			{ 1.3,  1.0 },
			{ 5.5,  5.6 },
			{ 5.4,  5.7 }
		};
		double[][] curve_s = {
			{ 1.300038, 0.9999616 },
			{ 1.100055, 1.1999451 },
			{ 5.499898, 5.6001026 },
			{ 5.400009, 5.6999908 }
		};
		int[] curve_tag = null;
		double stretch = 2.0;

		// expected result
		PrincipalCurve.Result expected = new PrincipalCurve.Result();
		expected.s = new double[][] {
			{ 1.100055, 1.1999451 },
			{ 1.300038, 0.9999616 },
			{ 5.499898, 5.6001026 },
			{ 5.400009, 5.6999908 }
		};
		expected.lambda = new double[] {
			0.2827651,
			0.0000000,
			6.5053050,
			6.6465814
		};
		expected.tag = new int[] { 1, 0, 2, 3 };
		expected.dist = 2.702405e-08;

		PrincipalCurve.Result result = PrincipalCurve.getLambdaWrapper(x,
									       curve_s,
									       curve_tag,
									       stretch);

		Comparison.assertMatrixDouble("test_getLambdaWrapper1() result.s",
					      expected.s, result.s, Comparison.Error.FIVE_IN_THOUSAND);
		Comparison.assertArrayDouble("test_getLambdaWrapper1() result.lambda",
					     expected.lambda, result.lambda, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_getLambdaWrapper1() result.tag",
					  expected.tag, result.tag);
		Comparison.assertDouble("test_getLambdaWrapper1() result.dist",
					expected.dist, result.dist, Comparison.Error.FIVE_IN_THOUSAND);
	}

	/** Test PrincipalCurve.getLambdaWrapper() */
	public void test_getLambdaWrapper2() {
		// input data
		double[][] x = UtilIO.readMatrixDouble(basePath + "getlambdawrapper.in.x");
		double[][] curve_s = UtilIO.readMatrixDouble(basePath + "getlambdawrapper.in.curve_s");
		int[] curve_tag = UtilIO.readArrayInt(basePath + "getlambdawrapper.in.curve_tag");
		double stretch = 0.0;

		// expected result
		PrincipalCurve.Result expected = new PrincipalCurve.Result();
		expected.s = UtilIO.readMatrixDouble(basePath + "getlambdawrapper.out.s");
		expected.lambda = UtilIO.readArrayDouble(basePath + "getlambdawrapper.out.lambda");
		expected.tag = UtilIO.readArrayInt(basePath + "getlambdawrapper.out.tag");
		expected.dist = UtilIO.readDouble(basePath + "getlambdawrapper.out.dist");

		PrincipalCurve.Result result = PrincipalCurve.getLambdaWrapper(x,
									       curve_s,
									       curve_tag,
									       stretch);

		Comparison.assertMatrixDouble("test_getLambdaWrapper2() result.s",
					      expected.s, result.s, Comparison.Error.TWO_IN_HUNDRED);
		Comparison.assertArrayDouble("test_getLambdaWrapper2() result.lambda",
					     expected.lambda, result.lambda, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_getLambdaWrapper2() result.tag",
					  expected.tag, result.tag);
		Comparison.assertDouble("test_getLambdaWrapper2() result.dist",
					expected.dist, result.dist, Comparison.Error.ONE_IN_MILLION);
	}

	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve1() {
		// basic data
		double[][] x = {
			{ 1.1, 1.2 },
			{ 1.3, 1.0 },
			{ 5.5, 5.6 },
			{ 5.4, 5.7 }
		};
		int maxIterations = 10;
		int degreesOfFreedom5 = 5;
		int degreesOfFreedom2 = 2;

		// result 1
		PrincipalCurve.Result result1_df5 = null;
		result1_df5 = PrincipalCurve.principalCurve(x,
							    degreesOfFreedom5,
							    maxIterations);
		PrincipalCurve.Result result1_df2 = null;
		result1_df2 = PrincipalCurve.principalCurve(x,
							    degreesOfFreedom2,
							    maxIterations);

		// result 2: test defaults, should be the same as above
		PrincipalCurve.Result result2_df5 = null;
		result2_df5 = PrincipalCurve.principalCurve(x,
							    degreesOfFreedom5,
							    PrincipalCurve.DEFAULT_MAX_ITERATIONS);
		PrincipalCurve.Result result2_df2 = null;
		result2_df2 = PrincipalCurve.principalCurve(x,
							    degreesOfFreedom2,
							    PrincipalCurve.DEFAULT_MAX_ITERATIONS);

		/* expected results

		   > library(princurve)
		   > x<-rbind(c(1.1, 1.2), c(1.3, 1.0), c(5.5, 5.6), c(5.4, 5.7)); print(x)
			[,1] [,2]
		   [1,]  1.1  1.2
		   [2,]  1.3  1.0
		   [3,]  5.5  5.6
		   [4,]  5.4  5.7
		   > p<-principal.curve(x=x, smoother = "smooth.spline", stretch=2.0, thres=0.001, df=5, maxit=10); print(p)
		   There were 20 warnings (use warnings() to see them)
		   $s
			    [,1]     [,2]
		   [1,] 1.100710 1.200703
		   [2,] 1.299654 0.999658
		   [3,] 5.474014 5.660029
		   [4,] 5.426253 5.639353

		   $tag
		   [1] 2 1 4 3

		   $lambda
		   [1] 0.2828389 0.0000000 6.5326199 6.4805763

		   $dist
		   [1] 0.008647191

		   $converged
		   [1] FALSE

		   $nbrOfIterations
		   [1] 10
		   ...

		   > p<-principal.curve(x=x, smoother = "smooth.spline", stretch=2.0, thres=0.001, df=2, maxit=10); print(p)
		   $s
			    [,1]     [,2]
		   [1,] 1.203397 1.103437
		   [2,] 1.196802 1.096375
		   [3,] 5.448254 5.648331
		   [4,] 5.451546 5.651856

		   $tag
		   [1] 2 1 3 4

		   $lambda
		   [1] 0.009661757 0.000000000 6.228574627 6.233397282

		   $dist
		   [1] 0.04994176

		   $converged
		   [1] TRUE

		   $nbrOfIterations
		   [1] 2
		   ...
		*/

		/////////////////////////////////////////////////////////////////////////
		// df=5
		/////////////////////////////////////////////////////////////////////////
		PrincipalCurve.Result expected_df5 = new PrincipalCurve.Result();
		expected_df5.s = new double[][] {
			{ 1.100710, 1.200703 },
			{ 1.299654, 0.999658 },
			{ 5.474014, 5.660029 },
			{ 5.426253, 5.639353 }
		};
		expected_df5.tag = new int[] { 1, 0, 3, 2 };
		expected_df5.lambda = new double[] { 0.2828389, 0.0000000, 6.5326199, 6.4805763 };
		expected_df5.dist = 0.008647191;

		/*
		UtilDebug.print("test_principalCurve1(), expected_df5.s", expected_df5.s);
		UtilDebug.print("test_principalCurve1(), result1_df5.s", result1_df5.s);
		UtilDebug.print("test_principalCurve1(), result2_df5.s", result2_df5.s);

		UtilDebug.print("test_principalCurve1(), expected_df5.tag", expected_df5.tag);
		UtilDebug.print("test_principalCurve1(), result1_df5.tag", result1_df5.tag);
		UtilDebug.print("test_principalCurve1(), result2_df5.tag", result2_df5.tag);

		UtilDebug.print("test_principalCurve1(), expected_df5.lambda", expected_df5.lambda);
		UtilDebug.print("test_principalCurve1(), result1_df5.lambda", result1_df5.lambda);
		UtilDebug.print("test_principalCurve1(), result2_df5.lambda", result2_df5.lambda);
		*/

		// compare result1_df5
		Comparison.assertMatrixDouble("test_principalCurve1() result1_df5.s",
					      expected_df5.s, result1_df5.s, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve1() result1_df5.tag",
					  expected_df5.tag, result1_df5.tag);
		Comparison.assertArrayDouble("test_principalCurve1() result1_df5.lambda",
					     expected_df5.lambda, result1_df5.lambda, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertDouble("test_principalCurve1(), result1_df5.dist",
					expected_df5.dist, result1_df5.dist, Comparison.Error.ONE_IN_TEN_THOUSAND);

		// compare result2_df5
		Comparison.assertMatrixDouble("test_principalCurve1() result2_df5.s",
					      expected_df5.s, result2_df5.s, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve1() result2_df5.tag",
					  expected_df5.tag, result2_df5.tag);
		Comparison.assertArrayDouble("test_principalCurve1() result2_df5.lambda",
					     expected_df5.lambda, result2_df5.lambda, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertDouble("test_principalCurve1(), result2_df5.dist",
					expected_df5.dist, result2_df5.dist, Comparison.Error.ONE_IN_TEN_THOUSAND);

		/////////////////////////////////////////////////////////////////////////
		// df=2
		/////////////////////////////////////////////////////////////////////////
		PrincipalCurve.Result expected_df2 = new PrincipalCurve.Result();
		expected_df2.s = new double[][] {
			{ 1.203397, 1.103437 },
			{ 1.196802, 1.096375 },
			{ 5.448254, 5.648331 },
			{ 5.451546, 5.651856 }
		};
		expected_df2.tag = new int[] { 1, 0, 2, 3 };
		expected_df2.lambda = new double[] { 0.009661757, 0.000000000, 6.228574627, 6.233397282 };
		expected_df2.dist = 0.04994176;

		/*
		UtilDebug.print("test_principalCurve1(), expected_df2.s", expected_df2.s);
		UtilDebug.print("test_principalCurve1(), result1_df2.s", result1_df2.s);
		UtilDebug.print("test_principalCurve1(), result2_df2.s", result2_df2.s);

		UtilDebug.print("test_principalCurve1(), expected_df2.tag", expected_df2.tag);
		UtilDebug.print("test_principalCurve1(), result1_df2.tag", result1_df2.tag);
		UtilDebug.print("test_principalCurve1(), result2_df2.tag", result2_df2.tag);

		UtilDebug.print("test_principalCurve1(), expected_df2.lambda", expected_df2.lambda);
		UtilDebug.print("test_principalCurve1(), result1_df2.lambda", result1_df2.lambda);
		UtilDebug.print("test_principalCurve1(), result2_df2.lambda", result2_df2.lambda);
		*/

		// compare result1_df2
		Comparison.assertMatrixDouble("test_principalCurve1() result1_df2.s",
					      expected_df2.s, result1_df2.s, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve1() result1_df2.tag",
					  expected_df2.tag, result1_df2.tag);
		Comparison.assertArrayDouble("test_principalCurve1() result1_df2.lambda",
					     expected_df2.lambda, result1_df2.lambda, Comparison.Error.FIVE_IN_TEN_THOUSAND);
		Comparison.assertDouble("test_principalCurve1(), result1_df2.dist",
					expected_df2.dist, result1_df2.dist, Comparison.Error.ONE_IN_TEN_THOUSAND);

		// compare result2_df2
		Comparison.assertMatrixDouble("test_principalCurve1() result2_df2.s",
					      expected_df2.s, result2_df2.s, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve1() result2_df2.tag", expected_df2.tag, result2_df2.tag);
		Comparison.assertArrayDouble("test_principalCurve1() result2_df2.lambda",
					     expected_df2.lambda, result2_df2.lambda, Comparison.Error.FIVE_IN_TEN_THOUSAND);
		Comparison.assertDouble("test_principalCurve1(), result2_df2.dist",
					expected_df2.dist, result2_df2.dist, Comparison.Error.ONE_IN_TEN_THOUSAND);
	}

	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve2() {
		// basic data
		double[][] x = {
			{ 2, 12 },
			{ 5, 35 },
			{ 3, 23 },
			{ 4, 41 },
			{ 1, 21 }
		};
		int maxIterations = 10;
		int degreesOfFreedom = 4;

		// result1
		PrincipalCurve.Result result1 = null;
		result1 = PrincipalCurve.principalCurve(x,
							degreesOfFreedom,
							maxIterations);

		// result2: test defaults, should be the same as above
		PrincipalCurve.Result result2 = null;
		result2 = PrincipalCurve.principalCurve(x,
							degreesOfFreedom,
							PrincipalCurve.DEFAULT_MAX_ITERATIONS);

		/* expected results

		   > library(princurve)
		   > x<-rbind(c(2, 12), c(5, 35), c(3, 23), c(4, 41), c(1, 21)); print(x)
			[,1] [,2]
		   [1,]    2   12
		   [2,]    5   35
		   [3,]    3   23
		   [4,]    4   41
		   [5,]    1   21
		   > p<-principal.curve(x=x, smoother = "smooth.spline", stretch=2.0, thres=0.001, df=4, maxit=10); print(p)
		   $s
			    [,1]     [,2]
		   [1,] 1.868070 11.99755
		   [2,] 4.861192 34.99058
		   [3,] 2.466423 23.11596
		   [4,] 4.100361 41.01268
		   [5,] 1.702916 20.87385

		   $tag
		   [1] 1 5 3 2 4

		   $lambda
		   [1]  0.000000 23.360071 11.246388 29.430046  8.877843

		   $dist
		   [1] 0.8551571

		   $converged
		   [1] TRUE

		   $nbrOfIterations
		   [1] 4
		   ...
		*/

		PrincipalCurve.Result expectedResult = new PrincipalCurve.Result();
		expectedResult.s = new double[][] {
			{ 1.86768, 12.01837 },
			{ 4.86119, 34.99054 },
			{ 2.46643, 23.11597 },
			{ 4.10180, 41.00131 },
			{ 1.70291, 20.87382 },
		};
		expectedResult.tag = new int[] { 0, 4, 2, 1, 3 };
		expectedResult.lambda = new double[] { 0.000000, 23.36004, 11.2464, 29.43005, 8.87781 };
		expectedResult.dist = 0.8551571;

		/*
		UtilDebug.print("test_principalCurve2(), expectedResult.s", expectedResult.s);
		UtilDebug.print("test_principalCurve2(), result1.s", result1.s);
		UtilDebug.print("test_principalCurve2(), result2.s", result2.s);

		UtilDebug.print("test_principalCurve2(), expectedResult.tag", expectedResult.tag);
		UtilDebug.print("test_principalCurve2(), result1.tag", result1.tag);
		UtilDebug.print("test_principalCurve2(), result2.tag", result2.tag);

		UtilDebug.print("test_principalCurve2(), expectedResult.lambda", expectedResult.lambda);
		UtilDebug.print("test_principalCurve2(), result1.lambda", result1.lambda);
		UtilDebug.print("test_principalCurve2(), result2.lambda", result2.lambda);
		*/

		// compare result1
		Comparison.assertMatrixDouble("test_principalCurve2() result1.s",
					      expectedResult.s, result1.s, Comparison.Error.FIVE_IN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve2() result1.tag",
					  expectedResult.tag, result1.tag);
		Comparison.assertArrayDouble("test_principalCurve2() result1.lambda",
					     expectedResult.lambda, result1.lambda, Comparison.Error.FIVE_IN_MILLION);
		Comparison.assertDouble("test_principalCurve2(), result1.dist",
					expectedResult.dist, result1.dist, Comparison.Error.ONE_IN_MILLION);

		// compare result2
		Comparison.assertMatrixDouble("test_principalCurve2() result2.s",
					      expectedResult.s, result2.s, Comparison.Error.FIVE_IN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve2() result2.tag",
					  expectedResult.tag, result2.tag);
		Comparison.assertArrayDouble("test_principalCurve2() result2.lambda",
					     expectedResult.lambda, result2.lambda, Comparison.Error.FIVE_IN_MILLION);
		Comparison.assertDouble("test_principalCurve2(), result2.dist",
					expectedResult.dist, result2.dist, Comparison.Error.ONE_IN_MILLION);
	}

	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve_point_of_cempcc() {
		// basic data
		double[][] x = UtilIO.readMatrixDouble(basePath + "data-lastpoint-cluster5-cempcc-failure");
		int degreesOfFreedom = 4;
		int maxIterations = 5;

		// result
		PrincipalCurve.Result result = PrincipalCurve.principalCurve(x,
									     degreesOfFreedom,
									     maxIterations);

		PrincipalCurve.Result expected = new PrincipalCurve.Result();
		expected.s = UtilIO.readMatrixDouble(basePath + "data-lastpoint-cluster5-cempcc-failure-curve_s");
		expected.lambda = UtilIO.readArrayDouble(basePath + "data-lastpoint-cluster5-cempcc-failure-curve_lambda");
		expected.tag = UtilIO.readArrayInt(basePath + "data-lastpoint-cluster5-cempcc-failure-curve_tag");
		expected.dist = UtilIO.readDouble(basePath + "data-lastpoint-cluster5-cempcc-failure-curve_dist");

		/*
		UtilDebug.print("test_principalCurve_point_of_cempcc(), expected.s", expected.s);
		UtilDebug.print("test_principalCurve_point_of_cempcc(), expected.lambda", expected.lambda);
		UtilDebug.print("test_principalCurve_point_of_cempcc(), expected.tag", expected.tag);

		UtilDebug.print("test_principalCurve_point_of_cempcc(), result.s", result.s);
		UtilDebug.print("test_principalCurve_point_of_cempcc(), result.lambda", result.lambda);
		UtilDebug.print("test_principalCurve_point_of_cempcc(), result.tag", result.tag);
		*/

		// compare result Java
		Comparison.assertMatrixDouble("test_principalCurve_point_of_cempcc() result.s)",
					      expected.s, result.s, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayDouble("test_principalCurve_point_of_cempcc() result.lambda)",
					     expected.lambda, result.lambda, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt("test_principalCurve_point_of_cempcc() result.tag)",
					  expected.tag, result.tag);
		Comparison.assertDouble("test_principalCurve_point_of_cempcc(), result.dist",
					expected.dist, result.dist, Comparison.Error.ONE_IN_TEN_THOUSAND);
	}

	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve_data2d() {
		helper_test_principalCurve("data2d");
	}

	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve_data3d() {
		helper_test_principalCurve("data3d");
	}

	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve_data5d() {
		helper_test_principalCurve("data5d");
	}

	/** Helper */
	private void helper_test_principalCurve(String dataName) {
		final String baseName = basePath + dataName + "-";
		final String testName = "test_principalCurve_" + dataName + "()";

		// basic data
		double[][] x = UtilIO.readMatrixDouble(baseName + "in.x");
		int degreesOfFreedom = UtilIO.readInt(baseName + "in.df");

		// expected result
		PrincipalCurve.Result expected = new PrincipalCurve.Result();
		expected.s = UtilIO.readMatrixDouble(baseName + "out.s");
		expected.lambda = UtilIO.readArrayDouble(baseName + "out.lambda");
		expected.tag = UtilIO.readArrayInt(baseName + "out.tag");
		expected.dist = UtilIO.readDouble(baseName + "out.dist");

		// result
		PrincipalCurve.Result result = PrincipalCurve.principalCurve(x,
									     degreesOfFreedom,
									     PrincipalCurve.DEFAULT_MAX_ITERATIONS);

		// compare result
		//
		// mafm: given the non-stability of sortdi algorithm in Fortran
		// of R princurve package, despite claiming so, the results of
		// the tag are not exactly the same when the lambda elements are
		// exactly the same.  So we just compare the sorted lambda
		// instead, to not get errors when there are just nuisances.
		Comparison.assertMatrixDouble(testName + " result.s",
					      expected.s, result.s, Comparison.Error.ONE_IN_TEN_THOUSAND);
		/*
		Comparison.assertArrayDouble(testName + " result.lambda",
						expected.lambda, result.lambda, Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertArrayInt(testName + " result.tag",
					     expected.tag, result.tag);
		*/
		Comparison.assertArrayDouble(testName + " result.lambda sorted",
					     Util.getSorted(expected.lambda, expected.tag),
					     Util.getSorted(result.lambda, result.tag),
					     Comparison.Error.ONE_IN_TEN_THOUSAND);
		Comparison.assertDouble(testName + ", result.dist",
					expected.dist, result.dist, Comparison.Error.ONE_IN_TEN_THOUSAND);
	}
}
