/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.lang.Math;
import java.io.*;


/** Helper class to compare results
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class Comparison {

	/** Encodes several typical error rates allowed */
	public class Error {
		/** Error rate */
		public static final double TWO_IN_HUNDRED = 0.02;
		/** Error rate */
		public static final double ONE_IN_HUNDRED = 0.01;
		/** Error rate */
		public static final double FIVE_IN_THOUSAND = 0.005;
		/** Error rate */
		public static final double ONE_IN_THOUSAND = 0.001;
		/** Error rate */
		public static final double FIVE_IN_TEN_THOUSAND = 0.0005;
		/** Error rate */
		public static final double ONE_IN_TEN_THOUSAND = 0.0001;
		/** Error rate */
		public static final double ONE_IN_HUNDRED_THOUSAND = 0.00001;
		/** Error rate */
		public static final double FIVE_IN_MILLION = 0.000005;
		/** Error rate */
		public static final double ONE_IN_MILLION = 0.000001;
		/** Error rate */
		public static final double ZERO = 0.0;
	}

	/** PrintWriter to write errors */
	private static PrintWriter errorPrinter = null;

	/** Title for current test, so in the case of errors we write the
	 * current test before (to help identify in which round of test it
	 * happens) */
	private static String testTitle = null;


	/** Set PrintWriter */
	public static void setPrintWriter(PrintWriter pw) {
		errorPrinter = pw;
	}

	/** Set a title for current test, so in the case of errors we write the
	 * current test before (to help identify in which round of test it
	 * happens) */
	public static void setTestTitle(String s) {
		testTitle = s;
	}

	/** Helper function to compare data */
	public static void assertInt(String message, int expected, int result) {
		if (expected != result) {
			printError("assertInt()", message, String.valueOf(expected), String.valueOf(result));
		}
	}

	/** Helper function to compare arrays */
	public static void assertArrayInt(String message, int[] expected, int[] result) {
		if (expected == null && result == null) {
			// considered success
			return;
		} else if (isArrayCompatible(expected, result)) {
			int errorCounter = 0;
			for (int i = 0; i < result.length; ++i) {
				if (expected[i] != result[i]) {
					printError("assertArrayInt()", message + " at " + i,
						   String.valueOf(expected[i]), String.valueOf(result[i]));
					++errorCounter;
				}
			}

			if (errorCounter > 0) {
				printError("assertArrayInt(): " + message + ": "
					   + errorCounter + " errors found");
			}
		}
	}

	/** Helper function to compare data */
	public static void assertDouble(String message, double expected, double result, double allowedError) {
		double currentError = getErrorMagnitude(expected, result);
		if (currentError > allowedError) {
			printError("assertDouble()", message,
				   String.valueOf(expected), String.valueOf(result), getErrorPercentageString(currentError));
		}
	}

	/** Helper function to compare arrays */
	public static void assertArrayDouble(String message, double[] expected, double[] result, double allowedError) {
		if (expected == null && result == null) {
			// considered success
			return;
		} else if (isArrayCompatible(expected, result)) {
			int errorCounter = 0;
			double biggestError = 0.0;
			for (int i = 0; i < result.length; ++i) {
				double currentError = getErrorMagnitude(expected[i], result[i]);
				if (currentError > allowedError) {
					printError("assertArrayDouble()", message + " at " + i,
						   String.valueOf(expected[i]),
						   String.valueOf(result[i]),
						   getErrorPercentageString(currentError));
					++errorCounter;

					if (currentError > biggestError) {
						biggestError = currentError;
					}
				}
			}

			if (errorCounter > 0) {
				printError("assertArrayDouble(): " + message + ": "
					   + errorCounter + " errors found, biggest error: " + getErrorPercentageString(biggestError));
			}
		}
	}

	/** Helper function to compare arrays */
	public static void assertMatrixDouble(String message, double[][] expected, double[][] result, double allowedError) {
		if (expected == null && result == null) {
			// considered success
			return;
		} else if (isArrayCompatible(expected, result)) {
			int errorCounter = 0;
			double biggestError = 0.0;
			for (int i = 0; i < result.length; ++i) {
				for (int j = 0; j < result[i].length; ++j) {
					double currentError = getErrorMagnitude(expected[i][j], result[i][j]);
					if (currentError > allowedError) {
						printError("assertMatrixDouble()",
							   message + " at (" + i + ", " + j + ")",
							   String.valueOf(expected[i][j]),
							   String.valueOf(result[i][j]),
							   getErrorPercentageString(currentError));
						++errorCounter;

						if (currentError > biggestError) {
							biggestError = currentError;
						}
					}
				}
			}

			if (errorCounter > 0) {
				printError("assertMatrixDouble(): " + message + ": "
					   + errorCounter + " errors found, biggest error: " + getErrorPercentageString(biggestError));
			}
		}
	}

	/** Print error (generic message)
	 *
	 * @param s String to print
	 */
	private static void printError(String s) {
		if (testTitle != null) {
			errorPrinter.println(testTitle);
			testTitle = null;
		}
		errorPrinter.println(s);
		errorPrinter.flush();
	}

	/** Print error (without error difference)
	 *
	 * @param source Source function
	 * @param Message Message from the test
	 * @param expected Expected value
	 * @param result Result obtained
	 */
	private static void printError(String source, String message, String expected, String result) {
		printError(source, message, expected, result, null);
	}

	/** Print error (with error difference)
	 *
	 * @param source Source function
	 * @param Message Message from the test
	 * @param expected Expected value
	 * @param result Result obtained
	 * @param error Error difference obtained
	 */
	private static void printError(String source, String message, String expected, String result, String error) {
		if (testTitle != null) {
			errorPrinter.println(testTitle);
			testTitle = null;
		}
		String s = source + ": " + message
			+ ", expected=<" + expected
			+ ">, result=<" + result + ">"
			+ (error == null ? "" : ", error=<" + error + ">");

		errorPrinter.println(s);
		errorPrinter.flush();
	}

	/** Helper function to compare data
	 *
	 * We use this for "allowedError" as parameter of other functions to
	 * work as as a kind of percentage of the magnitude of the value to
	 * compare, taking it as absolute isn't very fair/accurate for the
	 * purposes of comparison.  For that, we take the absolute of the
	 * difference between values to compare, then get the absolute of the
	 * biggest result (this way the result of the division is bigger), and
	 * check if the result is bigger than the allowed error.
	 *
	 * Example: getErrorMagnitude(1, 1.1) would see that the difference
	 * between values is 0.1, and would be 0.1/1 = 0.1 (10%) off the initial
	 * target.
	 *
	 *
	 * @param expected One of the values to compare
	 * @param result Another of the values to compare
	 *
	 * @return The magnitude of the error
	 */
	private static double getErrorMagnitude(double expected, double result) {
		double currentError = Math.abs(expected - result);
		if (currentError < 1E-15) {
			// error too insignificant for any purpose
			return 0.0;
		} else {
			double biggestQuantity = Math.max(Math.abs(expected), Math.abs(result));
			return (currentError/biggestQuantity);
		}
	}

	/** Helper function to convert error to string percentage
	 *
	 * @param error Error to be processed
	 *
	 * @return The percentage of the error, in string
	 */
	private static String getErrorPercentageString(double error) {
		return String.format("%g%%", error*100.0);
	}

	/** Helper function to compare array compatibility (int[] version)
	 *
	 * @param a Array to be compared
	 * @param b Array to be compared
	 *
	 * @return true when the arrays are compatible
	 */
	private static boolean isArrayCompatible(int[] a, int[] b) {
		if (a == null && b == null) {
			// considered success
			return true;
		} else if ((a == null && b != null) || (a != null && b == null)) {
			return false;
		}

		if (a.length != b.length) {
			return false;
		} else {
			return true;
		}
	}

	/** Helper function to compare array compatibility (double[] version)
	 *
	 * @param a Array to be compared
	 * @param b Array to be compared
	 *
	 * @return true when the arrays are compatible
	 */
	private static boolean isArrayCompatible(double[] a, double[] b) {
		if (a == null && b == null) {
			// considered success
			return true;
		} else if ((a == null && b != null) || (a != null && b == null)) {
			return false;
		}

		if (a.length != b.length) {
			return false;
		} else {
			return true;
		}
	}

	/** Helper function to compare array compatibility (double[] version)
	 *
	 * @param a Array to be compared
	 * @param b Array to be compared
	 *
	 * @return true when the arrays are compatible
	 */
	private static boolean isArrayCompatible(double[][] a, double[][] b) {
		if (a == null && b == null) {
			// considered success
			return true;
		} else if ((a == null && b != null) || (a != null && b == null)) {
			return false;
		}

		if (a.length != b.length && a[0].length != b[0].length) {
			return false;
		} else {
			return true;
		}
	}
}
