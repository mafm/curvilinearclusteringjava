/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class CEMPCCTest extends TestCase {

	/** Base path for filenames */
	private static final String basePath = "data/test/cempcc/";


	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test CEMPCC.cem() */
	public void test_cem_curves2d_noforced() {
		helper_test_cem("curves2d_noforced");
	}

	/** Test CEMPCC.cem() */
	public void test_cem_curves2d_forced6() {
		helper_test_cem("curves2d_forced6");
	}

	/** Test CEMPCC.cem() */
	public void test_cem_data3d() {
		helper_test_cem("data3d");
	}

	/** Test CEMPCC.cem() */
	public void test_cem_data5d() {
		helper_test_cem("data5d");
	}

	/** Test CEMPCC.cem() */
	public void test_cem_arcs3d() {
		helper_test_cem("arcs3d");
	}

	/** Test CEMPCC.cem() */
	public void test_cem_arcs3d_noise() {
		helper_test_cem("arcs3d_noise");
	}

	/** Helper */
	public void helper_test_cem(String dataName) {
		final String baseName = basePath + dataName + "-cempcc-";
		final String testName = "test_cem_" + dataName + "()";

		// error admitted depending on version of lower level functions
		// used
		double ERROR_ADMITTED = 0.0;
		if (PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD) {
			ERROR_ADMITTED = Comparison.Error.ONE_IN_HUNDRED_THOUSAND;
		} else {
			ERROR_ADMITTED = Comparison.Error.ONE_IN_MILLION;
		}

		// data
		double[][] points = UtilIO.readMatrixDouble(baseName + "in.x");
		int[] initialClustering = UtilIO.readArrayInt(baseName + "in.initclust");
		int degreesOfFreedom = UtilIO.readInt(baseName + "in.df");

		// expected result
		CEMPCC.Result expected = new CEMPCC.Result();
		expected.varBound = UtilIO.readDouble(baseName + "out.cem_varbound");
		expected.iterations = UtilIO.readInt(baseName + "out.cem_iterations");
		expected.clustering = UtilIO.readArrayInt(baseName + "out.cem_clustering");
		expected.bestClustering = UtilIO.readArrayInt(baseName + "out.cem_bestclustering");
		expected.clusteringLog = UtilIO.readArrayInt(baseName + "out.cem_clusteringlog");
		expected.likelihoodsAbout = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoodsabout");
		expected.likelihoodsAlong = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoodsalong");
		expected.likelihoods = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoods");
		expected.likelihoodsMix = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoodsmix");
		expected.overallLogLikelihood = UtilIO.readArrayDouble(baseName + "out.cem_ovloglike");
		expected.allCurveVars = UtilIO.readMatrixDouble(baseName + "out.cem_allcurvevars");
		expected.allCurveLengths = UtilIO.readMatrixDouble(baseName + "out.cem_allcurvelengths");
		expected.allMeanAlong = UtilIO.readMatrixDouble(baseName + "out.cem_allmeanalong");
		expected.allVarAlong = UtilIO.readMatrixDouble(baseName + "out.cem_allvaralong");
		expected.allMixProp = UtilIO.readMatrixDouble(baseName + "out.cem_allmixprop");
		// expected result 2
		CEMPCC.CalculateLikelihoodResult expected2 = new CEMPCC.CalculateLikelihoodResult();
		expected2.score = UtilIO.readDouble(baseName + "out.calc_score");
		expected2.bic = UtilIO.readDouble(baseName + "out.calc_bic");
		expected2.bic2 = UtilIO.readDouble(baseName + "out.calc_bic2");

		// calculations
		CEMPCC.Result result = CEMPCC.cem(points,
						  degreesOfFreedom,
						  initialClustering,
						  CEMPCC.DEFAULT_MAX_ITERATIONS,
						  CEMPCC.DEFAULT_TRIM,
						  CEMPCC.DEFAULT_MIX,
						  CEMPCC.DEFAULT_VAR_BOUND);
		// calculations 2
		CEMPCC.CalculateLikelihoodResult result2 = CEMPCC.calculateLikelihood(result, true);

		// compare results
		Comparison.assertInt(testName + " degreesOfFreedom",
				     degreesOfFreedom, result.degreesOfFreedom);
		Comparison.assertDouble(testName + " varBound",
					expected.varBound, result.varBound, ERROR_ADMITTED);
		Comparison.assertInt(testName + " iterations",
				     expected.iterations, result.iterations);

		Comparison.assertArrayInt(testName + " clustering",
					  expected.clustering, result.clustering);
		Comparison.assertArrayInt(testName + " bestClustering",
					  expected.bestClustering, result.bestClustering);

		// compare intermediate calculations only if we use svd() from
		// R, otherwise they differ because initial values of principal
		// curves et all are quite different
		if (!PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD) {
			Comparison.assertArrayInt(testName + " clusteringLog",
						  expected.clusteringLog, result.clusteringLog);

			Comparison.assertMatrixDouble(testName + " likelihoodsAbout",
						      expected.likelihoodsAbout, result.likelihoodsAbout, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " likelihoodsAlong",
						      expected.likelihoodsAlong, result.likelihoodsAlong, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " likelihoods",
						      expected.likelihoods, result.likelihoods, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " likelihoodsMix",
						      expected.likelihoodsMix, result.likelihoodsMix, ERROR_ADMITTED);
			Comparison.assertArrayDouble(testName + " overallLogLikelihood",
						     expected.overallLogLikelihood, result.overallLogLikelihood, ERROR_ADMITTED);

			Comparison.assertMatrixDouble(testName + " allCurveVars",
						      expected.allCurveVars, result.allCurveVars, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allCurveLengths",
						      expected.allCurveLengths, result.allCurveLengths, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allMeanAlong",
						      expected.allMeanAlong, result.allMeanAlong, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allVarAlong",
						      expected.allVarAlong, result.allVarAlong, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allMixProp",
						      expected.allMixProp, result.allMixProp, ERROR_ADMITTED);
		}

		// compare results 2
		Comparison.assertDouble(testName + " score",
					expected2.score, result2.score, ERROR_ADMITTED);
		Comparison.assertDouble(testName + " bic",
					expected2.bic, result2.bic, ERROR_ADMITTED);
		Comparison.assertDouble(testName + " bic2",
					expected2.bic2, result2.bic2, ERROR_ADMITTED);
	}
}
