/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;
import java.util.HashMap;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class HPCCTest extends TestCase {

	/** Base path for filenames */
	private static final String basePath = "data/test/hpcc/";


	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test HPCC.vdist() */
	public void test_vdist() {
		double[] p1 = { 4.505369e-05, -0.0046000690 };
		double[] p2 = { 4.569075e-05, -0.0046662670 };
		double[] p3 = { 3.674143e-04, -0.0375711600 };
		double[] p4 = { 7.535985e-06, -0.0007728143 };
		double[] p5 = { 4.126828e-05, -0.0042331240 };
		double[][] A = { p1, p2, p3, p4, p5 };

		double[] p6 = { 5.329031e-05, -0.00236833 };
		double[][] B = { p6, p6, p6, p6, p6 };

		double expectedResult = 0.0012556252;
		double result = HPCC.vdist(A, B);

		Comparison.assertDouble("test_vdist()",
					expectedResult, result, 0.000001);
	}

	/** Test HPCC.clustVarSpline() */
	public void test_clustVarSpline() {
		double[] p1  = { 17.07120500, -36.9239200 };
		double[] p2  = { 12.33668100, -38.2609500 };
		double[] p3  = { 11.47636500, -35.7336000 };
		double[] p4  = { 13.44792400, -42.4995200 };
		double[] p5  = { 16.31672900, -45.9350600 };
		double[] p6  = { 15.85465300, -49.0904600 };
		double[] p7  = { 17.60504500, -55.1896800 };
		double[] p8  = { 18.09267600, -51.3675600 };
		double[] p9  = { 19.44410200, -31.0419800 };
		double[] p10 = { 10.42611400, -40.7611900 };
		double[][] points = {
			p1,
			p2,
			p3,
			p4,
			p5,
			p6,
			p7,
			p8,
			p9,
			p10
		};
		double expectedResult = 27.3416;
		double result1 = HPCC.clustVarSpline(points, 5, 0.4);
		double result2 = HPCC.clustVarSpline(points,
						     HPCC.DEFAULT_DEGREES_OF_FREEDOM,
						     HPCC.DEFAULT_ALPHA);

		// mafm: error admission changed from 0.0001 to 0.0002
		//
		// - expected result, from R:	27.3416
		// - with Java code:		27.341422382803913
		// - with Java+R wrapper:	27.34159947176866
		Comparison.assertDouble("test_clustVarSpline() result1",
					expectedResult, result1, 0.0002);
		Comparison.assertDouble("test_clustVarSpline() result2",
					expectedResult, result2, 0.0002);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_curves2d_noforce() {
		helper_test_hpcc("curves2d", false, 0);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_curves2d_force6() {
		helper_test_hpcc("curves2d", true, 6);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_data3d_noforce() {
		helper_test_hpcc("data3d", false, 0);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_data3d_force7() {
		helper_test_hpcc("data3d", true, 7);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_data5d_noforce() {
		helper_test_hpcc("data5d", false, 0);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_data5d_force7() {
		helper_test_hpcc("data5d", true, 7);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_arcs3d_noforce() {
		helper_test_hpcc("arcs3d", false, 0);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_arcs3d_force3() {
		helper_test_hpcc("arcs3d", true, 3);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_arcs3d_noise_noforce() {
		helper_test_hpcc("arcs3d_noise", false, 0);
	}

	/** Test HPCC.hpcc() */
	public void test_hpcc_arcs3d_noise_force4() {
		helper_test_hpcc("arcs3d_noise", true, 4);
	}

	/** Helper */
	private void helper_test_hpcc(String dataName, boolean force, int forcenum) {
		final String baseName = basePath + dataName + "-hpcc-";
		final String variety = (force ? ("force" + forcenum) : "noforce");
		final String baseNameResult = baseName + variety + "-";
		final String testName = "test_hpcc_" + dataName + "_" + variety + "()";

		// data
		double[][] points = UtilIO.readMatrixDouble(baseName + "in.x");
		int[] initialClustering = UtilIO.readArrayInt(baseName + "in.initclust");
		int degreesOfFreedom = UtilIO.readInt(baseName + "in.df");

		// expected result, final clustering
		int[] expectedResult = UtilIO.readArrayInt(baseNameResult + "out.finalclust");

		HPCC.Result result = HPCC.hpcc(points,
					       initialClustering,
					       HPCC.DEFAULT_ALPHA,
					       degreesOfFreedom,
					       force,
					       forcenum);

		// mafm: sometimes clusters are the same, but just have
		// different "names" (integer numbers), so we attempt to fix
		// this glitch

		// first we check that the number of clusters is the same
		int[] uniqueClustersExpected = Util.unique(expectedResult);
		int[] uniqueClustersObtained = Util.unique(result.classes);
		if (uniqueClustersExpected.length != uniqueClustersObtained.length) {
			Comparison.assertInt(testName + " number of clusters differ",
					     uniqueClustersExpected.length, uniqueClustersObtained.length);
		}

		// then we try to find the correspondence/mapping between
		// clusters: if expected cluster point[i] is 'n', then their
		// "alias" of the cluster in the result we assume that it's the
		// cluster of result point[i], hoping that it's always the same
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < result.classes.length; ++i) {
			Integer oldValue = map.put(expectedResult[i], result.classes[i]);
			if (oldValue != null && oldValue != result.classes[i]) {
				// ops, we had already mapped it, but had a
				// different value
				Comparison.assertInt(testName + " mapping of clusters are not exact",
						     oldValue.intValue(), result.classes[i]);
			} else {
				// we replace the value, hoping that it's a
				// perfect mapping between both results
				result.classes[i] = expectedResult[i];
			}
		}

		// now we make the comparison based on the "aliased" cluster
		// names: for all points, if point[i] has clusterA in the
		// expected result, then point[i] of the result must be the
		// alias clusterA'
		Comparison.assertArrayInt(testName + " result (possibly changing cluster numbers)", expectedResult, result.classes);
	}
}
