/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;
import java.util.Arrays;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class QuicksortWithOrderTest extends TestCase {

	/** Number of elements for the arrays in these tests */
	private static final int NUMBER_OF_ELEMENTS = 512*1024;


	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test QuickSortWithOrder.swap*() */
	public void test_swap() {
		double[] arrayUnsorted = { 3.45, 1.23, 4.56, 2.34 };
		double[] arraySorted = { 1.23, 2.34, 3.45, 4.56 };

		// in-place swap
		double[] array = Arrays.copyOf(arrayUnsorted, arrayUnsorted.length);
		QuicksortWithOrder.swap(array, 0, 1);
		QuicksortWithOrder.swap(array, 1, 3);
		QuicksortWithOrder.swap(array, 2, 3);
		Comparison.assertArrayDouble("test_swap(array[], pos1, pos2)", arraySorted, array, Comparison.Error.ZERO);

		// external order swap
		int[] order = { 0, 1, 2, 3};
		QuicksortWithOrder.swapOrder(order, 0, 1);
		QuicksortWithOrder.swapOrder(order, 1, 3);
		QuicksortWithOrder.swapOrder(order, 2, 3);
		double[] arrayIndirectSorted = new double[arrayUnsorted.length];
		for (int i = 0; i < arrayUnsorted.length; ++i) {
			arrayIndirectSorted[i] = arrayUnsorted[order[i]];
		}
		Comparison.assertArrayDouble("test_swap(array[], pos1, pos2)", arraySorted, arrayIndirectSorted, Comparison.Error.ZERO);
	}

	/** Test QuickSortWithOrder.quicksort() */
	public void test_quicksort() {
		// generating random list
		double[] list = new double[NUMBER_OF_ELEMENTS];
		for (int i = 0; i < list.length; ++i) {
			list[i] = (double)((int)(Math.random() * NUMBER_OF_ELEMENTS) % NUMBER_OF_ELEMENTS);
		}

		double[] listJFC = null;
		{
			listJFC = Arrays.copyOf(list, list.length);

			// mafm: for benchmarking purposes
			//Timer timer = new Timer();
			Arrays.sort(listJFC);
			//Logger.DEBUG("QuickSortWithOrderTest, Arrays.sort()", " - done in " + timer.elapsedMillis());
		}


		// QuicksortWithOrder ***recursive***
		{
			double[] listMAFMrecursive = Arrays.copyOf(list, list.length);

			QuicksortWithOrder.quicksortRec(listMAFMrecursive, 0, listMAFMrecursive.length-1);

			Comparison.assertArrayDouble("test_quicksort(): ***recursive*** sort",
						     listJFC, listMAFMrecursive, Comparison.Error.ZERO);
		}


		// QuicksortWithOrder ***non recursive***
		{
			double[] listMAFM = Arrays.copyOf(list, list.length);

			QuicksortWithOrder.quicksort(listMAFM);

			Comparison.assertArrayDouble("test_quicksort(): ***non-recursive*** sort",
						     listJFC, listMAFM, Comparison.Error.ZERO);
		}


		// QuicksortWithOrder ***external order[]***
		{
			double[] listMAFMorder = Arrays.copyOf(list, list.length);

			int[] order = QuicksortWithOrder.quicksortWithOrder(listMAFMorder);

			// need to copy onto an ordered array for the next
			// comparison of Array.equals(), otherwise we can
			// compare with:
			//
			// for(...) { listMAFMorder[order[i]] == listJFC[i] }
			double[] listMAFMorderInPlace = new double[listMAFMorder.length];
			for (int i = 0; i < listMAFMorderInPlace.length; ++i) {
				listMAFMorderInPlace[i] = listMAFMorder[order[i]];
			}

			Comparison.assertArrayDouble("test_quicksort(): ***external order*** sort",
						     listJFC, listMAFMorderInPlace, Comparison.Error.ZERO);
		}
	}
}
