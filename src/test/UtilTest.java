/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;
import java.util.Arrays;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class UtilTest extends TestCase {

	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test Util.power2(double) */
	public void test_power2() {
		Comparison.assertDouble("power2(3.141592)=9.869600", 9.869600, Util.power2(3.141592), Comparison.Error.ONE_IN_MILLION);
		Comparison.assertDouble("power2(5432)=29506624", 29506624, Util.power2(5432), Comparison.Error.ONE_IN_MILLION);
	}

	/** Test Util.power2(double[]) */
	public void test_power2_array() {
		double[] array1 = { 0.0, 1.0, 2.0, 3.0, 4.0 };
		double[] array1Result = Util.power2(array1);
		double[] array1Expected = { 0.0, 1.0, 4.0, 9.0, 16.0 };
		Comparison.assertArrayDouble("power2(array1)", array1Expected, array1Result, Comparison.Error.ZERO);

		double[] array2 = { -0.432, 3.412, -9.546, 8.149 };
		double[] array2Result = Util.roundWithFormat(Util.power2(array2), "#.######");
		double[] array2Expected = { 0.186624, 11.641744, 91.126116, 66.406201 };
		Comparison.assertArrayDouble("power2(array2)", array2Expected, array2Result, Comparison.Error.ZERO);
	}

	/** Test Util.var(double[]) */
	public void test_var() {
		double[] array1 = { 0.0, 1.0, 2.0, 3.0, 4.0 };
		double array1Result = Util.roundWithFormat(Util.var(array1), "#.######");
		double array1Expected = 2.5;
		Comparison.assertDouble("var(" + Arrays.toString(array1) + ")=" + array1Expected, array1Expected, array1Result, Comparison.Error.ZERO);

		double[] array2 = { -0.432, 3.412, -9.546, 8.149 };
		double array2Result = Util.roundWithFormat(Util.var(array2), "#.######");
		double array2Expected = 56.244738;
		Comparison.assertDouble("var(" + Arrays.toString(array2) + ")=" + array2Expected, array2Expected, array2Result, Comparison.Error.ZERO);
	}

	/** Test Util.sum(double[]) */
	public void test_sum_doublearray() {
		double[] array1 = { 0.0, 1.0, 2.0, 3.0, 4.0 };
		Comparison.assertDouble("sum(double[])=10.0", 10.0, Util.sum(array1), Comparison.Error.ONE_IN_MILLION);

		double[] array2 = { 0.432, 3.412, 9.546, 8.149 };
		Comparison.assertDouble("sum(double[])=21.539", 21.539, Util.sum(array2), Comparison.Error.ONE_IN_MILLION);
	}

	/** Test Util.mean(double[]) */
	public void test_mean_doublearray() {
		double[] array1 = { 0.0, 1.0, 2.0, 3.0, 4.0 };
		Comparison.assertDouble("mean(double[])=2.0", 2.0, Util.mean(array1), Comparison.Error.ONE_IN_MILLION);

		double[] array2 = { 0.432, 3.412, 9.546, 8.149 };
		Comparison.assertDouble("mean(double[])=5.38475", 5.38475, Util.mean(array2), Comparison.Error.ONE_IN_MILLION);
	}

	/** Test Util.min(int[]) */
	public void test_min_intarray() {
		int[] array1 = { 2, 4, 3, 1 };
		Comparison.assertInt("min(int[])=1", 1, Util.min(array1));

		int[] array2 = { -1, 2, 4, 3 };
		Comparison.assertInt("min(int[])=-1", -1, Util.min(array2));

		int[] array3 = { -1, 2, -24, 3, 5 };
		Comparison.assertInt("min(int[])=-24", -24, Util.min(array3));
	}

	/** Test Util.max(int[]) */
	public void test_max_intarray() {
		int[] array1 = { 2, 4, 3, 5 };
		Comparison.assertInt("max(int[])=5", 5, Util.max(array1));

		int[] array2 = { 23, 4, 3, 5 };
		Comparison.assertInt("max(int[])=23", 23, Util.max(array2));

		int[] array3 = { -1, 2, 124, 3, 5 };
		Comparison.assertInt("max(int[])=124", 124, Util.max(array3));
	}

	/** Test Util.max(double[]) */
	public void test_max_doublearray() {
		double[] array1 = { 2.2, 4.4, 3.3, 1.1, 5.5 };
		Comparison.assertDouble("max(double[])=5.5", 5.5, Util.max(array1), Comparison.Error.ZERO);

		double[] array2 = { 23.23, 2.2, 4.4, 3.3, 5.5 };
		Comparison.assertDouble("max(double[])=23.23", 23.23, Util.max(array2), Comparison.Error.ZERO);

		double[] array3 = { -1.1, 2.2, 124.124, 3.3, 5.5 };
		Comparison.assertDouble("max(double[])=124.124", 124.124, Util.max(array3), Comparison.Error.ZERO);
	}

	/** Test Util.max(double[], start, end) */
	public void test_max_doublearraystartend() {
		double[] array1 = { 2.2, 4.4, 3.3, 1.1, 5.5 };
		Comparison.assertDouble("max(double[], start, end)=5.5", 5.5, Util.max(array1, 0, array1.length-1), Comparison.Error.ZERO);

		double[] array2 = { 9.9, 7.7, 23.23, 2.2, 4.4, 3.3, 5.5 };
		Comparison.assertDouble("max(double[], start, end)=23.23", 23.23, Util.max(array2, 1, 3), Comparison.Error.ZERO);

		double[] array3 = { -1.1, 2.2, 3.3, 5.5 };
		Comparison.assertDouble("max(double[], start, end)=-1.1", -1.1, Util.max(array3, 0, 0), Comparison.Error.ZERO);
	}

	/** Test Util.range(double[]) */
	public void test_range_doublearray() {
		double[] array1 = { 2.2, 4.4, 3.3, 1.1, 5.5 };
		Comparison.assertDouble("range(double[]).min=1.1", 1.1, Util.range(array1)[0], Comparison.Error.ZERO);
		Comparison.assertDouble("range(double[]).max=5.5", 5.5, Util.range(array1)[1], Comparison.Error.ZERO);

		double[] array2 = { 9.9, 7.7, 23.23, 2.2, 4.4, 3.3, 5.5 };
		Comparison.assertDouble("range(double[]).min=2.2", 2.2, Util.range(array2)[0], Comparison.Error.ZERO);
		Comparison.assertDouble("range(double[]).max=23.23", 23.23, Util.range(array2)[1], Comparison.Error.ZERO);

		double[] array3 = { -1.1, 2.2, 3.3, 5.5 };
		Comparison.assertDouble("range(double[]).min=-1.1", -1.1, Util.range(array3)[0], Comparison.Error.ZERO);
		Comparison.assertDouble("range(double[]).max=5.5", 5.5, Util.range(array3)[1], Comparison.Error.ZERO);
	}

	/** Test Util.range(int[]) */
	public void test_range_intarray() {
		int[] array1 = { 2, 4, 3, 1, 5 };
		Comparison.assertInt("range(int[]).min=1", 1, Util.range(array1)[0]);
		Comparison.assertInt("range(int[]).max=5", 5, Util.range(array1)[1]);

		int[] array2 = { 9, 7, 23, 2, 4, 3, 5 };
		Comparison.assertInt("range(int[]).min=2", 2, Util.range(array2)[0]);
		Comparison.assertInt("range(int[]).max=23", 23, Util.range(array2)[1]);

		int[] array3 = { -1, 2, 3, 5 };
		Comparison.assertInt("range(int[]).min=-1", -1, Util.range(array3)[0]);
		Comparison.assertInt("range(int[]).max=5", 5, Util.range(array3)[1]);
	}

	/** Test Util.unique(int[]) */
	public void test_unique_intarray() {
		int[] array1 = { 1, 2, 3, 4, 5 };
		int[] array1Unique = { 1, 2, 3, 4, 5 };
		int[] array1Result = Util.unique(array1);
		Comparison.assertArrayInt("unique(int[] array1)", array1Unique, array1Result);

		int[] array2 = { 1, 2, 3, 4, 5, 4, 3, 2, 1 };
		int[] array2Unique = { 1, 2, 3, 4, 5 };
		int[] array2Result = Util.unique(array2);
		Comparison.assertArrayInt("unique(int[] array2)", array2Unique, array2Result);

		int[] array3 = { 5, 1, 2, 3, 4, 3, 2, 1, 1, 2, 3, 4, 5 };
		int[] array3Unique = { 5, 1, 2, 3, 4 };
		int[] array3Result = Util.unique(array3);
		Comparison.assertArrayInt("unique(int[] array3)", array3Unique, array3Result);
	}

	/** Test Util.countElements[Not]Matching(int[], target) */
	public void test_countElementsMatching() {
		int[] array1 = { 1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5 };

		Comparison.assertInt("countElementsMatching(int[], 1)=5", 5, Util.countElementsMatching(array1, 1));
		Comparison.assertInt("countElementsMatching(int[], 2)=4", 4, Util.countElementsMatching(array1, 2));
		Comparison.assertInt("countElementsMatching(int[], 3)=3", 3, Util.countElementsMatching(array1, 3));
		Comparison.assertInt("countElementsMatching(int[], 4)=2", 2, Util.countElementsMatching(array1, 4));
		Comparison.assertInt("countElementsMatching(int[], 5)=1", 1, Util.countElementsMatching(array1, 5));
		Comparison.assertInt("countElementsMatching(int[], 6)=0", 0, Util.countElementsMatching(array1, 6));

		Comparison.assertInt("countElementsNotMatching(int[], 1)=10", 10, Util.countElementsNotMatching(array1, 1));
		Comparison.assertInt("countElementsNotMatching(int[], 2)=11", 11, Util.countElementsNotMatching(array1, 2));
		Comparison.assertInt("countElementsNotMatching(int[], 3)=12", 12, Util.countElementsNotMatching(array1, 3));
		Comparison.assertInt("countElementsNotMatching(int[], 4)=13", 13, Util.countElementsNotMatching(array1, 4));
		Comparison.assertInt("countElementsNotMatching(int[], 5)=14", 14, Util.countElementsNotMatching(array1, 5));
		Comparison.assertInt("countElementsNotMatching(int[], 6)=15", 15, Util.countElementsNotMatching(array1, 6));
	}

	/** Test Util.outer(double[], double[]) */
	public void test_outer() {
		double[] array1 = { 0.0, 1.0, 2.0, 3.0, 4.0 };
		double[] array2 = { -0.432, 3.412, -9.546, 8.149 };

		double[][] result = Util.outer(array1, array2);
		for (int i = 0; i < result.length; ++i) {
			result[i] = Util.roundWithFormat(result[i], "#.###");

		}
		double[][] expected = {
			{  0.000,  0.000,   0.000,  0.000 },
			{ -0.432,  3.412,  -9.546,  8.149 },
			{ -0.864,  6.824, -19.092, 16.298 },
			{ -1.296, 10.236, -28.638, 24.447 },
			{ -1.728, 13.648, -38.184, 32.596 }
		};

		Comparison.assertMatrixDouble("outer(double[], double[])", expected, result, Comparison.Error.ZERO);
	}

	/** Test getRowsOfGivenClasses(double[][], classes[], targetClasses[]) */
	public void test_getRowsOfGivenClasses() {
		double[] p1  = {  1,   1,   1 };
		double[] p2  = {  2,   2,   2 };
		double[] p3  = { -3,  -3,  -3 };
		double[] p11 = { 11, -11,  11 };
		double[] p22 = {-22,  22, -22 };
		double[][] points = { p1, p2, p3, p11, p22 };

		int[] classes = { 1, 2, 3, 1, 2 };

		int[] target1 = { 1, 2, 3 };
		double[][] result1 = { p1, p2, p3, p11, p22 };
		Comparison.assertMatrixDouble("getRowsOfGivenClasses(double[][], classes[], targetClasses[]), target1",
					      result1, Util.getRowsOfGivenClasses(points, classes, target1), Comparison.Error.ZERO);

		int[] target2 = { 1, 3 };
		double[][] result2 = { p1, p3, p11 };
		Comparison.assertMatrixDouble("getRowsOfGivenClasses(double[][], classes[], targetClasses[]), target2",
					      result2, Util.getRowsOfGivenClasses(points, classes, target2), Comparison.Error.ZERO);

		int[] target3 = { 3 };
		double[][] result3 = { p3 };
		Comparison.assertMatrixDouble("getRowsOfGivenClasses(double[][], classes[], targetClasses[]), target3",
					      result3, Util.getRowsOfGivenClasses(points, classes, target3), Comparison.Error.ZERO);

		int[] target4 = { 1, 2 };
		double[][] result4 = { p1, p2, p11, p22 };
		Comparison.assertMatrixDouble("getRowsOfGivenClasses(double[][], classes[], targetClasses[]), target4",
					      result4, Util.getRowsOfGivenClasses(points, classes, target4), Comparison.Error.ZERO);

		int[] target5 = { 0 };
		double[][] result5 = null;
		Comparison.assertMatrixDouble("getRowsOfGivenClasses(double[][], classes[], targetClasses[]), target5",
					      result5, Util.getRowsOfGivenClasses(points, classes, target5), Comparison.Error.ZERO);
	}

	/** Test getSortedArray(double[], order[]) */
	public void test_getSortedArray() {
		double[] array  = {  1.1,  2.2, -3.3, 5.5, -7.7 };
		int[] order = { 4, 2, 0, 1, 3 };

		double[] arrayExpected = Arrays.copyOf(array, array.length);
		Arrays.sort(arrayExpected);

		Comparison.assertArrayDouble("getSortedArray(double[], order[])",
					     arrayExpected, Util.getSorted(array, order), Comparison.Error.ZERO);
	}

	/** Test getColumnAsArray(double[][], index) */
	public void test_getColumnAsArray() {
		double[] p1  = {  1,   1,   1 };
		double[] p2  = {  2,   2,   2 };
		double[] p3  = { -3,  -3,  -3 };
		double[] p11 = { 11, -11,  11 };
		double[] p22 = {-22,  22, -22 };
		double[][] points = { p1, p2, p3, p11, p22 };

		int index = -1;

		index = 0;
		double[] result1expected = { p1[index], p2[index], p3[index], p11[index], p22[index] };
		Comparison.assertArrayDouble("getColumnAsArray(double[][], index), test1",
					     result1expected, Util.getColumnAsArray(points, index), Comparison.Error.ZERO);

		index = 1;
		double[] result2expected = { p1[index], p2[index], p3[index], p11[index], p22[index] };
		Comparison.assertArrayDouble("getColumnAsArray(double[][], index), test2",
					     result2expected, Util.getColumnAsArray(points, index), Comparison.Error.ZERO);

		index = 2;
		double[] result3expected = { p1[index], p2[index], p3[index], p11[index], p22[index] };
		Comparison.assertArrayDouble("getColumnAsArray(double[][], index), test3",
					     result3expected, Util.getColumnAsArray(points, index), Comparison.Error.ZERO);
	}

	/** Test diagVar(double[][]) */
	public void test_diagVar() {
		double[][] array1 = { { 1.1, 2.2, 3.3 },
				      { 5.5, 6.6, 4.4 } };
		double[] result1expected = { 9.68, 9.68, 0.605 };
		double result1sumexpected = 19.965;
		double[] result1 = Util.roundWithFormat(Util.diagVar(array1), "#.######");
		double result1sum = Util.roundWithFormat(Util.sum(result1), "#.######");
		Comparison.assertArrayDouble("diagVar(double[][]) test1",
					     result1expected, result1, Comparison.Error.ZERO);
		Comparison.assertDouble("diagVar(double[][]), result1sum",
					result1sumexpected, result1sum, Comparison.Error.ZERO);


		double[][] array2 = { { 876.54, 228.22 },
				      { 641.32, 632.93 },
				      { 175.57, 466.54 } };
		double[] result2expected = { 127268.408633, 41378.706433 };
		double result2sumexpected = 168647.115066;
		double[] result2 = Util.roundWithFormat(Util.diagVar(array2), "#.######");
		double result2sum = Util.roundWithFormat(Util.sum(result2), "#.######");
		Comparison.assertArrayDouble("diagVar(double[][]) test2",
					     result2expected, result2, Comparison.Error.ZERO);
		Comparison.assertDouble("diagVar(double[][]), result2sum",
					result2sumexpected, result2sum, Comparison.Error.ZERO);


		double[][] array3 = { { -351.421, -932.623 },
				      { -254.517,  620.346 } };
		double[] result3expected = { 4695.192608, 1205856.357481 };
		double result3sumexpected = 1210551.550089;
		double[] result3 = Util.roundWithFormat(Util.diagVar(array3), "#.######");
		double result3sum = Util.roundWithFormat(Util.sum(result3), "#.######");
		Comparison.assertArrayDouble("diagVar(double[][]) test3",
					     result3expected, result3, Comparison.Error.ZERO);
		Comparison.assertDouble("diagVar(double[][]), result3sum",
					result3sumexpected, result3sum, Comparison.Error.ZERO);
	}

	/** Test meanOfColumns(double[][]) */
	public void test_meanOfColumns() {
		double[][] array1 = { { 1.1, 2.2, 3.3 },
				      { 5.5, 6.6, 4.4 } };
		double[] result1expected = { 3.30, 4.40, 3.85 };
		double[] result1 = Util.roundWithFormat(Util.meanOfColumns(array1), "#.######");
		Comparison.assertArrayDouble("meanOfColumns(double[][]) test1",
					     result1expected, result1, Comparison.Error.ZERO);


		double[][] array2 = { { 876.54, 228.22 },
				      { 641.32, 632.93 },
				      { 175.57, 466.54 } };
		double[] result2expected = { 564.476667, 442.563333 };
		double[] result2 = Util.roundWithFormat(Util.meanOfColumns(array2), "#.######");
		Comparison.assertArrayDouble("meanOfColumns(double[][]) test2",
					     result2expected, result2, Comparison.Error.ZERO);


		double[][] array3 = { { -351.421, -932.623 },
				      { -254.517,  620.346 } };
		double[] result3expected = { -302.9690, -156.1385 };
		double[] result3 = Util.roundWithFormat(Util.meanOfColumns(array3), "#.######");
		Comparison.assertArrayDouble("meanOfColumns(double[][]) test3",
					     result3expected, result3, Comparison.Error.ZERO);
	}

	/** Test roundWithFormat() */
	public void test_roundingWithFormat() {
		double[] result1expected = { 3.301, -4.405, 3.854 };
		double[] result1 = Util.roundWithFormat(new double[] { 3.301, -4.404892, 3.853980}, "#.###");
		Comparison.assertArrayDouble("roundWithFormat(double[]) test1",
					     result1expected, result1, Comparison.Error.ZERO);


		double result2expected = -192833.308213;
		double result2 = Util.roundWithFormat(-192833.3082131111, "#.######");
		Comparison.assertDouble("roundWithFormat(double) test2",
					result2expected, result2, Comparison.Error.ZERO);
	}
}
