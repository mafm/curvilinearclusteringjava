/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;


/** Quicksort algorithm but maintaining the original array intact, and returning
 * an array with the indices of the sorted original array.  It needs to be so to
 * avoid modifying too much the rest of the code, although currently is much
 * slower than Arrays.sort() or even the basic recursive implementation here,
 * and similar to the non-recursive one (so probably the non-recursive part is
 * to blame for this).
 *
 * The rest of the algorithms are implemented for reference, but probably won't
 * be used.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class QuicksortWithOrder {

	/** Quicksort algorithm (non recursive)
	 *
	 * @param array The array to swap the elements
	 */
	public static void quicksort(double[] array) {
		quicksortNonRec(array, 0, array.length - 1);
	}

	/** Quicksort algorithm (with external order instead of in-place
	 * swapping, non recursive)
	 *
	 * @param array The array with the contents
	 *
	 * @return The order, the array to swap the elements
	 */
	public static int[] quicksortWithOrder(double[] array) {
		// initially should be {0, 1, ..., n}
		int[] order = new int[array.length];
		for (int i = 0; i < order.length; ++i) {
			order[i] = i;
		}

		quicksortWithOrderNonRec(array, order, 0, array.length - 1);

		return order;
	}

	/** Basic quicksort algorithm (recursive)
	 *
	 * @param array The array to swap the elements
	 * @param start Position to start sorting
	 * @param end Position to end sorting
	 */
	public static void quicksortRec(double[] array, int start, int end) {
		if (start < end) {
			double pivot = array[start];

			int left = start, right = end;

			while (right > left) {
				while (array[left] <= pivot && left < right && left <= end)
					++left;
				while (array[right] > pivot && left <= right && right >= start)
					--right;
				if (right > left) {
					swap(array, left, right);
				}
			}
			swap(array, start, right);

			quicksortRec(array, start, right - 1);
			quicksortRec(array, right + 1, end);
		}
	}

	/** Quicksort algorithm (non recursive)
	 *
	 * @param array The array to swap the elements
	 * @param startNR Position to start sorting
	 * @param endNR Position to end sorting
	 */
	private static void quicksortNonRec(double[] array, int startNR, int endNR) {
		Stack<Integer> startStack = new Stack<Integer>();
		Stack<Integer> endStack = new Stack<Integer>();

		startStack.push(startNR);
		endStack.push(endNR);

		while (!startStack.isEmpty()) {
			int start = startStack.pop(), end = endStack.pop();
			if (start >= end)
				continue;

			int left = start, right = end;
			double pivot = array[left];
			//Logger.INFO("QuicksortWithOrder.quicksortNonRec()", "using left|right: " + left + "|" + right + " for " + Arrays.toString(array));

			while (right > left) {
				while (array[left] <= pivot && left < right)
					++left;
				while (array[right] > pivot && left <= right)
					--right;
				if (right > left) {
					swap(array, left, right);
				}
			}
			swap(array, start, right);

			startStack.push(start);
			endStack.push(right-1);
			startStack.push(right+1);
			endStack.push(end);
		}
	}

	/** Quicksort algorithm (with external order instead of in-place
	 * swapping, non recursive)
	 *
	 * @param array The array with the contents
	 *
	 * @param order The array to swap the elements, initially should be {0,
	 * 1, ..., n}
	 *
	 * @param startNR Position to start sorting
	 *
	 * @param endNR Position to end sorting
	 */
	private static void quicksortWithOrderNonRec(double[] array, int[] order, int startNR, int endNR) {
		Stack<Integer> startStack = new Stack<Integer>();
		Stack<Integer> endStack = new Stack<Integer>();

		startStack.push(startNR);
		endStack.push(endNR);

		while (!startStack.isEmpty()) {
			int start = startStack.pop(), end = endStack.pop();
			if (start >= end)
				continue;

			int left = start, right = end;
			double pivot = array[order[left]];
			//Logger.INFO("QuicksortWithOrder.quicksortNonRec()", "using left|right: " + left + "|" + right + " for " + Arrays.toString(array));

			while (right > left) {
				while (array[order[left]] <= pivot && left < right)
					++left;
				while (array[order[right]] > pivot && left <= right)
					--right;
				if (right > left) {
					swapOrder(order, left, right);
				}
			}
			swapOrder(order, start, right);

			startStack.push(start);
			endStack.push(right-1);
			startStack.push(right+1);
			endStack.push(end);
		}
	}

	/** Swap elements in place
	 *
	 * @param array The array to swap the elements
	 * @param pos1 Position of the 1st element to swap
	 * @param pos2 Position of the 2nd element to swap
	 */
	public static void swap(double[] array, int pos1, int pos2) {
		double aux = array[pos1];
		array[pos1] = array[pos2];
		array[pos2] = aux;
	}

	/** Swap elements in the index (order[] array)
	 *
	 * @param order The array to swap the elements
	 * @param pos1 Position of the 1st element to swap
	 * @param pos2 Position of the 2nd element to swap
	 */
	public static void swapOrder(int[] order, int pos1, int pos2) {
		int aux = order[pos1];
		order[pos1] = order[pos2];
		order[pos2] = aux;
	}
}
