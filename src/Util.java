/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.text.DecimalFormat;


/** Utility numerical functions used in many places, or implementation of
 * complex functions readily available in R as simple function calls (so the
 * code using them is not affected or obfuscated by the calculations).
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class Util {

	/** Calculates x^2
	 *
	 * @param x Value on which to perform the operation
	 * @return Result of the operation
	 */
	public static double power2(double x) {
		return (x * x);
	}


	/** Calculates x^2
	 *
	 * @param Array Value on which to perform the operation
	 * @return Result of the operation
	 */
	public static double[] power2(double[] array) {
		double[] result = new double[array.length];
		for (int i = 0; i < result.length; ++i) {
			result[i] = array[i] * array[i];
		}
		return result;
	}


	/** Calculates variance of x
	 *
	 * @param Array Value on which to perform the operation
	 * @return Result of the operation
	 *
	 *
	 * Note: Regarding Util.sum(result)/(double)(result.length-1), which
	 * could have result.length in the denominator according to some
	 * definitions and therefore the expression equivalent to
	 * Util.mean(result), the thing is that var() in R calculates things
	 * this way.  From R var() documentation:
	 *
	 * The denominator n - 1 is used which gives an unbiased estimator of
	 * the (co)variance for i.i.d. observations.  These functions return
	 * ‘NA’ when there is only one observation (whereas S-PLUS has been
	 * returning ‘NaN’), and fail if ‘x’ has length zero.
	 *
	 */
	public static double var(double[] array) {
		double mean = Util.mean(array);
		double[] result = new double[array.length];
		for (int i = 0; i < result.length; ++i) {
			result[i] = Util.power2(array[i] - mean);
		}
		return Util.sum(result)/(double)(result.length-1);
	}


	/** Calculates the sum of the given array
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static double sum(double[] array) {
		double total = 0.0;
		for (double v : array) {
			total += v;
		}
		return total;
	}


	/** Calculates the mean of the given array
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static double mean(double[] array) {
		if (array.length == 0) {
			return 0.0;
		} else {
			return (sum(array) / (double)array.length);
		}
	}


	/** Returns the minimum value of the given array
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static int min(int[] array) {
		int minValue = array[0];
		for (int i = 1; i < array.length; ++i) {
			if (array[i] < minValue) {
				minValue = array[i];
			}
		}
		return minValue;
	}


	/** Returns the maximum value of the given array
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static int max(int[] array) {
		int maxValue = array[0];
		for (int i = 1; i < array.length; ++i) {
			if (array[i] > maxValue) {
				maxValue = array[i];
			}
		}
		return maxValue;
	}


	/** Returns the maximum value of the given array
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static double max(double[] array) {
		return max(array, 0, array.length-1);
	}


	/** Returns the maximum value of the given array, between 'start' and
	 * 'end' (both included)
	 *
	 * @param array Array on which to perform the operation
	 * @param start Start at this element (included)
	 * @param end End at this element (included)
	 * @return Result of the operation
	 */
	public static double max(double[] array, int start, int end) {
		double maxValue = array[start];
		for (int i = start+1; i <= end; ++i) {
			if (array[i] > maxValue) {
				maxValue = array[i];
			}
		}
		return maxValue;
	}


	/** Returns the minimum and maximum value of the given array, resulting
	 * { minValue, maxValue }
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static double[] range(double[] array) {
		double minValue = array[0];
		double maxValue = array[0];
		for (int i = 0; i < array.length; ++i) {
			double v = array[i];
			if (v < minValue) {
				minValue = v;
			}
			if (v > maxValue) {
				maxValue = v;
			}
		}
		double[] result = { minValue, maxValue };
		return result;
	}


	/** Returns the minimum and maximum value of the given array, resulting
	 * { minValue, maxValue }
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static int[] range(int[] array) {
		int minValue = array[0];
		int maxValue = array[0];
		for (int i = 0; i < array.length; ++i) {
			int v = array[i];
			if (v < minValue) {
				minValue = v;
			}
			if (v > maxValue) {
				maxValue = v;
			}
		}
		int[] result = { minValue, maxValue };
		return result;
	}


	/** Returns an array with unique values of the given array
	 *
	 * @param array Array on which to perform the operation
	 * @return Result of the operation
	 */
	public static int[] unique(int[] array) {
		// we use a set to ensure uniqueness, and array list to return
		// the array sorted as it goes, not by values
		HashSet<Integer> set = new HashSet<Integer>();
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		for (int elem : array) {
			boolean added = set.add(elem);
			if (added) {
				// false if repeated
				arrayList.add(elem);
			}
		}
		int[] result = new int[arrayList.size()];
		for (int i = 0; i < result.length; ++i) {
			result[i] = arrayList.get(i).intValue();
		}
		return result;
	}


	/** Count elements matching
	 *
	 * @param array Array on which to perform the operation
	 * @param target Target to match
	 * @return Result of the operation
	 */
	public static int countElementsMatching(int[] array, int target) {
		int counter = 0;
		for (int elem : array) {
			if (target == elem) {
				++counter;
			}
		}
		return counter;
	}


	/** Count elements NOT matching
	 *
	 * @param array Array on which to perform the operation
	 * @param target Target to match
	 * @return Result of the operation
	 */
	public static int countElementsNotMatching(int[] array, int target) {
		int counter = 0;
		for (int elem : array) {
			if (target != elem) {
				++counter;
			}
		}
		return counter;
	}


	/** Get outer product
	 *
	 * @param a Array a
	 * @param b Array b
	 *
	 * @return Result of the operation
	 */
	public static double[][] outer(double[] a, double[] b) {
		double[][] result = new double[a.length][b.length];
		for (int i = 0; i < result.length; ++i) {
			for (int j = 0; j < result[i].length; ++j) {
				result[i][j] = a[i] * b[j];
			}
		}
		return result;
	}


	/** Get only rows/points matching given classes.  It's a way to filter
	 * the results and getting only points matching interesting criteria.
	 *
	 * @param points Array of points as base collection
	 * @param classArray Array of classes of the given points
	 * @param targetClasses Target classes to match
	 *
	 * @return Result of the operation
	 */
	public static double[][] getRowsOfGivenClasses(double[][] points,
						       int[] classArray,
						       int[] targetClasses) {
		if (points.length != classArray.length) {
			String msg = "can't be performed when arrays have different length";
			throw new RuntimeException(msg);
		}

		Vector<double[]> vector = new Vector<double[]>();
		for (int i = 0; i < classArray.length; ++i) {
			for (int j = 0; j < targetClasses.length; ++j) {
				if (classArray[i] == targetClasses[j]) {
					vector.add(points[i]);
				}
			}
		}

		if (vector.size() > 0) {
			double[][] result = new double[vector.size()][points[0].length];
			vector.toArray(result);
			return result;
		} else {
			return null;
		}
	}


	/** Get the array sorted according to the order that the second array
	 * provides.
	 *
	 * @param array Array of data
	 * @param order Array with the order of the data
	 *
	 * @return Result of the operation
	 */
	public static double[] getSorted(double[] array, int[] order) {
		double[] arrayCopy = new double[array.length];

		for (int i = 0; i < array.length; ++i) {
			arrayCopy[i] = array[order[i]];
		}

		return arrayCopy;
	}

	/** Get the column of a matrix as an array (similar operation to
	 * indexing a complete row of the array, e.g. array[0]), which can't be
	 * done directly in the language.
	 *
	 * @param array Array of 2-dimensional data
	 * @param index Index of the column to get
	 *
	 * @return Result of the operation
	 */
	public static double[] getColumnAsArray(double[][] array, int index) {
		double[] result = new double[array.length];
		for (int i = 0; i < result.length; ++i) {
			result[i] = array[i][index];
		}
		return result;
	}


	/** Diagonal of R var() (variance/covariance matrix)
	 *
	 * Used in "principal.curve": dist.old <- sum(diag(var(x)))
	 *
	 * @param array Multidimensional array of data
	 * @return Result of the operation
	 */
	public static double[] diagVar(double[][] array) {
		int nRows = array.length;
		int nCols = array[0].length;

		double[] result = new double[nCols];
		for (int col = 0; col < nCols; ++col) {
			// get an array with the values of a column
			double[] colArray = new double[nRows];
			double colArraySum = 0.0;
			for (int row = 0; row < nRows; ++row) {
				colArray[row] = array[row][col];
				colArraySum += array[row][col];
			}
			double colArrayMean = colArraySum / (double)(nRows);

			// for each value, Xi=(Xi-mean(X))^2
			for (int row = 0; row < nRows; ++row) {
				colArray[row] = Util.power2(colArray[row] - colArrayMean);
			}

			// result = sum(Xi) / (length(X)-1)
			result[col] = Util.sum(colArray) / (double)(colArray.length - 1);
		}

		return result;
	}


	/** Mean of columns: apply(x, 2, "mean")
	 *
	 * Used in "principal.curve": xbar <- apply(x, 2, "mean")
	 *
	 * @param array Multidimensional array of data
	 * @return Result of the operation
	 */
	public static double[] meanOfColumns(double[][] array) {
		int nRows = array.length;
		int nCols = array[0].length;

		double[] result = new double[nCols];
		for (int col = 0; col < nCols; ++col) {
			// collect the sum of each column
			result[col] = 0.0;
			for (int row = 0; row < nRows; ++row) {
				result[col] += array[row][col];
			}

			// divide by the rows to get the mean of each column
			result[col] /= (double)nRows;
		}

		return result;
	}


	/** Round doubles with specific format, format being of type "#.##" for
	 * two decimal places
	 *
	 * @param value Value to be formatted
	 * @param strFormat String to specify the format
	 * @return Result of the operation
	 */
	public static double roundWithFormat(double value, String strFormat) {
		return Double.valueOf(new DecimalFormat(strFormat).format(value));
	}


	/** Round doubles with specific format, format being of type "#.##" for
	 * two decimal places
	 *
	 * @param array Array of values to be formatted
	 * @param strFormat String to specify the format
	 * @return Result of the operation
	 */
	public static double[] roundWithFormat(double[] values, String strFormat) {
		DecimalFormat df = new DecimalFormat(strFormat);
		for (int i = 0; i < values.length; ++i) {
			values[i] = Double.valueOf(df.format(values[i]));
		}
		return values;
	}
}
