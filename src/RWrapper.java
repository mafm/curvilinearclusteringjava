/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.*;


/** Class acting as wrapper for R interpreter, performing the necessary
 * conversions between the Java code and the results from R.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class RWrapper {

	/** Singleton instance */
	private static RWrapper _instance;

	/** R invocation */
	private static final String command = "/usr/bin/R";
	/** R invocation */
	private static final String[] args = { "--vanilla", "--silent" };
	/** R invocation */
	private static final String dirName = "/dev/shm/RWrapper";
	/** R invocation */
	private static Process rProcess;
	/** R invocation serial */
	private static long serial = 0;
	/** R invocation output dir */
	private static File dir = null;


	/** Access to the singleton instance */
	public synchronized static RWrapper instance() {
		if (_instance == null) {
			_instance = new RWrapper();
		}
		return _instance;
	}

	/** Constructor, private so it's initialized only once in the
	 * Singleton */
	private RWrapper() {
		try {
			rProcess = new ProcessBuilder(command, args[0], args[1]).start();

			OutputStream stdin = rProcess.getOutputStream();
			String s = "library(princurve);\n";
			stdin.write(s.getBytes());
			stdin.flush();

		} catch (Exception e) {
			throw new RuntimeException("RWrapper(): "  + e.getMessage());
		}
	}

	/** Stop the R engine (destroy process) */
	public void finalize() {
		if (rProcess != null) {
			instance().rProcess.destroy();
			rProcess = null;
		}
	}

	/** Access to the serial */
	public static synchronized String getSerial() {
		return String.format("%06d", serial++);
	}

	/** Redirect available output to the given file
	 *
	 * @param in Input to read
	 * @param out PrintWriter to write to
	 */
	private static void redirectOutput(InputStream in, PrintWriter out) {
		try {
			for (int b = 0; in.available() > 0 && b != -1; b = in.read()) {
				if (b > 0) {
					out.print((char)b);
				}
			}
			out.flush();
		} catch (Exception e) {
			throw new RuntimeException("RWrapper.redirectOutput(): "  + e.getMessage());
		}
	}

	/** Create the directory name, wipe old contents if there are some
	 *
	 * @param dirname Name of the directory
	 * @return File representing the directory
	 */
	private static File wipeAndCreateDir(String dirname) {
		//Logger.INFO("RWrapper", "Removing dir '" + dirname + "' and contents, creating it anew");
		try {
			File dir = new File(dirname);
			if (dir.isDirectory()) {
				for (File e : dir.listFiles()) {
					e.delete();
				}
				// no need to delete between invocations
				//dir.delete();
			} else if (!dir.exists()) {
				dir.mkdir();
			}
		} catch (Exception e) {
			throw new RuntimeException("RWrapper.wipeAndCreateDir(): "  + e.getMessage());
		}

		return dir;
	}

	/** Execute commands, block until result is ready (so the callers can
	 * safely read the output)
	 *
	 * @param pathname Pathname of the files related to the execution
	 *
	 * @param source Source file to execute (adding the 'file ready part'
	 * instead of repeating it in every function calling this one)
	 */
	private void execute(String pathname, PrintWriter source) {
		try {
			// file descriptors of the process
			OutputStream stdin = rProcess.getOutputStream();
			InputStream stdout = rProcess.getInputStream();
			InputStream stderr = rProcess.getErrorStream();

			// push commands and add file to know when calculations
			// are ready
			File ready = new File(pathname + "R-ready");
			source.println("file.create('" + ready + "');");
			source.flush();
			source.close();
			String s = "source('" + pathname + "source', echo=TRUE);\n";
			stdin.write(s.getBytes());
			stdin.flush();

			// log stdout & stderr
			PrintWriter writerOut = new PrintWriter(pathname + "R-output");
			PrintWriter writerErr = new PrintWriter(pathname + "R-error");

			// wait for the file-flag telling that the operation is ready
			do {
				// log stdout & stderr
				redirectOutput(stdout, writerOut);
				redirectOutput(stderr, writerErr);

				Thread.sleep(1, 000000);
			} while (!ready.exists());

			// close our redirections of stdout & stderr
			writerOut.close();
			writerErr.close();

		} catch (Exception e) {
			throw new RuntimeException("RWrapper.execute(): "  + e.getMessage());
		}
	}

	/** Version invoking R instead of doing it natively
	 *
	 * @see PrincipalCurve.getLambdaWrapper()
	 */
	public static PrincipalCurve.Result getLambdaWrapper(double[][] x,
							     double[][] s,
							     int[] tag,
							     double stretch) {
		//mafm: for benchmarking purposes
		//Timer timer = new Timer();

		// processing tag, not always valid
		if (tag == null) {
			tag = new int[s.length];
			for (int i = 0; i < tag.length; ++i) {
				tag[i] = i + 1;
			}
		} else {
			// R indices start in 1...
			for (int i = 0; i < tag.length; ++i) {
				tag[i] += 1;
			}
		}

		PrincipalCurve.Result result = new PrincipalCurve.Result();

		try {
			// base file
			dir = wipeAndCreateDir(dirName);
			String tmp = dirName + "/" + getSerial() + "-get.lam-";

			File source = new File(tmp + "source");
			File in_x = new File(tmp + "in.x");
			File in_s = new File(tmp + "in.s");
			File in_tag = new File(tmp + "in.tag");
			File out_s = new File(tmp + "out.s");
			File out_lambda = new File(tmp + "out.lambda");
			File out_tag = new File(tmp + "out.tag");
			File out_dist = new File(tmp + "out.dist");

			UtilIO.writeMatrixDouble(in_x.toString(), x);
			UtilIO.writeMatrixDouble(in_s.toString(), s);
			UtilIO.writeArrayInt(in_tag.toString(), tag);

			// write source file to be interpreted by R
			PrintWriter sourceWriter = new PrintWriter(source);
			// mafm: the method with matrix(scan(...)) is much more efficient
			//sourceWriter.println("x<-as.matrix(read.table('" + in_x + "'))");
			//sourceWriter.println("s<-as.matrix(read.table('" + in_s + "'))");
			//sourceWriter.println("tag<-as.matrix(read.table('" + in_tag + "'))");
			sourceWriter.println("x<-matrix(scan('" + in_x + "')," + x.length + "," + x[0].length + ", byrow=TRUE)");
			sourceWriter.println("s<-matrix(scan('" + in_s + "')," + s.length + "," + s[0].length + ", byrow=TRUE)");
			sourceWriter.println("tag<-matrix(scan('" + in_tag + "')," + tag.length + ", byrow=TRUE)");
			sourceWriter.println("result<-get.lam(x, s, tag, stretch=" + stretch + ")");
			sourceWriter.println("write.table(result$s,file='" + out_s + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$lambda,file='" + out_lambda + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$tag,file='" + out_tag + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$dist,file='" + out_dist + "',row.names=F,col.names=F)");

			// execute in R
			instance().execute(tmp, sourceWriter);

			result.s = UtilIO.readMatrixDouble(out_s.toString());
			result.lambda = UtilIO.readArrayDouble(out_lambda.toString());
			result.tag = UtilIO.readArrayInt(out_tag.toString());
			result.dist = UtilIO.readDouble(out_dist.toString());

		} catch (Exception e) {
			throw new RuntimeException("RWrapper.getLambdaWrapper(): "  + e.getMessage());
		}

		// R indices start in 1...
		for (int i = 0; i < result.tag.length; ++i) {
			result.tag[i] -= 1;
		}

		// R indices start in 1...
		for (int i = 0; i < tag.length; ++i) {
			tag[i] -= 1;
		}

		//Logger.DEBUG("RWrapper.getLambdaWrapper()", " - done in " + timer.elapsed());
		return result;
	}

	/** Version invoking R instead of doing it natively
	 *
	 * @see PrincipalCurve.principalCurve()
	 */
	public static PrincipalCurve.Result principalCurve(double[][] x,
							   int degreesOfFreedom,
							   int maxIterations) {
		//mafm: for benchmarking purposes
		//Timer timer = new Timer();

		PrincipalCurve.Result result = new PrincipalCurve.Result();

		try {
			// base file
			dir = wipeAndCreateDir(dirName);
			String tmp = dirName + "/" + getSerial () + "-princurve-";

			File source = new File(tmp + "source");
			File in_x = new File(tmp + "in.x");
			File out_s = new File(tmp + "out.s");
			File out_lambda = new File(tmp + "out.lambda");
			File out_tag = new File(tmp + "out.tag");
			File out_dist = new File(tmp + "out.dist");

			UtilIO.writeMatrixDouble(in_x.toString(), x);

			// write source file to be interpreted by R
			PrintWriter sourceWriter = new PrintWriter(source);
			// mafm: the method with matrix(scan(...)) is much more efficient
			//sourceWriter.println("x<-as.matrix(read.table('" + in_x + "'))");
			sourceWriter.println("x<-matrix(scan('" + in_x + "')," + x.length + "," + x[0].length + ", byrow=TRUE)");
			sourceWriter.println("result<-principal.curve(x, smoother='smooth.spline', df="
					     + degreesOfFreedom + ", maxit=" + maxIterations + ", trace=TRUE)");
			sourceWriter.println("write.table(result$s,file='" + out_s + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$lambda,file='" + out_lambda + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$tag,file='" + out_tag + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$dist,file='" + out_dist + "',row.names=F,col.names=F)");

			// execute in R
			instance().execute(tmp, sourceWriter);

			// gather results
			result.s = UtilIO.readMatrixDouble(out_s.toString());
			result.lambda = UtilIO.readArrayDouble(out_lambda.toString());
			result.tag = UtilIO.readArrayInt(out_tag.toString());
			result.dist = UtilIO.readDouble(out_dist.toString());

		} catch (Exception e) {
			throw new RuntimeException("RWrapper.principalCurve(): "  + e.getMessage());
		}

		// R indices start in 1...
		for (int i = 0; i < result.tag.length; ++i) {
			result.tag[i] -= 1;
		}

		//Logger.DEBUG("RWrapper.principalCurve()", " - done in " + timer.elapsed());
		return result;
	}

	/** Version invoking R svd() operation instead of doing it natively with
	 * a Java library
	 *
	 * @param xstar Input for the function
	 * @return Result of the calculation
	 */
	public static PrincipalCurve.SVDResult svd(double[][] xstar) {
		//mafm: for benchmarking purposes
		//Timer timer = new Timer();

		PrincipalCurve.SVDResult result = new PrincipalCurve.SVDResult();

		try {
			// base file
			dir = wipeAndCreateDir(dirName);
			String tmp = dirName + "/" + getSerial() + "-svd-";

			File source = new File(tmp + "source");
			File in_xstar = new File(tmp + "in.xstar");
			File out_d = new File(tmp + "out.d");
			File out_u = new File(tmp + "out.u");
			File out_v = new File(tmp + "out.v");

			UtilIO.writeMatrixDouble(in_xstar.toString(), xstar);

			// write source file to be interpreted by R
			PrintWriter sourceWriter = new PrintWriter(source);
			// mafm: the method with matrix(scan(...)) is much more efficient
			//sourceWriter.println("xstar<-as.matrix(read.table('" + in_xstar + "'))");
			sourceWriter.println("xstar<-matrix(scan('" + in_xstar + "')," + xstar.length + "," + xstar[0].length + ", byrow=TRUE)");
			sourceWriter.println("result<-svd(xstar)");
			sourceWriter.println("write.table(result$d,file='" + out_d + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$u,file='" + out_u + "',row.names=F,col.names=F)");
			sourceWriter.println("write.table(result$v,file='" + out_v + "',row.names=F,col.names=F)");

			// execute in R
			instance().execute(tmp, sourceWriter);

			// gather results
			result.d = UtilIO.readArrayDouble(out_d.toString());
			result.u = UtilIO.readMatrixDouble(out_u.toString());
			result.v = UtilIO.readMatrixDouble(out_v.toString());

		} catch (Exception e) {
			throw new RuntimeException("RWrapper.svd(): "  + e.getMessage());
		}

		//Logger.DEBUG("RWrapper.svd()", " - done in " + timer.elapsed());
		return result;
	}

	/** Version invoking R smooth.splines() operation instead of doing it
	 * natively with a Java library
	 *
	 * @param lambda Input for the function
	 * @param x Input for the function
	 * @param df Input for the function
	 * @return Result of the calculation
	 */
	public static double[] smoothSpline(double[] lambda, double[] x, int df) {
		//mafm: for benchmarking purposes
		//Timer timer = new Timer();

		double[] result = null;

		try {
			// base file
			dir = wipeAndCreateDir(dirName);
			String tmp = dirName + "/" + getSerial() + "-smooth_splines-";

			File source = new File(tmp + "source");
			File in_lambda = new File(tmp + "in.lambda");
			File in_x = new File(tmp + "in.x");
			File out = new File(tmp + "out");

			UtilIO.writeArrayDouble(in_lambda.toString(), lambda);
			UtilIO.writeArrayDouble(in_x.toString(), x);

			// write source file to be interpreted by R
			PrintWriter sourceWriter = new PrintWriter(source);
			// mafm: the method with matrix(scan(...)) is much more efficient
			//sourceWriter.println("lambda<-as.matrix(read.table('" + in_lambda + "'))");
			//sourceWriter.println("x<-as.matrix(read.table('" + in_x + "'))");
			sourceWriter.println("lambda<-matrix(scan('" + in_lambda + "')," + lambda.length + ", byrow=TRUE)");
			sourceWriter.println("x<-matrix(scan('" + in_x + "')," + x.length + ", byrow=TRUE)");
			sourceWriter.println("fit<-smooth.spline(lambda, x, df=" + df + ")");
			sourceWriter.println("result<-predict(fit, x=lambda)$y");
			sourceWriter.println("write.table(result,file='" + out + "',row.names=F,col.names=F)");

			// execute in R
			instance().execute(tmp, sourceWriter);

			// gather results
			result = UtilIO.readArrayDouble(out.toString());

		} catch (Exception e) {
			throw new RuntimeException("RWrapper.smoothSplines(): "  + e.getMessage());
		}

		//Logger.DEBUG("RWrapper.smoothSplines()", " - done in " + timer.elapsed());
		return result;
	}
}
