/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.*;
import java.util.*;
import java.text.*;


/**
 * Logging class for the project, with Singleton pattern.
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class Logger {

	/** Encodes the class priority levels and names */
	public enum Level {
		DEBUG(1), INFO(2), WARNING(3), ERROR(4), FATAL(5);

		/** Value of the level (to filter out different levels of
		 * debugging messages) */
		private int _value;


		/** Default constructor */
		Level(int value) {
			_value = value;
		}

		/** Get the value */
		public int getValue() {
			return _value;
		}
	}

	/** Singleton instance */
	private static Logger _instance;

	/** File name of the log */
	private static String _fileName;

	/** To write the output to a file */
	private FileWriter _fileWriter;

	/** Date format for the log message */
	private DateFormat _dateFormatter;

	/** Whether to show date in the log message (very long lines) */
	private boolean _showDate;

	/** Only print messages with given level, or superior */
	private Level _levelFilter;


	/** Access to the singleton instance */
	public synchronized static Logger instance() {
		if (_instance == null) {
			_instance = new Logger();
		}
		return _instance;
	}

	/** Default constructor */
	private Logger() {
		// level
		setLevelFilter(Level.DEBUG);

		// date format
		_dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
						      Locale.getDefault());

		// show date?
		setShowDate(false);

		// initialize file writer
		try {
			_fileWriter = new FileWriter(new File(_fileName));
		} catch (Exception e) {
			_fileWriter = null;
			//System.err.println("Logger: Can't write log to file '" + _fileName + "', using console");
		}
	}

	/**
	 * Print a DEBUG message
	 *
	 * @param className
	 *            Class (module) name that calls this method.
	 * @param message
	 *            The message itself.
	 */
	public static void DEBUG(String className, String message) {
		instance().print(className, Level.DEBUG, message);
	}

	/**
	 * Print a INFO message
	 *
	 * @param className
	 *            Class (module) name that calls this method.
	 * @param message
	 *            The message itself.
	 */
	public static void INFO(String className, String message) {
		instance().print(className, Level.INFO, message);
	}

	/**
	 * Print a WARNING message
	 *
	 * @param className
	 *            Class (module) name that calls this method.
	 * @param message
	 *            The message itself.
	 */
	public static void WARNING(String className, String message) {
		instance().print(className, Level.WARNING, message);
	}

	/**
	 * Print a ERROR message
	 *
	 * @param className
	 *            Class (module) name that calls this method.
	 * @param message
	 *            The message itself.
	 */
	public static void ERROR(String className, String message) {
		instance().print(className, Level.ERROR, message);
	}

	/**
	 * Print a FATAL message
	 *
	 * @param className
	 *            Class (module) name that calls this method.
	 * @param message
	 *            The message itself.
	 */
	public static void FATAL(String className, String message) {
		instance().print(className, Level.FATAL, message);
	}

	/**
	 * Print a message from the given class name with the given level.  It's
	 * the backend for all the other methods who have the level encoded in
	 * their names.
	 *
	 * @param className
	 *            Class (module) name that calls this method.
	 * @param level
	 *            The type (priority, severity) of message.
	 * @param message
	 *            The message itself.
	 */
	private void print(String className, Level level, String message) {
		if (level.getValue() >= _levelFilter.getValue()) {
			// full message that will be printed
			String fullMessage = "";

			// date
			if (_showDate) {
				String dateFormatted = _dateFormatter.format(Calendar.getInstance().getTime());
				fullMessage += dateFormatted + "::";
			}

			fullMessage += className + "::" + level + ": " + message;

			// writing to the file or the console if we can't access the file
			if (_fileWriter == null) {
				System.err.println(fullMessage);
			} else {
				try {
					_fileWriter.write(fullMessage + "\n");
					_fileWriter.flush();
				} catch (Exception e) {
					_fileWriter = null;
					WARNING("Logger", "Writing to log file failed or disabled, using console from now on");
					print(className, level, message);
				}
			}
		}
	}

	/**
	 * Set a new level filter (to really log the messages with the given or
	 * superior level).
	 *
	 * @param level
	 *            New level to filter out messages.
	 */
	public void setLevelFilter(Level level) {
		_levelFilter = level;
	}

	/**
	 * Set whether to show date in the log message (very long lines)
	 *
	 * @param show
	 *            Whether to show date
	 */
	public void setShowDate(boolean show) {
		_showDate = show;
	}

	/**
	 * Set the filename to log to.  Theoretically it should be set
	 * before starting to log and never be changed.
	 *
	 * @param fileName
	 *            File name to use for logging
	 */
	public static void setFileName(String fileName) {
		_fileName = fileName;
		if (_fileName != null) {
			try {
				instance()._fileWriter = new FileWriter(new File(_fileName));
			} catch (Exception e) {
				instance()._fileWriter = null;
				WARNING("Logger", "Can't write log to file '" + _fileName + "', using console");
			}
		}
	}
}
