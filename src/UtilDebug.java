/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.*;
import java.util.*;


/** Helper to track time
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Timer {

	/** Start */
	private long start = System.currentTimeMillis();


	/** Time elapsed (in s)
	 *
	 * @return Elapsed time
	 */
	public double elapsedSecondsNumber() {
		return (System.currentTimeMillis()-start)/1000.0;
	}


	/** Get time elapsed (in s)
	 *
	 * @return Elapsed time in number
	 */
	public long elapsedTimeMillis() {
		return (System.currentTimeMillis()-start);
	}

	/** Print time elapsed (in s)
	 *
	 * @return Elapsed time
	 */
	public String elapsed() {
		return String.format("%.3fs", (System.currentTimeMillis()-start)/1000.0);
	}

	/** Print time elapsed (in ms)
	 *
	 * @return Elapsed time
	 */
	public String elapsedMillis() {
		return String.format("%3fms", System.currentTimeMillis()-start);
	}

	/** Print time elapsed (in ms, average)
	 *
	 * @param denom Denominator to divide and get the average
	 * @return Elapsed time
	 */
	public String elapsedAverage(int denom) {
		return "" + ((System.currentTimeMillis()-start) / (float) denom) + "ms";
	}

	/** Print time elapsed, pretty format, static version
	 *
	 * Pretty format means with hours, minutes and seconds when time is high
	 * enough.
	 *
	 * @param runTime Milliseconds to format
	 * @return Elapsed time in pretty formating
	 */
	public static String getPrettyFormatting(long runTime) {
		// calculation
		int remainder = (int)runTime;
		int hours = remainder / (60*60*1000);
		remainder = (int)runTime % (60*60*1000);
		int minutes = remainder / (60*1000);
		remainder = remainder % (60*1000);
		int seconds = remainder / (1000);
		remainder = remainder % (1000);
		int milliseconds = remainder;

		// formatting of pretty printing
		String pretty = "";
		if (hours > 0) {
			pretty += String.format("%dh", hours);
		}
		if (hours > 0) {
			pretty += String.format("%02dm", minutes);
		} else if (minutes > 0) {
			pretty += String.format("%dm", minutes);
		}
		if (hours > 0 || minutes > 0) {
			pretty += String.format("%02d", seconds);
		} else {
			pretty += String.format("%d", seconds);
		}
		pretty += String.format(".%03ds", milliseconds);

		return pretty;
	}

	/** Print time elapsed, pretty format
	 *
	 * Pretty format means with hours, minutes and seconds when time is high
	 * enough.
	 *
	 * @return Elapsed time in pretty formating
	 */
	public String getPrettyFormatting() {
		return getPrettyFormatting(System.currentTimeMillis() - start);
	}

	/** Print time elapsed (in s), pretty format
	 *
	 * @param runTime Passing runTime explicitly
	 * @return Elapsed time
	 */
	public static String getPrettySeconds(long runTime) {
		return String.format("%.3fs", runTime/1000.0);
	}
}


/** Misc. utilities useful for debugging
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class UtilDebug {

	/** Method to print arrays/matrices in R compatible format (sometimes to
	 * send them directly to the RWrapper experimentally, instead of passing
	 * them by files)
	 *
	 * @param name Name of the structure to be printed
	 * @param array Array/Matrix to be printed
	 */
	public static String toRString(String name, double[][] array) {
		int nRows = array.length;
		int nCols = array[0].length;

		StringBuilder sb = new StringBuilder();
		sb.append(name + "<-rbind(\n");
		for (int row = 0; row < nRows; ++row) {
			sb.append("c(");
			for (int col = 0; col < nCols; ++col) {
				if (col > 0)
					sb.append(",");
				sb.append(array[row][col]);
			}
			sb.append("),\n");
		}
		sb.setCharAt(sb.length() - 2, ')');

		return sb.toString();
	}

	/** Method to print arrays/matrices in R compatible format (sometimes to
	 * send them directly to the RWrapper experimentally, instead of passing
	 * them by files)
	 *
	 * @param name Name of the structure to be printed
	 * @param array Array/Matrix to be printed
	 */
	public static String toRString(String name, double[] array) {
		int nRows = array.length;

		StringBuilder sb = new StringBuilder();
		sb.append(name + "<-rbind(");
		for (int row = 0; row < nRows; ++row) {
			if (row > 0)
				sb.append(",");
			sb.append(array[row]);
		}
		sb.append(")\n");

		return sb.toString();
	}

	/** Method to print arrays/matrices in R compatible format (sometimes to
	 * send them directly to the RWrapper experimentally, instead of passing
	 * them by files)
	 *
	 * @param name Name of the structure to be printed
	 * @param array Array/Matrix to be printed
	 */
	public static String toRString(String name, int[] array) {
		int nRows = array.length;

		StringBuilder sb = new StringBuilder();
		sb.append(name + "<-rbind(");
		for (int row = 0; row < nRows; ++row) {
			if (row > 0)
				sb.append(",");
			sb.append(array[row]);
		}
		//sb.setCharAt(sb.length() - 1, ')');
		sb.append(")\n");

		return sb.toString();
	}

	/** Print matrix (of doubles) to stderr
	 *
	 * @param name Name of the structure to be printed
	 * @param array Matrix to be printed
	 */
	public static void print(String name, double[][] array) {
		System.err.println(toString(name, array));
	}

	/** Print matrix (of ints) to stderr
	 *
	 * @param name Name of the structure to be printed
	 * @param array Matrix to be printed
	 */
	public static void print(String name, int[][] array) {
		System.err.println(toString(name, array));
	}

	/** Print array (of doubles) to stderr
	 *
	 * @param name Name of the structure to be printed
	 * @param array Array to be printed
	 */
	public static void print(String name, double[] array) {
		double[] tmp = Arrays.copyOf(array, array.length);
		Util.roundWithFormat(tmp, "#.######");
		System.err.println(name + ": " + Arrays.toString(tmp));
	}

	/** Print array (of ints) to stderr
	 *
	 * @param name Name of the structure to be printed
	 * @param array Array to be printed
	 */
	public static void print(String name, int[] array) {
		System.err.println(name + ": " + Arrays.toString(array));
	}

	/** Helper function to print matrices, converting them to matrices of
	 * String first
	 *
	 * @param name Name of the structure to be printed
	 * @param array Matrix to be printed
	 */
	private static String toString(String name, double[][] array) {
		String[][] tmp = new String[array.length][array[0].length];

		for (int row = 0; row < array.length; ++row) {
			for (int col = 0; col < array[row].length; ++col) {
				tmp[row][col] = String.format("%.6f", array[row][col]);
			}
		}

		return toString(name, tmp);
	}

	/** Helper function to print matrices, converting them to matrices of
	 * String first
	 *
	 * @param name Name of the structure to be printed
	 * @param array Matrix to be printed
	 */
	private static String toString(String name, int[][] array) {
		String[][] tmp = new String[array.length][array[0].length];

		for (int row = 0; row < array.length; ++row) {
			for (int col = 0; col < array[row].length; ++col) {
				tmp[row][col] = String.format("%6d", array[row][col]);
			}
		}

		return toString(name, tmp);
	}

	/** Helper function to print matrices, already converted to matrices of
	 * String first
	 *
	 * @param name Name of the structure to be printed
	 * @param array Matrix to be printed
	 */
	private static String toString(String name, String[][] array) {
		String s = name + ":\n";

		for (int row = 0; row < array.length; ++row) {
			// for easy comparisons with R
			s += String.format("[%3f,] ", row);

			for (int col = 0; col < array[row].length; ++col) {
				s += " " + array[row][col];
			}
			s += "\n";
		}

		return s;
	}
}
