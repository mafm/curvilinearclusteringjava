/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.lang.Math;


/** CEM-PCC algorithm
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class CEMPCC {

	/** Some parameter defaults (Java lacks this property) */
	public static final int DEFAULT_DEGREES_OF_FREEDOM = 5;
	/** Some parameter defaults (Java lacks this property) */
	public static final int DEFAULT_MAX_ITERATIONS = 12;
	/** Some parameter defaults (Java lacks this property) */
	public static final double DEFAULT_TRIM = 0;
	/** Some parameter defaults (Java lacks this property) */
	public static final boolean DEFAULT_MIX = true;
	/** Some parameter defaults (Java lacks this property) */
	public static final double DEFAULT_VAR_BOUND = -1;

	/** Parameter to control whether to output trace information or not */
	public static boolean TRACE = false;


	/** Show trace information
	 *
	 * @param message The message to show
	 */
	public static void trace(String function, String message) {
		if (TRACE) {
			Logger.DEBUG("CEMPCC." + function, message);
		}
	}


	/** cem() result */
	public static class Result {
		/** Data */
		public double[][] x;
		/** Degrees of Freedom */
		public int degreesOfFreedom;
		/** Var bound */
		public double varBound;
		/** Iterations */
		public int iterations;

		/** Clustering */
		public int[] clustering;
		/** Best clustering */
		public int[] bestClustering;
		/** Clustering log*/
		public int[] clusteringLog;

		/** Likelihoods */
		public double[][] likelihoodsAbout;
		/** Likelihoods */
		public double[][] likelihoodsAlong;
		/** Likelihoods */
		public double[][] likelihoods;
		/** Likelihoods */
		public double[][] likelihoodsMix;
		/** Overall Log Likelihoods of clusters */
		public double[] overallLogLikelihood;

		/** Variables of all iterations */
		public double[][] allCurveVars;
		/** Variables of all iterations */
		public double[][] allCurveLengths;
		/** Variables of all iterations */
		public double[][] allMeanAlong;
		/** Variables of all iterations */
		public double[][] allVarAlong;
		/** Variables of all iterations */
		public double[][] allMixProp;

		/** Constructor */
		Result() {
		}

		/** Constructor */
		Result(double[][] x,
		       int degreesOfFreedom,
		       double varBound,
		       int iterations,
		       int[] clustering,
		       int[] bestClustering,
		       int[] clusteringLog,
		       double[][] likelihoodsAbout,
		       double[][] likelihoodsAlong,
		       double[][] likelihoods,
		       double[][] likelihoodsMix,
		       double[] overallLogLikelihood,
		       double[][] allCurveVars,
		       double[][] allCurveLengths,
		       double[][] allMeanAlong,
		       double[][] allVarAlong,
		       double[][] allMixProp) {
			this.x = x;
			this.degreesOfFreedom = degreesOfFreedom;
			this.varBound = varBound;
			this.iterations = iterations;

			this.clustering = clustering;
			this.bestClustering = bestClustering;
			this.clusteringLog = clusteringLog;

			this.likelihoodsAbout = likelihoodsAbout;
			this.likelihoodsAlong = likelihoodsAlong;
			this.likelihoods = likelihoods;
			this.likelihoodsMix = likelihoodsMix;
			this.overallLogLikelihood = overallLogLikelihood;

			this.allCurveVars = allCurveVars;
			this.allCurveLengths = allCurveLengths;
			this.allMeanAlong = allMeanAlong;
			this.allVarAlong = allVarAlong;
			this.allMixProp = allMixProp;
		}
	}

	/** calculateLikelihood() result */
	public static class CalculateLikelihoodResult {
		/** Score */
		public double score;
		/** bic */
		public double bic;
		/** bic2 */
		public double bic2;

		/** Constructor */
		CalculateLikelihoodResult() {
		}

		/** Constructor */
		CalculateLikelihoodResult(double score, double bic, double bic2) {
			this.score = score;
			this.bic = bic;
			this.bic2 = bic2;
		}
	}


	/** Function to calculate the likelihood
	 *
	 * @param cemResult Output from cem().  it includes:
	 * - x is the data
	 * - df is the degrees of freedom
	 * - clust is the classification (0 indicates the noise cluster)
	 * - likelihoods is a matrix of likelihoods (one column per cluster)
	 * if trim was used in cem, then the likelihood calculated here will
	 * reflect it
	 *
	 * @param mixBest is used to indicate if the best overall loglikelihood
	 * (over all iterations of CEM that were run) should be used
	 *
	 * @return Result of the calculations
	 */
	public static CalculateLikelihoodResult calculateLikelihood(Result cemResult,
								    boolean mixBest) {
		int numPoints = cemResult.x.length;

		int numClusters = 0;
		if (mixBest) {
			numClusters = Util.unique(cemResult.bestClustering).length;
		} else {
			numClusters = Util.unique(cemResult.clustering).length;
		}
		int[] clusterSizes = new int[numClusters];

		double score = 0.0;
		if (mixBest) {
			//score <- max(cem.obj$overall.loglikelihood[cem.obj$overall.loglikelihood != 0])
			//
			// mafm: means to take elements different from zero, but
			// it's supposed that some of them are bigger, or if
			// they're all zero score is initialized to zero anyway
			//
			// mafm: it turns out that (sometimes? always?) elements
			// can be negative and 0 means something like not
			// initialized, so we can't rely on regular max function
			// anyway
			score = Double.NEGATIVE_INFINITY;
			for (double elem : cemResult.overallLogLikelihood) {
				if (elem != 0.0 && elem > score) {
					score = elem;
				}
			}

		} else {
			// cluster.sizes[i] <- sum(cem.obj$clust == unique(cem.obj$clust)[i])
			//
			// mafm: clusterSizes[i] is the number of elements of
			// the indexed cluster
			for (int i = 0; i < numClusters; ++i) {
				clusterSizes[i] = Util.countElementsMatching(cemResult.clustering,
									     Util.unique(cemResult.clustering)[i]);
			}

			// now the entries of cluster.sizes correspond to each
			// column of the likelihood matrix (regardless of
			// ordering)
			for (int i = 0; i < numPoints; ++i) {
				double lik = 0;
				for (int j = 0; j < numClusters; ++j) {
					lik += (clusterSizes[j]/(double)numPoints) * cemResult.likelihoods[i][j];
				}
				score += Math.log(lik);
			}
		}

		// assume n-1 curve clusters, 1 noise cluster
		//
		// estimate curves, mixture proportions, noise
		int numParameters = ((numClusters - 1) * (cemResult.degreesOfFreedom + 2)) + (numClusters - 1) + 1;
		double bic = (2 * score) - (numParameters * Math.log(numPoints));

		// TOCHANGE But experimental Important: see changes if real dimension introduced
		// 2= num of dimensions (bic2 is experimental)
		double bic2 = (2 * score) - (2 * numParameters * Math.log(numPoints));

		trace("calculateLikelihood()",
		      "Mix best? " + mixBest + ", score=" + score
		      + ", bic=" + bic
		      + ", bic2=" + bic2);

		return new CalculateLikelihoodResult(score, bic, bic2);
	}

	/** CEM algorithm
	 *
	 * printTrace argument to print tracing/debugging information is
	 * substituted by setting to 'true' the boolean member variable TRACE.
	 *
	 *
	 * @param x Set of points: each row is a data point, each column a
	 * dimension of the point
	 *
	 * @param degreesOfFreedom Degrees of Freedom
	 *
	 * @param initialClustering It has the same length as the number of rows
	 * in data, with each entry giving the number of the cluster to which
	 * that point initially belongs.  The feature clusters are numbered by
	 * consecutive integers starting with 1; 0 indicates the noise cluster.
	 *
	 * @param maxIterations Number of EM iterations to do
	 *
	 * @param trim Can be used to compute a robust estimate of the variance
	 * about the curve for any feature cluster.  The trim amount should be
	 * between 0 and 0.4.
	 *
	 * @param mix Determines whether to use the likelihood or the mixture
	 * likelihood (recommended) for each point when forming the new
	 * clustering at each iteration.  The default value T uses the mixture
	 * likelihood.
	 *
	 * @param varBound A lower bound on the estimate of the variance about
	 * the curve for any feature cluster.  If var.bound < 0, it is computed
	 * automatically based on the range of the data.  Setting var.bound
	 * equal to zero removes any constraint from the variance estimate.
	 */
	public static Result cem(double[][] x,
				 int degreesOfFreedom,
				 int[] initialClustering,
				 int maxIterations,
				 double trim,
				 boolean mix,
				 double varBound) {

		// mafm: called 'n' in the original code
		final int numPoints = x.length;
		// mafm: called 'p' in the original code
		final int numDimensions = x[0].length;

		if (x.length == 0
		    || initialClustering.length == 0
		    || initialClustering.length != numPoints) {
			throw new RuntimeException("CEMPCC: Initial clustering doesn't match number of points, or they're empty");
		}

		if (trim < 0.0 || trim > 0.4) {
			throw new RuntimeException("CEMPCC: 'trim' parameter outside accepted range 0.0-0.4");
		}

		/* mafm: this takes into account only 2-dimensional data, use
		   n-dimensions, instead of area it's hypervolume, and save max
		   range for later

		  n<-length(x[,1])
		  area<-(max(x[,1])-min(x[,1]))*(max(x[,2])-min(x[,2]))
		  done<-F
		  iterations<-0 */
		double maxRange = 0.0;
		double hyperVolume = 1.0;
		for (int col = 0; col < numDimensions; ++col) {
			// mafm: equivalent to calculate range(x[,i])
			double rangeMin = x[0][col];
			double rangeMax = x[0][col];
			for (int row = 1; row < numPoints; ++row) {
				double value = x[row][col];
				if (value < rangeMin) {
					rangeMin = value;
				}
				if (value > rangeMax) {
					rangeMax = value;
				}
			}
			double diff = rangeMax - rangeMin;
			hyperVolume *= diff;
			if (diff > maxRange) {
				maxRange = diff;
			}
		}
		//trace("cem()", "hyperVolume=" + hyperVolume);

		/* mafm: using maxRange from before, and we want more than 2
		 * dimensions

		   # compute the variance bound, if needed
		   if (var.bound<0) {
			   var.bound<- (max(c(range(x[,1])[2]-range(x[,1])[1],
					      range(x[,2])[2]-range(x[,2])[1]))/4000)^2 } */
		if (varBound < 0) {
			// mafm: where does the 4000.0 magic number come from?
			varBound = Util.power2(maxRange/4000.0);
		}
		//trace("cem()", "varBound=" + varBound);

		/* # initialize the clustering and the variables which will keep track of
		   # results at each iteration
		   clust<-clust.init
		   clust.log<-rep(0,max.iter)
		   overall.loglikelihood<-rep(0,max.iter)
		   all.curvevars<-matrix(rep(0,max.iter*length(unique(clust))),
					   ncol=length(unique(clust)))
		   all.varalong<-matrix(rep(0,max.iter*length(unique(clust))),
					   ncol=length(unique(clust)))
		   all.meanalong<-matrix(rep(0,max.iter*length(unique(clust))),
					   ncol=length(unique(clust)))
		   all.curvelengths<-matrix(rep(0,max.iter*length(unique(clust))),
					   ncol=length(unique(clust)))
		   all.mixprop<-matrix(rep(0,max.iter*length(unique(clust))),
					   ncol=length(unique(clust))) */
		int[] clustering = initialClustering;
		int[] clusteringLog = new int[maxIterations];
		double[] overallLogLikelihood = new double[maxIterations];
		int[] bestClustering = null;

		int nRows = maxIterations;
		int nCols = Util.unique(clustering).length;
		double[][] allCurveVars = new double[nRows][nCols];
		double[][] allCurveLengths = new double[nRows][nCols];
		double[][] allVarAlong = new double[nRows][nCols];
		double[][] allMeanAlong = new double[nRows][nCols];
		double[][] allMixProp = new double[nRows][nCols];

		// mafm: defined here to be able to use them later
		double[][] sqDists = null;
		double[][] distAlong = null;
		double[][] likelihoods = null;
		double[][] likelihoodsMix = null;
		double[][] likelihoodsAlong = null;
		double[][] likelihoodsAbout = null;

		// main loop
		boolean done = false;
		int iterations = 0;
		while (!done) {
			//mafm: for benchmarking purposes
			//Timer timer = new Timer();

			/* #initialize loop variables
			   last.clust<-clust
			   classes<-unique(clust)
			   num.of.classes<-length(classes)
			   curvelengths<-rep(0,num.of.classes)
			   curvevars<-rep(0,num.of.classes)
			   varalong<-rep(0,num.of.classes)
			   meanalong<-rep(0,num.of.classes)
			   sqdists<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
			   distalong<-matrix(rep(NA,n*num.of.classes),ncol=num.of.classes)
			   likelihoods<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
			   likelihoods.mix<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
			   like.along<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
			   like.about<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes) */
			int[] lastClustering = Arrays.copyOf(clustering, clustering.length);
			int[] classes = Util.unique(clustering);
			int numClasses = classes.length;

			double[] curveVars = new double[numClasses];
			double[] curveLengths = new double[numClasses];
			double[] varAlong = new double[numClasses];
			double[] meanAlong = new double[numClasses];

			sqDists = new double[numPoints][numClasses];
			distAlong = new double[numPoints][numClasses];
			likelihoods = new double[numPoints][numClasses];
			likelihoodsMix = new double[numPoints][numClasses];
			likelihoodsAlong = new double[numPoints][numClasses];
			likelihoodsAbout = new double[numPoints][numClasses];

			// mafm: initialize to NaN, it seems to be important for
			// this variable
			for (int i = 0; i < distAlong.length; ++i) {
				for (int j = 0; j < distAlong[i].length; ++j) {
					distAlong[i][j] = Double.NaN;
				}
			}

			//if (trace) {cat("Clustering\n",clust,"\n")}
			/* mafm: comment out tracing, noticeable performance
			 * inside these frequently repeated loops (and dealing
			 * with array manipulating and conversion to strings

			trace("cem()", "Clustering: " + Arrays.toString(clustering));
			*/

			//#fit curve and get parameter estimates for each cluster
			//for ( j in 1:num.of.classes) {
			for (int j = 0; j < numClasses; ++j) {
				//if (classes[j]>0) {  #this is a feature cluster
				if (classes[j] > 0) {
					/* filter<-(clust==classes[j])
					   n.curve <- length(x[filter, 1])
					   if (n.curve!=sum(filter) || sum(filter) == 0) {stop("Filter error.")}
					   curve <- principal.curve(x[filter,],maxit=5, smoother="smooth.spline",df=df) */
					// numElementsInCluster is n.curve in R
					int numElementsInCluster = Util.countElementsMatching(clustering, classes[j]);
					// mafm: pointsForCurve equivalent of x[filter,]
					double[][] pointsForCurve = Util.getRowsOfGivenClasses(x, clustering, new int[]{ classes[j] });
					if (numElementsInCluster != pointsForCurve.length
					    || numElementsInCluster == 0) {
						throw new RuntimeException("CEMPCC: Filter error");
					}

					PrincipalCurve.Result curve = PrincipalCurve.principalCurve(pointsForCurve,
												    degreesOfFreedom,
												    5);

					// mafm: maybe we can sort lambda in the
					// beginning (the equivalent of Fortran
					// code) and forget about all the
					// curve.tag[] indirection to get the
					// elements sorted...
					double[] lambdaSorted = Util.getSorted(curve.lambda, curve.tag);

					/* [plot]
					   curvelengths[j] <- (curve$lambda[curve$tag])[n.curve]
					   [trace] */
					// mafm: get elements of lambda with
					// indices by tag, in the same order.
					// in R indexing by 0 doesn't count, but
					// then tag[] should not contain 0 (is
					// the indexed order of the lambda
					// elements, starting with 1). So
					// basically, we get curve.lambda
					// elements sorted
					//
					// mafm: indexing in Java starts with 0, 1 in R
					curveLengths[j] = lambdaSorted[numElementsInCluster-1];

					/* mafm: comment out tracing, noticeable
					 * performance inside these frequently
					 * repeated loops (and dealing with
					 * array manipulating and conversion to
					 * strings

					trace("cem()",
					      "j=" + j +
					      "\n - classes[j]=" + classes[j] +
					      "\n - nCurve=" + numElementsInCluster +
					      "\n - curveLengths[j]=" + curveLengths[j] +
					      "\n - curve.lambda.length=" + curve.lambda.length +
					      "\n - curve.tag.length=" + curve.tag.length +
					      "\n - curve.lambda[curve.tag]=<not shown>"); //Arrays.toString(lambdaSorted));
					*/

					/* #naive variance estimate: curvevars[j] <- (curve$dist)/(n.curve - 1)
					   #or robust estimate from trimmed variance

					   temp.dists<-x[filter,]
					   #CHANGED BY LSB ** To be implemented in java by calling vdist**
					   #        temp.dists<-((temp.dists[,1]-curve$s[,1])^2)+
					   #                    ((temp.dists[,2]-curve$s[,2])^2)
					   nr<-length(temp.dists[,1])
					   nc<-length(temp.dists[1,])
					   total <- array(0,nr)
					   for(i in 1:nr) {
						total[i]<-0
						for(ii in 1:nc) {total[i]<-total[i]+((temp.dists[i,ii]-curve$s[i,ii])^2) }
					   }
					   #END CHANGE
					   temp.dists<-total[order(total)] */

					// mafm: we compute it differently here,
					// partially to have more than 2
					// dimensional points into account.
					// tmp.dists starts as matrix (of
					// x[filter], or pointsForCurve) and
					// ends as vector, with the sum of
					// distances between point and curve, so
					// we skip and rename some steps as
					// well.
					double[] tmpDists = new double[pointsForCurve.length];
					for (int row = 0; row < pointsForCurve.length; ++row) {
						for (int col = 0; col < pointsForCurve[row].length; ++col) {
							tmpDists[row] += Util.power2(pointsForCurve[row][col] - curve.s[row][col]);
						}
					}
					Arrays.sort(tmpDists);

					/* if (trim>0) {
						trimlow<-ceiling((trim/2)*length(temp.dists))
						trimhigh<-floor((1-(trim/2))*length(temp.dists))
						temp.dists<-temp.dists[c(trimlow:trimhigh)]
					   } */
					// mafm: trim before sorting to diminish
					// computing power needed?
					//
					// mafm: would abort when trim>2, index
					// out of bounds (negative), but
					// according to a check in the beginning
					// it should be between 0.0 and 0.4
					if (trim > 0) {
						int trimLow = (int) Math.ceil((trim/2.0)*tmpDists.length);
						int trimHigh = (int) Math.floor((1-(trim/2.0))*tmpDists.length);

						double[] tmpDists2 = new double[trimHigh-trimLow+1];
						for (int i = trimLow; i <= trimHigh; ++i) {
							tmpDists2[i-trimLow] = tmpDists[i];
						}
						tmpDists = tmpDists2;
					}

					/* #check variance bound
					   curvevars[j] <-sum(temp.dists)/(length(temp.dists)-1)
					   if (curvevars[j]<var.bound) { curvevars[j]<-var.bound } */
					curveVars[j] = Util.sum(tmpDists) / (double)(tmpDists.length-1);
					if (curveVars[j] < varBound) {
						curveVars[j] = varBound;
					}

					/* mafm: we want more than 2 dimensions

					   # Loop over all points and (still) cluster j
					   for (i in 1:n) {
					   #CHANGED BY LSB
					   #   sqdists[i,j]<-get.lam(cbind(x[i, 1], x[i, 2]),
					   #                                 s = curve$s, tag = curve$tag,stretch=0)$dist
					   # rbind needed for matrix input to get.lam # Comment by LSB
					   sqdists[i,j]<-get.lam(rbind(x[i, ]),
								 s = curve$s, tag = curve$tag,stretch=0)$dist
					   #END CHANGE
					   } */
					for (int i = 0; i < numPoints; ++i) {
						double[][] xLambda = { x[i] };
						sqDists[i][j] = PrincipalCurve.getLambdaWrapper(xLambda, curve.s, curve.tag, 0).dist;
					}

					/* # distincurve stores distances along principal curve
					  distincurve<-rep(NA,(n.curve-1))

					  for (i in 2:n.curve) {
						distincurve[(i-1)]<-(curve$lambda[curve$tag])[i] -
								    (curve$lambda[curve$tag])[i-1]
					  }
					  #ad hoc method to deal with distincurve at endpoints
					  distincurve<-c(distincurve,distincurve[(n.curve-1)]) */
					double[] distInCurve = new double[numElementsInCluster];
					for (int i = 1; i < numElementsInCluster; ++i) {
						distInCurve[i-1] = curve.lambda[curve.tag[i]] - curve.lambda[curve.tag[i-1]];
					}
					distInCurve[numElementsInCluster-1] = distInCurve[numElementsInCluster-2];

					/* meanalong[j]<-mean(distincurve)
					   varalong[j]<-var(distincurve) */
					meanAlong[j] = Util.mean(distInCurve);
					varAlong[j] = Util.var(distInCurve);

					/* for (i in 1:n) { */
					for (int i = 0; i < numPoints; ++i) {
						/* mafm: filterInd not really
						 * used, and the rest translate
						 * that we call get.lam() with x
						 * having x[i,] first, and the
						 * rest the elements of x that
						 * match (clust==classes[j]),
						 * except x[i,]

						   #find order on curve
						   temp.filter<-filter
						   filter.ind<-0
						   if (temp.filter[i]) {filter.ind<-1}
						   temp.filter[i]<-F
						   this.proj<-NA
						   this.proj<-get.lam(rbind(x[i,],x[temp.filter,]),s=curve$s,
								     tag=curve$tag,stretch=0)
						*/
						Vector<double[]> xLambdaV = new Vector<double[]>();
						xLambdaV.add(x[i]);
						for (int ii = 0; ii < clustering.length; ++ii) {
							if (clustering[ii] == classes[j] && ii != i) {
								xLambdaV.add(x[ii]);
							}
						}
						double[][] xLambda = new double[xLambdaV.size()][x[i].length];
						xLambdaV.toArray(xLambda);
						PrincipalCurve.Result thisProj = PrincipalCurve.getLambdaWrapper(xLambda, curve.s, curve.tag, 0);

						/* mafm: this.n is the last
						 * point, simply the number of
						 * points of xLambda (minus 1 in
						 * Java)... because xLambda has
						 * as many elements as
						 * temp.filter is true, plus
						 * x[i,]

						  #find index of this point. It is point i, but it will be the
						  #first point in this.proj
						  this.n<-sum(temp.filter)+1
						  this.point<-(c(1:this.n))[this.proj$tag==1]
						*/
						int thisN = xLambda.length - 1;
						int thisPoint = -1;
						for (int ii = 0; ii < thisProj.tag.length; ++ii) {
							if (thisProj.tag[ii] == 0) {
								thisPoint = ii;
							}
						}

						// mafm: combining statements,
						// they were just changing
						// "first endpoint" for "last"
						String position = "";
						if (thisPoint == 0 || thisPoint == thisN) {
							/*
							  #now we know that point i projects to the first endpoint
							  #we want to extend the line from the last few projection points
							  #to get distalong and sqdists.  This would be hard and
							  #computationally expensive, so we approximate:
							  distalong[i,j]<-sqrt(sqdists[i,j])
							  #this will err on the side of making the curve too
							  #conservative at the endpoints
							  if (trace) {cat("Point ",i," first in cluster ",j,
									  " distalong: ",distalong[i,j],"\n")}
							*/
							/*
							  #now we know that point i projects to the last endpoint
							  #we want to extend the line from the last two projection points
							  #to get distalong and sqdists
							  distalong[i,j]<-sqrt(sqdists[i,j])
							  if (trace) {cat("Point ",i," last in cluster ",j,
									  " distalong: ",distalong[i,j],"\n")}
							*/
							distAlong[i][j] = Math.sqrt(sqDists[i][j]);
							if (thisPoint == 0) {
								position = "first";
							} else {
								position = "last";
							}
						} else {
							/*
							  #now we know that point i projects inside curve j
							  #put the lambdas in order along the curve
							  current.lams<-this.proj$lambda[this.proj$tag]
							  dist1<-current.lams[this.point+1]-current.lams[this.point]
							  dist2<-current.lams[this.point]-current.lams[this.point-1]
							  distalong[i,j]<-dist1
							  if (trace) {
								cat("Point ",i," is ",this.point," in cluster ",j,
								    " distalong: ",distalong[i,j],"\n")
								    }
							*/
							double[] currentLambdaSorted = Util.getSorted(thisProj.lambda, thisProj.tag);
							double dist1 = currentLambdaSorted[thisPoint+1] - currentLambdaSorted[thisPoint];
							// mafm: not used
							//double dist2 = currentLambdaSorted[thisPoint] - currentLambdaSorted[thisPoint-1];
							distAlong[i][j] = dist1;
							position = Integer.toString(thisPoint);
						}

						/* mafm: comment out tracing,
						 * noticeable performance inside
						 * these frequently repeated
						 * loops (and dealing with array
						 * manipulating and conversion
						 * to strings

						trace("cem()",
						      "Point " + i + " " + Arrays.toString(x[i])
						      + " is " + position + " in cluster " + j + " distalong=" + distAlong[i][j]);
						*/
					}
				} // end of feature cluster routine

				/* mafm: they're already initialized to 0 but
				 * reused, so we can't skip this code

				  if (classes[j]==0) { #now deal with noise cluster
					curvelengths[j]<-0
					curvevars[j]<-0
					for (i in 1:n) {
						sqdists[i,j]<-0
					}
				  } #end of noise cluster routine */
				if (classes[j] == 0) {
					curveLengths[j] = 0.0;
					curveVars[j] = 0.0;
					for (int i = 0; i < numPoints; ++i) {
						sqDists[i][j] = 0.0;
					}
				}
			}

			// mafm: created here because in the original it's
			// recalculated many times in the next loop and again
			// when assigning all* variables...
			double[] mixProp = new double[numClasses];
			for (int j = 0; j < numClasses; ++j) {
				mixProp[j] = Util.countElementsMatching(clustering, classes[j]) / (double)numPoints;
			}

			/* #calculate likelihood of each point being in each cluster
			   for (i in 1:n) {
			     for (j in 1:num.of.classes) {
			       if(classes[j]>0) {
				 ### uniform*normal method:
				 like.about[i,j]<-(1/(sqrt(2*pi*curvevars[j])))*
					(exp((-1/2)*(sqdists[i,j]/curvevars[j])))
				 like.along[i,j]<-(1/curvelengths[j])
				 mix.prop<-(sum(clust==classes[j]))/n
				 likelihoods.mix[i,j]<- mix.prop*like.about[i,j]*like.along[i,j]
				 likelihoods[i,j]<- like.about[i,j]*like.along[i,j]
			       }
			       if (classes[j]==0) { mix.prop<-(sum(clust==classes[j]))/n
				 likelihoods[i,j]<-1/area
				 likelihoods.mix[i,j]<-mix.prop/area
				 like.along[i,j]<-0
				 like.about[i,j]<-0
			       }
			     }
			   } */
			for (int i = 0; i < numPoints; ++i) {
				for (int j = 0; j < numClasses; ++j) {
					if (classes[j] > 0) {
						likelihoodsAbout[i][j] =
							(1/(Math.sqrt(2*Math.PI*curveVars[j])))
							* (Math.exp((-1/2.0)*(sqDists[i][j]/curveVars[j])));
						likelihoodsAlong[i][j] = 1 / curveLengths[j];
						likelihoods[i][j] = likelihoodsAbout[i][j] * likelihoodsAlong[i][j];
						likelihoodsMix[i][j] = mixProp[j] * likelihoods[i][j];
					} else if (classes[j] == 0) {
						likelihoodsAbout[i][j] = 0;
						likelihoodsAlong[i][j] = 0;
						likelihoods[i][j] = 1 / hyperVolume;
						likelihoodsMix[i][j] = mixProp[j] / hyperVolume;
					} else {
						throw new RuntimeException("Unexpected negative value in " +
									   "classes[" + j + "]=" + classes[j]);
					}
				}
			}

			/* mafm: not used anywhere, only for returning/debugging

			  #save all parameter estimates
			  all.curvevars[iterations+1,]<-curvevars
			  all.varalong[iterations+1,]<-varalong
			  all.meanalong[iterations+1,]<-meanalong
			  all.curvelengths[iterations+1,]<-curvelengths
			  mix.prop<-rep(NA,num.of.classes)
			  for (j in 1:num.of.classes) {mix.prop[j]<-(sum(clust==classes[j]))/n}
			  all.mixprop[iterations+1,]<-mix.prop */
			allCurveVars[iterations] = curveVars;
			allCurveLengths[iterations] = curveLengths;
			allMeanAlong[iterations] = meanAlong;
			allVarAlong[iterations] = varAlong;
			allMixProp[iterations] = mixProp;

			/* # make new clustering
			   if (trace) {
			       for (j in 1:num.of.classes) {
			       cat("Likelihoods for cluster ",j,"\n",round(likelihoods[,j],dig=4),"\n\n")
			  	 }
			       cat("\n\nVar along: ",varalong,"\n")
			       cat("\nVar about: ",curvevars,"\n")
			       cat("\nMean along: ",meanalong,"\n")
			  	}

			   if (mix) {
			   for (i in 1:n) {
			     largest<-max(likelihoods.mix[i,])
			     for (j in 1:num.of.classes) {
			  	  if (likelihoods.mix[i,j]==largest) {clust[i]<-classes[j]}
			      }
			     }}

			   if (!mix) {
			  	for (i in 1:n) {
			     largest<-max(likelihoods[i,])
			     for (j in 1:num.of.classes) {
			  	  if (likelihoods[i,j]==largest) {clust[i]<-classes[j]}
			      }
			     }} */

			/* mafm: comment out tracing, noticeable performance
			 * inside these frequently repeated loops (and dealing
			 * with array manipulating and conversion to strings

			if (TRACE) {
				for (int j = 0; j < numClasses; ++j) {
					trace("cem()", " - Likelihoods for cluster " + j + ": "
					      + Arrays.toString(Util.getColumnAsArray(likelihoods, j)));
				}
				trace("cem()",
				      " - varAlong=" + Arrays.toString(varAlong) +
				      "\n - varAbout=" + Arrays.toString(curveVars) +
				      "\n - meanAlong=" + Arrays.toString(meanAlong));
			}
			*/

			double[][] likelihoodsToUse = null;
			if (mix) {
				likelihoodsToUse = likelihoodsMix;
			} else {
				likelihoodsToUse = likelihoods;
			}
			for (int i = 0; i < numPoints; ++i) {
				// mafm: I think that efficiency might be
				// improved getting not max(), but the position
				// of the maximum value (if there's only one)
				double largest = Util.max(likelihoodsToUse[i]);
				for (int j = 0; j < numClasses; ++j) {
					if (likelihoodsToUse[i][j] == largest) {
						clustering[i] = classes[j];
					}
				}
			}

			/* mafm: reordering because of array indices in R start
			 * in 1, 0 in Java...

			  # test for convergence of clustering or max number of iterations
			  iterations<-iterations+1
			  sameness.counter<-0
			  for(i in 1:n) {if (last.clust[i]==clust[i])
						{sameness.counter<-sameness.counter+1}}
			  clust.log[iterations]<-sameness.counter
			  if(sameness.counter==n) {done<-T}
			  if(iterations>=max.iter) {done<-T}
			  temp<-0
			  for (i in 1:n) {
				temp<-temp+log(sum(likelihoods.mix[i,]))
			  }
			  overall.loglikelihood[iterations]<-temp
			  if (overall.loglikelihood[iterations]>=max(overall.loglikelihood[1:iterations]))
				{ clust.best<-clust }
			 */
			double temp = 0;
			for (int i = 0; i < numPoints; ++i) {
				temp += Math.log(Util.sum(likelihoodsMix[i]));
			}
			overallLogLikelihood[iterations] = temp;
			if (overallLogLikelihood[iterations] >= Util.max(overallLogLikelihood, 0, iterations)) {
				// copying, referencing doesn't work since
				// "clustering" is not created anew in
				// subsequent iterations, thus overwriting both
				// "clustering" and "bestClustering" and
				// rendering a wrong result...
				bestClustering = Arrays.copyOf(clustering, clustering.length);
			}

			int samenessCounter = 0;
			for (int i = 0; i < numPoints; ++i) {
				if (lastClustering[i] == clustering[i]) {
					++samenessCounter;
				}
			}
			clusteringLog[iterations] = samenessCounter;

			++iterations;
			if (samenessCounter == numPoints || iterations >= maxIterations) {
				done = true;
			}

			//mafm: for benchmarking purposes
			//Logger.DEBUG("CEMPCC", "iteration " + (iterations-1) + ", time elapsed " + timer.elapsed());

		} // end of while loop -- END OF MAIN PROGRAM LOOP

		/*
		  return(varalong,meanalong,curvelengths,curvevars,likelihoods,likelihoods.mix,
			clust,iterations,clust.log,x,df,var.bound,like.along,like.about,
			distalong, sqdists,clust.best,overall.loglikelihood,
			all.curvevars,all.varalong,all.meanalong,all.curvelengths,all.mixprop)
		*/
		return new Result(x, degreesOfFreedom, varBound, iterations,
				  clustering, bestClustering, clusteringLog,
				  likelihoodsAbout, likelihoodsAlong, likelihoods, likelihoodsMix, overallLogLikelihood,
				  allCurveVars, allCurveLengths, allMeanAlong, allVarAlong, allMixProp);
	}
}
