/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.io.*;


/** Input/Output utility functions
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class UtilIO {

	/** Helper function to read a matrix of double
	 *
	 * @param filename Name/path of the file to read
	 * @return The data read
	 */
	public static double[][] readMatrixDouble(String filename) {
		String[][] data = readData(filename);
		double[][] points = new double[data.length][data[0].length];

		for (int i = 0; i < points.length; ++i) {
			for (int j = 0; j < points[i].length; ++j) {
				points[i][j] = Double.valueOf(data[i][j]);
			}
		}

		return points;
	}

	/** Helper function to read array of int
	 *
	 * @param filename Name/path of the file to read
	 * @return The data read
	 */
	public static int[] readArrayInt(String filename) {
		String[][] data = readData(filename);
		if (data[0].length > 1) {
			throw new RuntimeException("readArrayInt(): '" + filename + "' contains data with more than one column");
		}
		int[] result = new int[data.length];

		for (int i = 0; i < result.length; ++i) {
			result[i] = Integer.valueOf(data[i][0]);
		}

		return result;
	}

	/** Helper function to read array of double
	 *
	 * @param filename Name/path of the file to read
	 * @return The data read
	 */
	public static double[] readArrayDouble(String filename) {
		String[][] data = readData(filename);
		if (data[0].length > 1) {
			throw new RuntimeException("readArrayDouble(): '" + filename + "' contains data with more than one column");
		}
		double[] result = new double[data.length];

		for (int i = 0; i < result.length; ++i) {
			result[i] = Double.valueOf(data[i][0]);
		}

		return result;
	}

	/** Helper function to read a single double
	 *
	 * @param filename Name/path of the file to read
	 * @return The data read
	 */
	public static double readDouble(String filename) {
		String[][] data = readData(filename);
		if (data.length != 1 && data[0].length != 1) {
			throw new RuntimeException("readDouble(): '" + filename + "' contains more than one value");
		} else {
			return Double.valueOf(data[0][0]);
		}
	}

	/** Helper function to read a single int
	 *
	 * @param filename Name/path of the file to read
	 * @return The data read
	 */
	public static int readInt(String filename) {
		String[][] data = readData(filename);
		if (data.length != 1 && data[0].length != 1) {
			throw new RuntimeException("readInt(): '" + filename + "' contains more than one value");
		} else {
			return Integer.valueOf(data[0][0]);
		}
	}

	/** Helper function to read data (type agnostic, as an array of arrays
	 * of String), it will be used by the other functions to return the
	 * appropriate types.
	 *
	 * @param filename Name/path of the file to read
	 * @return The data read
	 */
	private static String[][] readData(String filename) {
		String[][] data = null;

		Vector<Vector> readData = new Vector<Vector>();
		try {
			BufferedReader bufReader = new BufferedReader(new FileReader(filename));
			String line = bufReader.readLine();
			while (line != null) {
				StringTokenizer st = new StringTokenizer(line);
				Vector<String> row = new Vector<String>();
				while (st.hasMoreTokens()) {
					row.add(st.nextToken());
				}
				readData.add(row);
				line = bufReader.readLine();
			}
			bufReader.close();
		} catch (Exception e) {
			throw new RuntimeException("readData(): Couldn't read data: " + e.getMessage());
		}

		data = new String[readData.size()][readData.get(0).size()];
		for (int i = 0; i < data.length; ++i) {
			for (int j = 0; j < data[i].length; ++j) {
				data[i][j] = (String)(readData.get(i).get(j));
			}
		}

		return data;
	}

	/** Helper function to write matrix of double
	 *
	 * @param filename Name/path of the file to be written
	 * @param data The data to be written
	 */
	public static void writeMatrixDouble(String filename, double[][] data) {
		String[][] dataStr = new String[data.length][data[0].length];
		for (int i = 0; i < data.length; ++i) {
			for (int j = 0; j < data[i].length; ++j) {
				dataStr[i][j] = Double.toString(data[i][j]);
			}
		}
		writeData(filename, dataStr);
	}

	/** Helper function to write array of int
	 *
	 * @param filename Name/path of the file to be written
	 * @param data The data to be written
	 */
	public static void writeArrayInt(String filename, int[] data) {
		String[][] dataStr = new String[data.length][1];
		for (int i = 0; i < data.length; ++i) {
			dataStr[i][0] = Integer.toString(data[i]);
		}
		writeData(filename, dataStr);
	}

	/** Helper function to write array of double
	 *
	 * @param filename Name/path of the file to be written
	 * @param data The data to be written
	 */
	public static void writeArrayDouble(String filename, double[] data) {
		String[][] dataStr = new String[data.length][1];
		for (int i = 0; i < data.length; ++i) {
			dataStr[i][0] = Double.toString(data[i]);
		}
		writeData(filename, dataStr);
	}

	/** Helper function to write a single double
	 *
	 * @param filename Name/path of the file to be written
	 * @param data The data to be written
	 */
	public static void writeDouble(String filename, double data) {
		String[][] dataStr = new String[1][1];
		dataStr[0][0] = Double.toString(data);
		writeData(filename, dataStr);
	}

	/** Helper function to write a single int
	 *
	 * @param filename Name/path of the file to be written
	 * @param data The data to be written
	 */
	public static void writeInt(String filename, int data) {
		String[][] dataStr = new String[1][1];
		dataStr[0][0] = Integer.toString(data);
		writeData(filename, dataStr);
	}

	/** Helper function to write data (type agnostic, as an array of arrays
	 * of String), it will be used by the other functions after conversion
	 * to string.
	 *
	 * @param filename Name/path of the file to be written
	 * @param data The data to be written
	 */
	private static void writeData(String filename, String[][] data) {
		File tmp = new File(filename);

		try {
			PrintWriter writer = new PrintWriter(tmp);
			for (int i = 0; i < data.length; ++i) {
				for (int j = 0; j < data[i].length; ++j) {
					if (j > 0) {
						writer.print(" ");
					}
					writer.print(data[i][j]);
				}
				writer.println();
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			Logger.ERROR("writeData()", e.getMessage());
		}
	}
}
