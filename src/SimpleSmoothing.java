/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** Class implementing a simple smoothing function
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class SimpleSmoothing {

	/** Simple smoothing function, which modifies a given element taking
	 * into account the elements surrounding it.
	 *
	 * The description of the parameters is copied from R's smooth.spline(),
	 * although the first variable is not actually used in the current
	 * implementation.
	 *
	 * @param x An array giving the values of the predictor variable.
	 *
	 * @param responses. If ‘y’ is missing, the responses are assumed to be
	 * specified by ‘x’.
	 *
	 * @return The smoothed values as an array
	 */
	public static double[] smooth(double[] x, double[] y) {
		double[] fit = new double[y.length];

		// emulate missing point the ends, the end point has double
		// influence than the previous one (thus the *3), and then /4 to
		// compensate
		double[] smoothed = new double[y.length + 2];
		for (int i = 0; i < y.length; ++i) {
			smoothed[i+1] = y[i];
		}
		smoothed[0] = (y[0]*3 + y[1]) / 4.0;
		smoothed[smoothed.length-1] = (y[y.length-2] + y[y.length-1]*3) / 4.0;

		for (int i = 0; i < y.length; ++i) {
			double distWithPrevious = (smoothed[i] - smoothed[i+1]);
			double previous = smoothed[i] + (distWithPrevious)/3.0;

			double distWithNext = (smoothed[i+2] - smoothed[i+1]);
			double next = smoothed[i+2] + (distWithNext)/3.0;

			double current = smoothed[i+1]*2;

			fit[i] = (previous + current + next) / 4.0;
		}

		return fit;
	}
}
