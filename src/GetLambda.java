/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.lang.Double;
import java.lang.Float;
import java.lang.Math;


/** Helper class to return values from lamix()
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class LamixResult {
	/** Value to be returned by lamix() */
	double lambda = Double.NaN;
	/** Value to be returned by lamix() */
	double distmin = Double.NaN;
}


/** GetLambda helper class, read methods for some information of what it does
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class GetLambda {

	/** Parameter to control whether to output trace information or not */
	public static boolean TRACE = false;


	/** Show trace information
	 *
	 * @param message The message to show
	 */
	public static void trace(String function, String message) {
		if (TRACE) {
			Logger.DEBUG("GetLambda." + function, message);
		}
	}


	/** Method translated from getlam.r
	 *
	 * Original code:
	 *
	 * subroutine getlam(n,p,x,sx,lambda,order,dist,ns,s,strech,vecx,tempsx);
	 * integer n,p,ns,order(n)
	 * double precision x(n,p),sx(n,p),s(ns,p),lambda(n),dist(n),tempsx(p),
	 *   vecx(p),strech
	 *
	 * #this subroutine assumes on input a set of p co-ordinate functions s
	 * #defined on the integers 1 ... ns; ie a parametrized  curve.
	 * #
	 * #It produces a vector of lambda and fitted values sx for each value of x
	 *
	 * # stretch left and right end boundries by factor 1+strech
	 * if(strech < 0d0) strech=0d0
	 * if(strech > 2d0) strech=2d0
	 *
	 * do j=1,p{
	 *   s(1,j)=s(1,j)+strech*(s(1,j)-s(2,j));
	 *   s(ns,j)=s(ns,j)+strech*(s(ns,j)-s(ns-1,j))
	 * }
	 * do i=1,n{
	 *   do j=1,p {vecx(j)=x(i,j)}
	 *   call lamix(ns,p,vecx,s,lambda(i),dist(i),tempsx);
	 *   do j=1,p {sx(i,j)=tempsx(j)}
	 * }
	 * do i=1,n{order(i)=i}
	 * call sortdi(lambda,order,1,n)
	 * call newlam(n,p,sx,lambda,order)
	 * return;
	 * end;
	 */
	public static void getLambda(int numberOfPoints,
				     int numberOfDimensions,
				     double[][] x,
				     double[][] sx,
				     double[][] s,
				     double stretch,
				     int[] tag,
				     double[] lambda,
				     double[] dist) {
		/* mafm: for tracing input data
		UtilDebug.print("getLambda(), input x", x);
		UtilDebug.print("getLambda(), input sx", sx);
		UtilDebug.print("getLambda(), input s", s);
		*/

		// mafm: ns is the number of rows of 's', not necessarily the
		// same as 'x' (which is numberOfPoints)
		if (s.length < 3) {
			throw new RuntimeException("GetLambda.getLambda(): array 's' needs at least 3 dimensions");
		}
		int ns = s.length;

		// stretch correction
		if (stretch < 0.0) {
			stretch = 0.0;
		} else if (stretch > 2.0) {
			stretch = 2.0;
		}

		//trace("getLambda()", "modifying 's' with stretch=" + stretch);
		for (int j = 0; j < numberOfDimensions; ++j) {
			s[0][j] += stretch * (s[0][j] - s[1][j]);
			s[ns-1][j] += stretch * (s[ns-1][j] - s[ns-2][j]);
		}

		// mafm: for benchmarking purposes
		//Timer timer = new Timer();
		for (int i = 0; i < numberOfPoints; ++i) {
			//trace("getLambda()", "lamix() call, loop " + i);

			// mafm: optimization, referencing instead of copying
			double[] vecx = x[i];

			// mafm: values lambda[i] and dist[i] expected to be
			// passed by reference (all arguments in Fortran work in
			// this way), which they are not in Java
			double[] tempsx = new double[numberOfDimensions];
			LamixResult lamixResult = new LamixResult();
			lamix(ns, numberOfDimensions, vecx, s, lamixResult, tempsx);

			// mafm: optimization, referencing instead of copying
			sx[i] = tempsx;

			lambda[i] = lamixResult.lambda;
			dist[i] = lamixResult.distmin;

			//trace("getLambda()", " - lambda= " + lambda[i] + ", dist=" + dist[i]);
		}
		//Logger.INFO("lamix loop()", " - done in " + timer.elapsed() + ", average " + timer.elapsedAverage(numberOfPoints));

		// mafm: reimplementation of sortdi() from Fortran. will take
		// the sorted order of the values ("lambda" is not modified)
		//
		// mafm: have to copy the results over, substituting the
		// variable doesn't work because the caller remains with the old
		// object
		int[] tagNew = QuicksortWithOrder.quicksortWithOrder(lambda);
		for (int i = 0; i < tag.length; ++i) {
			tag[i] = tagNew[i];
		}

		//trace("getLambda()", "getNewLambda() call");
		getNewLambda(numberOfPoints, numberOfDimensions, sx, lambda, tag);
	}


	/** Method translated from getlam.r
	 *
	 * Original code:
	 *
	 * subroutine newlam(n,p,sx,lambda,tag);
	 * integer n,p,tag(n);
	 * double precision sx(n,p),lambda(n),lami
	 *
	 * lambda(tag(1))=0d0;
	 * for(i=1;i<n;i=i+1){
	 *   lami=0d0
	 *   do j=1,p{
	 *     lami=lami+(sx(tag(i+1),j)-sx(tag(i),j))**2
	 *   }
	 *   lambda(tag(i+1))=lambda(tag(i))+dsqrt(lami);
	 * }
	 * return;
	 * end;
	 */
	public static void getNewLambda(int numberOfPoints,
					int numberOfDimensions,
					double[][] sx,
					double[] lambda,
					int[] tag) {
		lambda[tag[0]] = 0.0;

		for (int i = 0; i < (numberOfPoints-1); ++i) {
			double lami = 0.0;
			for (int j = 0; j < numberOfDimensions; ++j) {
				lami += Util.power2(sx[tag[i+1]][j] - sx[tag[i]][j]);
			}
			lambda[tag[i+1]] = lambda[tag[i]] + Math.sqrt(lami);
			//trace("getNewLambda()", "lambda[" + tag[i+1] + "]=" + lambda[tag[i+1]]);
		}
		// mafm: for tracing calculation results
		//UtilDebug.print("getNewLambda(), lambda", lambda);
	}


	/** Method translated from getlam.r
	 *
	 * Original code:
	 *
	 * subroutine lamix(ns,p,x,s,lambda,dismin,temps);
	 * integer ns,p;
	 * double precision lambda,x(p),s(ns,p),dismin,temps(p);
	 * double precision v(2,10),d1sqr,d2sqr,dismin,d12,dsqr,d1,w;
	 * real lam,lammin;
	 * integer left,right;
	 *
	 * # look for the closest segment
	 * dismin=1d60;lammin=1;
	 * for(ik = 1;ik<ns;ik=ik+1){
	 * 	d1sqr=0.0;
	 * 	d2sqr=0.0;
	 * 	do j=1,p{
	 * 		v(2,j)=x(j)-s(ik,j);
	 * 		v(1,j)=s(ik+1,j)-s(ik,j);
	 * 		d1sqr=d1sqr+v(1,j)**2;
	 * 		d2sqr=d2sqr+v(2,j)**2;
	 * 	}
	 * 	if(d1sqr.lt.1d-8*d2sqr){
	 * 		lam=ik;
	 * 		dsqr=d2sqr;
	 * 	}
	 * 	else{
	 * 		d12=0d0;
	 * 		do j=1,p{
	 * 			d12  = d12+v(1,j)*v(2,j);
	 * 		}
	 * 		d1=d12/d1sqr;
	 * 		if(d1.gt.1d0){
	 * 			lam=ik+1;
	 * 			dsqr=d1sqr+d2sqr-2d0*d12;
	 * 		}
	 * 		else if(d1.lt.0.0){
	 * 			lam=ik;
	 * 			dsqr=d2sqr;
	 * 		}
	 * 		else {
	 * 			lam=ik+(d1);
	 * 			dsqr=d2sqr-(d12**2)/d1sqr;
	 * 		}
	 * 	}
	 * 	if(dsqr.lt.dismin){
	 * 		dismin=dsqr;
	 * 		lammin=lam;
	 * 	}
	 * }
	 * left=lammin;
	 * if(lammin.ge.ns) left=ns-1;
	 * right=left+1;
	 * w=dble(lammin-left);
	 * do j=1,p{
	 * 	temps(j)=(1d0-w)*s(left,j)+w*s(right,j);
	 * }
	 * lambda=dble(lammin);
	 * return;
	 * end;
	 */
	public static void lamix(int ns,
				 int numberOfDimensions,
				 double[] vecx,
				 double[][] s,
				 LamixResult lamixResult,
				 double[] temps) {
		// mafm: the floats could be double, but are 'real' in the
		// original and maybe that affects a bit for rounding due to
		// different precisions... so better safe than sorry
		lamixResult.distmin = Double.valueOf(1.e60);
		float lammin = 0.0f;
		float lam = Float.NaN;

		// mafm: moved up here for performance reasons, so they're not
		// initialized every loop... trace() commented out for the same
		// reason, in some tests (depends on number of dimensions of the
		// data) the time executing this loop goes from ~7ms down to
		// ~0.025ms, it's a 300x speedup!  Just moving up the
		// declaration+definition of the vector cuts down time about
		// half, using by-hand multiplication instead of Util.power2()
		// doesn't seem to bring any benefit.
		//
		// mafm: number of dimensions != 10 all the time
		//double[][] v = new double[2][10];
		double[][] v = new double[2][numberOfDimensions];

		for (int ik = 0; ik < (ns-1); ++ik) {
			// mafm: note that indices in Fortran start with 1 as in
			// R, and unlike Java, so those variables depending on
			// incides have extra addition/subtraction of +/-1
			final int ikCorrected = ik + 1;

			double dsqr = 0.0;
			double d1sqr = 0.0;
			double d2sqr = 0.0;

			for (int j = 0; j < numberOfDimensions; ++j) {
				v[1][j] = vecx[j] - s[ik][j];
				v[0][j] = s[ik+1][j] - s[ik][j];
				d1sqr += Util.power2(v[0][j]);
				d2sqr += Util.power2(v[1][j]);
			}
			// mafm: commented out for performance, see above
			//trace("lamix()", " - d1sqr=" + d1sqr + ", d2sqr=" + d2sqr);

			if (d1sqr < (1.e-8 * d2sqr)) {
				lam = ikCorrected;
				dsqr = d2sqr;
			} else {
				double d12 = 0.0;
				for (int j = 0; j < numberOfDimensions; ++j) {
					d12 += v[0][j] * v[1][j];
				}
				double d1 = d12/d1sqr;
				if (d1 > 1.0) {
					lam = ikCorrected + 1;
					dsqr = d1sqr + d2sqr - (2.0*d12);
				} else if (d1 < 0.0) {
					lam = ikCorrected;
					dsqr = d2sqr;
				} else {
					lam = (float)(ikCorrected + d1);
					dsqr = d2sqr - (Util.power2(d12)/d1sqr);
				}
			}
			// mafm: commented out for performance, see above
			//trace("lamix()", " - values: lam=" + lam + ", dsqr=" + dsqr);

			// mafm: I used to have the additional check to prevent
			// for minute changes to change the path of the
			// calculation, but in this way it doesn't work as
			// expected in R/Fortran...
			//
			//double diff = Math.abs(dsqr - lamixResult.dist.doubleValue());
			//if (... && diff > 0.000001) {
			if (dsqr < lamixResult.distmin) {
				lamixResult.distmin = dsqr;
				lammin = lam;
				// mafm: commented out for performance, see above
				//trace("lamix()", " - set new lammin=" + lammin + ", distmin=" + lamixResult.distmin);
			}
		}

		/* left=lammin;
		 * if(lammin.ge.ns) left=ns-1;
		 * right=left+1;
		 * w=dble(lammin-left);
		 * do j=1,p{
		 * 	temps(j)=(1d0-w)*s(left,j)+w*s(right,j);
		 * }
		 * lambda=dble(lammin);
		 * return; */

		// mafm: R indices start in 1
		float lamminIndexCorrected = lammin - 1;
		int left = (int)lamminIndexCorrected;
		if (lamminIndexCorrected >= (ns-1)) {
			left = ns-2;
		}
		int right = left + 1;
		double w = (lamminIndexCorrected - left);
		//trace("lamix()", "w=" + w + ", left=" + left + ", right=" + right);

		// set temps[] to return back...
		for (int j = 0; j < numberOfDimensions; ++j) {
			temps[j] = ((1.0-w) * s[left][j]) + (w * s[right][j]);
		}

		lamixResult.lambda = lammin;
		//trace("lamix()", "returned lambda=" + lamixResult.lambda);
	}
}
