library(princurve)


"get.lam2" <- function(x, s, tag, stretch = 2)
{
  storage.mode(x) <- "double"
  storage.mode(s) <- "double"
  storage.mode(stretch) <- "double"
  if(!missing(tag))
    s <- s[tag,  ]
  np <- dim(x)
  if(length(np) != 2)
    stop("get.lam needs a matrix input")
  n <- np[1]
  p <- np[2]
  tt <- .Fortran("getlam",
                 n,
                 p,
                 x,
                 s = x,
                 lambda = double(n),
                 tag = integer(n),
                 dist = double(n),
                 as.integer(nrow(s)),
                 s,
                 stretch,
                 double(p),
                 double(p),
                 PACKAGE = "princurve")[c("s", "tag", "lambda", "dist")]
  #cat("dist[]=", tt$dist, "\n")
  #cat("length of dist[]=", length(tt$dist), "\n")
  tt$dist <- sum(tt$dist)
  class(tt) <- "principal.curve"
  tt
}


"principal.curve2" <-
  function(x, start = NULL, thresh = 0.001, plot.true = FALSE, maxit = 10,
           stretch = 2, smoother = "smooth.spline", trace = TRUE, ...)
{
  this.call <- match.call()
  dist.old <- sum(diag(var(x)))
  d <- dim(x)
  n <- d[1]
  p <- d[2]

  if(missing(start)) {
      xbar <- apply(x, 2, "mean")
      xstar <- scale(x, xbar, FALSE)
      svd.xstar <- svd(xstar)
      dd <- svd.xstar$d
      lambda <- svd.xstar$u[, 1] * dd[1]
      tag <- order(lambda)
      s <- scale(outer(lambda, svd.xstar$v[, 1]),  - xbar, FALSE)
      dist <- sum((dd^2)[-1]) * n
      start <- list(s = s, tag = tag, lambda = lambda, dist
                    = dist)
  }

  pcurve <- start
  it <- 0
  cat("Starting curve---distance^2: ", pcurve$dist, "\n")
  while(abs((dist.old - pcurve$dist)/dist.old) > thresh & it < maxit) {
  cat("dist.old=", dist.old, "\n")
  cat("pcurve.dist=", pcurve$dist, "\n")
  cat("==================> abs=", abs((dist.old - pcurve$dist)/dist.old), "\n")

    it <- it + 1
    s <- NULL
    for(j in 1:p) {
      sj <- switch(smoother,
                   lowess = lowess(pcurve$lambda, x[, j], ...)$y,
                   smooth.spline = smooth.spline(pcurve$lambda,
                       x[, j], ...)$y,
                   periodic.lowess = periodic.lowess(pcurve$lambda,
                       x[, j], ...)$y)
      s <- cbind(s, sj)
    }

    cat("Iteration ", it, "---distance^2: ", pcurve$dist, "\n")
    dist.old <- pcurve$dist

    #cat("tag before getlam"); print(pcurve$tag)
    #cat("s before getlam:\n"); print(s)
    pcurve <- get.lam2(x, s, stretch = stretch)
    #cat("Iteration ", it, "\npcurve$s after getlam:\n"); print(pcurve$s)
  }

  cat("STOPPED dist.old=", dist.old, "\n")
  cat("STOPPED pcurve.dist=", pcurve$dist, "\n")
  cat("STOPPED abs=", abs((dist.old - pcurve$dist)/dist.old), "\n")

  structure(list(s = pcurve$s, tag = pcurve$tag, lambda = pcurve$lambda,
                 dist = pcurve$dist, call = this.call),
            class = "principal.curve")
}




x2<-as.matrix(read.table("data3d-v2"))
#print(x2)

p2<-principal.curve2(x=x2, smoother = "smooth.spline", stretch=2.0, thres=0.001, df=4, maxit=10)
p2<-get.lam2(x=x2, s=p2$s, stretch=2.0)

#print(p2$dist)
#print(apply(p2$s, 2, "mean"))
#print(mean(apply(p2$s, 2, "mean")))
#print(mean(p2$lambda))



