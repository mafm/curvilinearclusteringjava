#from example_princlust.txt

#In this example, we use the OGLE LMC cepheids data.  
#We proceed
#through all steps of the process necessary to compute the BIC value

#mafm
#data <- read.table("allLMCceps.stripped")
data <- read.table("data3d")
#/mafm
sine.data = cbind(data[,1],data[,2])
# Source file to define noise cleaning functions
source("NNclean.r")

#remove estimated noise using NNclean 
sine.clean<-NNclean(sine.data,k=2)
#sine.clean<-sine.data

#make initial clustering (into six clusters) using hclust
#data.dist = dist(sine.data[sine.clean$z==1,],method="eu")
#clust.hclust.wa <- hclust(data.dist, method="wa")
#curve1.hclust <- cutree(clust.hclust.wa, k=6)

#make initial clustering (into six clusters) using kmeans
#curve1.kmeans <- kmeans(sine.data[sine.clean$z==1,],9)

#make initial clustering (into six clusters) using mclust
library("mclust")
ncG<-6
curve1.mclust<-Mclust(sine.data[sine.clean$z==1,],G=ncG)
#curve1.mclust<-Mclust(sine.data,G=ncG)


#HPCC cluster down from 6 clusters to 1
#we can specify degrees of freedom here and again in cem, or
#approximate by using the default value for degrees of freedom 
#in hpcc
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.classes6$classification,force=T,forcenum=1,
#  plot.iter=F,plot.result=F)
source("hpcc.r")
library(princurve)
#source("principal.curve.R")
nchpcc<-6
dfhpcc <- 4
#curve1.hpcc<-hpcc(sine.data,
curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
  clust.init=curve1.mclust$classification,force=T,forcenum=nchpcc,
  plot.iter=F,plot.result=F,df=dfhpcc)
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.hclust,force=T,forcenum=nchpcc,
#  plot.iter=F,plot.result=F,df=dfhpcc)
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.kmeans$cluster,force=T,forcenum=nchpcc,
#  plot.iter=F,plot.result=F,df=dfhpcc)


#make raw classification by combining hpcc and nnclean results
#(with only 1 feature, we can skip hpcc and use curve1.raw<-sine.clean$z)
curve1.raw<-rep(NA,length(sine.data[,1]))
counter<-0
for (i in 1:length(sine.data[,1])) {
if (sine.clean$z[i]==0) {curve1.raw[i]<-0}
if (sine.clean$z[i]==1) {counter<-counter+1
                     curve1.raw[i]<-curve1.hpcc$classes[counter]}
}
temp<-unique(curve1.hpcc$classes)
curve1.raw[curve1.raw==temp[1]]<-1

#mafm
#source("cempcc.r")
source("cempcc.r")
data.set<-"data3d-"
data.filename<-paste(data.set, "points", sep="")
clust.init.filename<-paste(data.set, "initclust", sep="")
write.table(sine.data,file=data.filename,row.names=F,col.names=F)
write.table(curve1.raw,file=clust.init.filename,row.names=F,col.names=F)
#debug(get.lam)
#/mafm

#mafm
#/mafm

#refine the clustering with CEM
dfcempcc <- 4
#curve1.cem<-cem(x=sine.data,clust.init=curve1.hpcc,
curve1.cem<-cem(x=sine.data,clust.init=curve1.raw,
#mafm
#   max.iter=12,plot.me=F,trace=T,trim=0,df=dfcempcc)
   max.iter=12,plot.me=F,trace=F,trim=0,df=dfcempcc)
#/mafm

#mafm
#stop("stop not wanting to overwrite files")
write.table(curve1.cem$var.bound,file=paste(data.set, "cem_varbound", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$iterations,file=paste(data.set, "cem_iterations", sep=""),row.names=F,col.names=F)

write.table(curve1.cem$clust,file=paste(data.set, "cem_clustering", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$clust.best,file=paste(data.set, "cem_bestclustering", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$clust.log,file=paste(data.set, "cem_clusteringlog", sep=""),row.names=F,col.names=F)

write.table(curve1.cem$like.about,file=paste(data.set, "cem_likelihoodsabout", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$like.along,file=paste(data.set, "cem_likelihoodsalong", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$likelihoods,file=paste(data.set, "cem_likelihoods", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$likelihoods.mix,file=paste(data.set, "cem_likelihoodsmix", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$overall.loglikelihood,file=paste(data.set, "cem_ovloglike", sep=""),row.names=F,col.names=F)

write.table(curve1.cem$all.curvevars,file=paste(data.set, "cem_allcurvevars", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.curvelengths,file=paste(data.set, "cem_allcurvelengths", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.meanalong,file=paste(data.set, "cem_allmeanalong", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.varalong,file=paste(data.set, "cem_allvaralong", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.mixprop,file=paste(data.set, "cem_allmixprop", sep=""),row.names=F,col.names=F)
#/mafm

#compute BIC value
#mafm
#calc.likelihood(curve1.cem,mix.best=T)$bic
calc<-calc.likelihood(curve1.cem,mix.best=T)
calc.score.filename<-paste(data.set, "calc_score", sep="")
calc.bic.filename<-paste(data.set, "calc_bic", sep="")
calc.bic2.filename<-paste(data.set, "calc_bic2", sep="")
write.table(calc$score,file=calc.score.filename,row.names=F,col.names=F)
write.table(calc$bic,file=calc.bic.filename,row.names=F,col.names=F)
write.table(calc$bic2,file=calc.bic2.filename,row.names=F,col.names=F)
#/mafm

#print(curve1.hpcc$classes)
#print(curve1.hpcc$totalvar.track)
#clust.init.filename<-paste(data.set, "-initclust", sep="")
#clust.final.filename<-paste(data.set, "-finalclust", sep="")
#write.table(curve1.kmeans$cluster,file=clust.init.filename,row.names=F,col.names=F)
#write.table(curve1.hpcc$classes,file=clust.final.filename,row.names=F,col.names=F)
