library(princurve)
source("principal.curve.R")

n=5
p=2

sx<-(cbind(c(2, 5, 3, 4, 1), c(12, 35, 23, 41, 21)))

lambda<-c(2, 5, 3, 4, 1)

tag<-c(5, 1, 3, 4, 2)

tt <- .Fortran("newlam",
		 n,
		 p,
		 sx,
		 lambda,
		 tag,
                 PACKAGE = "princurve")[c("lambda", "tag")]
cat(tt$lambda)
cat(tt$tag)

do i=1,n{tag(i)=i}

for (i in 1:n)
  {
    tag[i]<-i
    cat(tag[i])
  }

tt <- .Fortran("sortdi",
		 lambda,
		 tag,
		 1,
		 n,
                 PACKAGE = "princurve")[c("lambda", "tag")]

cat(tt$lambda)
cat(tt$tag)