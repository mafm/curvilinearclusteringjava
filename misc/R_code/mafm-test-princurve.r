library(princurve)
source("principal.curve.R")

x<-(cbind(c(2, 5, 3, 4, 1), c(12, 35, 23, 41, 21)))
x

pcurve.result <- principal.curve(x, smoother="smooth.spline", thresh=0.001, maxit=10, df=5)

cat(pcurve.result$lambda)
cat(pcurve.result$tag)
cat(pcurve.result$dist)

#do i=1,n{tag(i)=i}
#
#for (i in 1:n)
#  {
#    tag[i]<-i
#    cat(tag[i])
#  }
