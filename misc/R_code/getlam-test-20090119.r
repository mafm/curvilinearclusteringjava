library(princurve)

colClasses<-c("double", "double")
data <- read.table("x", colClasses=colClasses)
x = cbind(data[,1], data[,2])
#x

data <- read.table("s", colClasses=colClasses)
s = cbind(data[,1], data[,2])
#s

storage.mode(x)
storage.mode(s)

n=nrow(s)
p=ncol(s)
cat("n=", n, ", p=", p, "\n")

result <- get.lam(x, s, stretch=2.0)

write.table(result$s, "mafm/pcurve.s", row.names=F, col.names=F)
write.table(result$lambda, "mafm/pcurve.lambda", row.names=F, col.names=F)
write.table(result$dist, "mafm/pcurve.dist", row.names=F, col.names=F)
write.table(result$tag, "mafm/pcurve.tag", row.names=F, col.names=F)
