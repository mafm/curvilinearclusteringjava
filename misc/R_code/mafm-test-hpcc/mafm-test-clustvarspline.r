library(princurve)
source("hpcc.r")

data <- as.matrix(read.table("data"))
data

clustvarspline.result <- clust.var.spline(data, dg=5, alpha=0.4)
cat("clustvarspline.result:\n"); print(clustvarspline.result)













if (FALSE) {

alpha=.4
order <- c(7, 8, 6, 5, 4, 10, 2, 3, 1, 9)
temp<-list(s=data, tag=order)

	vabout<-13.82799
	nn<-length(temp$s[,1])
	p<-length(temp$s[1,])
#mafm
        cat("temp$s:\n"); print(temp$s)
        cat("temp.tag: ",temp$tag,"\n")
        cat("vabout (= temp$dist): ",vabout,"\n")
        cat("nn: ",nn,"\n")
#/mafm

# For closed principal curves, the nn index in epsilon should wrap
# around, so length(epsilon) = nn.  For open curves, we have nn-1 segments.
# We must put the points in the s matrix in the correct order before
# calculating variance along the curve.  The ordering is provided by
# the $tag value from principal.curve()

        news<-temp$s[temp$tag,]
	epsilon<-news[1:(nn-1),]-news[2:nn,]
	mu<-apply(epsilon,2,mean)

# Changed by LSB to handle multidimensional (p>2) arrays
#	mu<-matrix(mu,byrow=T,ncol=2,nrow=nn-1)
	mu<-matrix(mu,byrow=T,ncol=p,nrow=nn-1)
# END CHANGE
	valong<-(.5)*vdist(epsilon,mu)

    total<-alpha*vabout+valong

#mafm
        cat("news:\n");	print(news)
        cat("epsilon:\n"); print(epsilon)
        cat("mu matrix:\n"); print(mu)
        cat("valong:\n"); print(valong)
        cat("total:\n"); print(total)
#/mafm

}