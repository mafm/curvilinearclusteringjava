x1<-as.matrix(read.table("dist.array.R.txt"))
x2<-as.matrix(read.table("dist.array.java.txt"))
print(x1)

for (i in 1:length(x1)) {
    x1e<-round(x1[i], 5)
    x2e<-round(x2[i], 5)
    if (abs(x1e-x2e)>0.05) {
       cat("Element ", i, ": ", x1e, "<>", x2e, "\n")
    }
}