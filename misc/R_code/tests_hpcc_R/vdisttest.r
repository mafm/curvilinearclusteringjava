#------------------------------------------------------------------

vdist<-function(x,y)  {
	print ("x:")
	print (x)
	print ("y:")
	print (y)
#x, y are n x p matrices.  Each row is a point.
        total<-0
        n<-length(x[,1])
        p<-length(x[1,])
        for(i in 1:n) {
              for(j in 1:p) {total<-total+((x[i,j]-y[i,j])^2) }   
                        }
	print ("total:")
	print (total)
	print ("--")
        total}

#------------------------------------------------------------------


vdisttest<-function() {

a1<- c(4.505369e-05, 4.569075e-05, 3.674143e-04, 7.535985e-06, 4.126828e-05)
a2<- c(-4.600069e-03, -4.666267e-03, -3.757116e-02, -7.728143e-04, -4.233124e-03)
a<- cbind(a1, a2)

b1<- c(5.329031e-05, 5.329031e-05, 5.329031e-05, 5.329031e-05, 5.329031e-05)
b2<- c(-0.002368330, -0.002368330, -0.002368330, -0.002368330, -0.002368330)
b<- cbind(b1, b2)

res<-vdist(a, b)
}


