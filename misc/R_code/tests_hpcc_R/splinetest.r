#------------------------------------------------------------------
clust.var.spline<-function(x,dg=5,alpha=.4) {
#This is the function which calls principal.curve
#if there are fewer than 7 points in a cluster, we fit a principal component
#line instead of a principal curve.  


#fewer than seven points
     if(length(x[,1])<7) {

	temp<-prcomp(cbind(x[,1],x[,2]))
	temp.slope<-temp$rot[2,1]/temp$rot[1,1]
	temp.int<-mean(x[,2])-(temp.slope)*(mean(x[,1]))
	temp.coef<-c(temp.int,temp.slope)
	a<-projpoints(x,temp.coef)

	vabout<-vdist(x,a)
	nn<-length(x[,1])
	epsilon<-a[1:(nn-1),]-a[2:nn,]
	mu<-apply(epsilon,2,mean)
	mu<-matrix(mu,byrow=T,ncol=2,nrow=nn-1)
	valong<-(.5)*vdist(epsilon,mu)
                           }

#more than seven points
     else {
	print("x for principal curve:")
	print(x)
	temp<-principal.curve(x,plot.true=FALSE,df=dg)
#	print("temp:")
#	print(temp)
	vabout<-temp$dist
	nn<-length(temp$s[,1])

# For closed principal curves, the nn index in epsilon should wrap
# around, so length(epsilon) = nn.  For open curves, we have nn-1 segments.
# We must put the points in the s matrix in the correct order before
# calculating variance along the curve.  The ordering is provided by
# the $tag value from principal.curve()

        news<-temp$s[temp$tag,]
	epsilon<-news[1:(nn-1),]-news[2:nn,]
	mu<-apply(epsilon,2,mean)
print("temp.s:")
print(temp$s)
print("news:")
print(news)
print("epsilon:")
print(epsilon)
print("mu:")
print(mu)

	mu<-matrix(mu,byrow=T,ncol=2,nrow=nn-1)

print("mu-matrix:")
print(mu)

	valong<-(.5)*vdist(epsilon,mu)

print("valong:")
print(valong)
        }
    total<-alpha*vabout+valong

print("total:")
print(total)
    total}

#------------------------------------------------------------------

vdist<-function(x,y)  {
#x, y are n x p matrices.  Each row is a point.
        total<-0
        n<-length(x[,1])
        p<-length(x[1,])
        for(i in 1:n) {
              for(j in 1:p) {total<-total+((x[i,j]-y[i,j])^2) }   
                        }
        total}

#------------------------------------------------------------------

splinetest<-function() {
	x1<- c(0.105910, 0.123912, 0.103066, 0.099921, 0.162130, 0.085472, 0.179259, 0.181219, 0.192046, 0.121756)
	x2<- c(-0.368503, -0.358562, -0.343040, -0.406566, -0.354804, -0.386859, -0.342802, -0.353090, -0.365219, -0.385133)
	x<- cbind(x1, x2)

	res<-clust.var.spline(x, dg=4, alpha=0.4)
	print("result:")
	print(res)
}


