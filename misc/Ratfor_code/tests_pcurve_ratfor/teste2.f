C Output from Public domain Ratfor, version 1.01
      subroutine printmatrix(m, rows, columns)
      integer rows, columns
      double precision m(rows, columns)
10    format(f8.3,f8.3,f8.3)
      do23000 i=1,rows 
      print 10, m(i, 1), m(i, 2), m(i, 3)
      do23002 j=1,columns 
23002 continue
23003 continue
23000 continue
23001 continue
      end
      program test
      integer n, p, ns
      parameter (n=1, p=3)
      double precision x(n, p), sx(n, p), s(n, p), lambda(n), dist(n), t
     *empsx(n, p), vecsx(p)
      integer tag(n)
      double precision javai, javaj, prod, base, strech
11    format(i4)
12    format(f8.3)
      print *, "*** initialiting..."
      do23004 i=1,n 
      ii=i*10
      do23006 j=1,p 
      x(i, j)= ((i+j)**2)
      sx(i, j)=x(i, j)
      s(i, j)=(i+j)*p/(n*1.0) + x(i, j)
23006 continue
23007 continue
      tag(i)=i
      lambda(i)=0d0
      dist(i)=0d0
23004 continue
23005 continue
      strech=2d0
      ns=n
      print *, "*** data"
      print *, "n=", n
      print *, "p=", p
      print *, "x:"
      call printmatrix(x, n, p)
      print *, "sx:"
      call printmatrix(sx, n, p)
      print *, "s:"
      call printmatrix(s, n, p)
      print *, "*** calling getlam()..."
      call getlam(n, p, x, sx, lambda, tag, dist, n, s, strech, vecsx, t
     *empsx)
      print *, "*** printing results"
      print *, "tag:"
      do23008 i=1,n 
      print 11, tag(i)
23008 continue
23009 continue
      print *, "lambda:"
      do23010 i=1,n 
      print 12, lambda(i)
23010 continue
23011 continue
      print *, "dist:"
      do23012 i=1,n 
      print 12, dist(i)
23012 continue
23013 continue
      print *, "*** printing modifications over original data"
      print *, "x:"
      call printmatrix(x, n, p)
      print *, "sx:"
      call printmatrix(sx, n, p)
      print *, "s:"
      call printmatrix(s, n, p)
      print *, "*** finishing"
      end
