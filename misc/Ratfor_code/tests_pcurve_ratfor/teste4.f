C Output from Public domain Ratfor, version 1.01
      subroutine printmatrix(m, rows, columns)
      integer rows, columns
      double precision m(rows, columns)
10    format(f8.3,f8.3)
      do23000 i=1,rows 
      print 10, m(i, 1), m(i, 2)
      do23002 j=1,columns 
23002 continue
23003 continue
23000 continue
23001 continue
      end
      program test
      integer n, p, ns
      parameter (n=10, p=2)
      double precision x(n, p), sx(n, p), s(n, p), lambda(n), dist(n), t
     *empsx(n, p), vecsx(p)
      integer tag(n)
      double precision javai, javaj, prod, base, strech
11    format(i4)
12    format(f8.3)
      print *, "*** initialiting..."
      x(1, 1)=-7.1465695
      x(2, 1)=-7.09588461
      x(3, 1)=-.00568917143
      x(4, 1)=-6.95107063
      x(5, 1)=-6.84246016
      x(6, 1)=-.711688014
      x(7, 1)=-4.30821565
      x(8, 1)=-6.0821868
      x(9, 1)=254.904865
      x(10, 1)=-3.0410934
      x(1, 2)=-7.52670617
      x(2, 2)=-7.41266517
      x(3, 2)=-.0139301036
      x(4, 2)=-7.08683373
      x(5, 2)=-6.84246016
      x(6, 2)=-1.85413456
      x(7, 2)=-1.99571755
      x(8, 2)=-5.13184512
      x(9, 2)=93.638522
      x(10, 2)=1.71061504
      s(1, 1)=-6.7465695
      s(2, 1)=-6.49588461
      s(3, 1)=.794310829
      s(4, 1)=-5.95107063
      s(5, 1)=-5.64246016
      s(6, 1)=.688311986
      s(7, 1)=-2.70821565
      s(8, 1)=-4.2821868
      s(9, 1)=256.904865
      s(10, 1)=-.841093402
      s(1, 2)=-6.92670617
      s(2, 2)=-6.61266517
      s(3, 2)=.986069896
      s(4, 2)=-5.88683373
      s(5, 2)=-5.44246016
      s(6, 2)=-.254134562
      s(7, 2)=-.195717545
      s(8, 2)=-3.13184512
      s(9, 2)=95.838522
      s(10, 2)=4.11061504
      do23004 i=1,n 
      ii=i*10
      do23006 j=1,p 
      sx(i, j)=x(i, j)
23006 continue
23007 continue
      tag(i)=i
      lambda(i)=0d0
      dist(i)=0d0
23004 continue
23005 continue
      strech=2d0
      ns=n
      print *, "*** data"
      print *, "n=", n
      print *, "p=", p
      print *, "strech=", strech
      print *, "x:"
      call printmatrix(x, n, p)
      print *, "sx:"
      call printmatrix(sx, n, p)
      print *, "s:"
      call printmatrix(s, n, p)
      print *, "*** calling getlam()..."
      call getlam(n, p, x, sx, lambda, tag, dist, n, s, strech, vecsx, t
     *empsx)
      print *, "*** printing results"
      print *, "tag:"
      do23008 i=1,n 
      print 11, tag(i)
23008 continue
23009 continue
      print *, "lambda:"
      do23010 i=1,n 
      print 12, lambda(i)
23010 continue
23011 continue
      print *, "dist:"
      do23012 i=1,n 
      print 12, dist(i)
23012 continue
23013 continue
      print *, "*** printing modifications over original data"
      print *, "x:"
      call printmatrix(x, n, p)
      print *, "sx:"
      call printmatrix(sx, n, p)
      print *, "s:"
      call printmatrix(s, n, p)
      print *, "*** finishing"
      end
