integer n
integer p

double precision sx(3,5), lambda(3)
integer order(3)


n=3
p=5
print *, "n=", n
print *, "p=", p

for(i = 1; i <= n; i=i+1) {
      	for(j = 1; j <= p; j=j+1) {
              sx(i,j) = ((i-1) * sin(real((i-1)*(j-1)))) + (j-1);
              print *, "sx(", i, ",", j, "): ", sx(i,j)
        }
}

for(i = 1; i <= n; i=i+1) {
      lambda(i) = mod((cos(real(i-1)) * n), 10);
      print *, "lambda(", i, "): ", lambda(i)
}

for(i = 1; i <= n; i=i+1) {
      order(i) = mod((i), n) + 1;
      print *, "order(", i, "): ", order(i)
}


call sortdi(lambda,order,1,n)
for(i = 1; i <= n; i=i+1) {
      print *, "lambda(", i, "): ", lambda(i)
}
for(i = 1; i <= n; i=i+1) {
      print *, "order(", i, "): ", order(i)
}
call newlam(n,p,sx,lambda,order)
for(i = 1; i <= n; i=i+1) {
      print *, "final lambda(", i, "): ", lambda(i)
}
for(i = 1; i <= n; i=i+1) {
      print *, "final order(", i, "): ", order(i)
}

print *, 1D0, 1D-1, 0D0, 0D-1, 1D60/1D40, 2D0

end
