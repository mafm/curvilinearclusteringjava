/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.io.*;


/** Point of undetermined dimension
 * 
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
class Point {
	/** Delta for comparisons */
	public static final double delta = 0.000001;

	/** Raw data */
	public final double[] data;


	/** Constructor
	 *
	 * @param data data to create the point
	 */
	public Point(double[] data) {
		this.data = data;
	}

	/** Difference between points
	 *
	 * @param other The other point
	 *
	 * @return Point representing the difference (a vector, really)
	 */
	public Point difference(Point other) {
		if (this.data.length != other.data.length) {
			String msg = "Point.difference(): can't be performed when points have different dimension";
			throw new RuntimeException(msg);
		}

		double[] newPointData = new double[data.length];
		for (int i = 0; i < this.data.length; ++i) {
			newPointData[i] = this.data[i] - other.data[i];
		}

		return new Point(newPointData);
	}

	/** Equals (having delta into account)
	 *
	 * @param other The other point
	 *
	 * @return Whether the Points contain the same data
	 */
	@Override public boolean equals(Object oOther) {
		Point other = null;
		if (! (oOther instanceof Point)) {
			return false;
		} else {
			other = (Point) oOther;
		}

		if (this.data.length != other.data.length) {
			return false;
		} else {
			for (int i = 0; i < this.data.length; ++i) {
				double diff = other.data[i] - this.data[i];
				if (diff < 0.0)
					diff = -diff;
				if (diff > delta)
					return false;
			}
			// no differences -> they are equal
			return true;
		}
	}

	/** Convert to string -- mostly for debugging purposes
	 *
	 * @return Point in string representation: "(x1, x2, ..., xn)"
	 */
	public String toString() {
		String s = "(";
		for (int i = 0; i < (this.data.length-1); ++i) {
			s += this.data[i] + ", ";
		}
		s += this.data[data.length-1] + ")";
		return s;
	}
}
