	public static int[] sortdi(double[] lambda) {
		//mafm: for benchmarking purposes
		Timer timer = new Timer();

		int[] result = null;

		int[] order = new int[lambda.length];
		for (int i = 0; i < order.length; ++i) {
			order[i] = i+1;
		}

		try {
			// create dir and base file
			File dir = wipeAndCreateDir("RWrapper");
			File tmp = File.createTempFile("sortdi-", "", dir);

			File source = new File(tmp + ".source");
			File in_lambda = new File(tmp + ".in.lambda");
			File in_order = new File(tmp + ".in.order");
			File out = new File(tmp + ".out");

			UtilIO.writeArrayDouble(in_lambda.toString(), lambda);
			UtilIO.writeArrayInt(in_order.toString(), order);

			// write source file to be interpreted by R
			PrintWriter writer = new PrintWriter(source);
			writer.println("library(princurve)");
			writer.println("lambda<-as.matrix(read.table('" + in_lambda + "'))");
			writer.println("order<-as.matrix(read.table('" + in_order + "'))");
			writer.println(".Fortran(\"sortdi\", lambda, order, 1, length(lambda))[c(\"lambda\", \"order\", \"start\", \"end\")]");
			writer.println("write.table(order,file='" + out + "',row.names=F,col.names=F)");
			writer.flush();
			writer.close();

			// execute in R
			instance().execute(dir, "source('" + source + "', echo=TRUE)\n");

			// gather results
			result = UtilIO.readArrayInt(out.toString());

		} catch (Exception e) {
			throw new RuntimeException("RWrapper.smoothSplines(): "  + e.getMessage());
		}

		// R indices start in 1...
		for (int i = 0; i < order.length; ++i) {
			order[i] -= 1;
		}

		UtilDebug.print("new [world] order:", order);

		Logger.DEBUG("RWrapper.sortdi()", " - done in " + timer.elapsed());
		return result;
	}
