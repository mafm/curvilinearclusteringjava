/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.io.*;
import java.text.DecimalFormat;


/** 
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class RSmoothSpline {

	public static double[] smooth_spline(double[] lambda, double[] x, int df) {
		//Logger.DEBUG("R", "lambda: " + Arrays.toString(lambda));
		//Logger.DEBUG("R", "x: " + Arrays.toString(x));

		double[] result = null;

		try {
			File tmp = new File("toR-smooth.spline-");
			PrintWriter writer = new PrintWriter(tmp);

			writer.print("lambda<-c(");
			for (int i = 0; i < (lambda.length-1); ++i) {
				writer.print(lambda[i] + ", ");
			}
			writer.println(lambda[lambda.length-1] + "); ");

			writer.print("x<-c(");
			for (int i = 0; i < (x.length-1); ++i) {
				writer.print(x[i] + ", ");
			}
			writer.println(x[x.length-1] + "); ");

			writer.println("fit<-smooth.spline(lambda, x, df=" + df + ", keep.data=FALSE);");
			writer.println("result.y=predict(fit, x=lambda)$y");
			writer.println("write.table(result.y,file=\"" + tmp.getAbsolutePath() +".y\",row.names=F,col.names=F);");

			writer.flush();
			writer.close();

			//Logger.DEBUG("R", "file: " + tmp.getAbsolutePath());

			// retrieve result
			String command = "(/usr/bin/R --vanilla --silent < "
				+ tmp.getAbsolutePath()
				+ ") | grep -v '^>' | cut -d']' -f 2- | cut -c2- > "
				+ tmp.getAbsolutePath() + ".output";
			writer = new PrintWriter(tmp + ".command");
			writer.println(command);
			writer.flush();
			writer.close();

			Process p = java.lang.Runtime.getRuntime().exec("/bin/bash -- " + tmp + ".command");
			p.waitFor();
/*
			Logger.DEBUG("R", "stderr: ");
			for (int b = 0; b != -1; b = p.getErrorStream().read()) {
				System.err.print((char)b);
			}
			System.err.println();

			Logger.DEBUG("R", "stdout: ");
			for (int b = 0; b != -1; b = p.getInputStream().read()) {
				System.err.print((char)b);
			}
			System.err.println();
*/

			result = UtilIO.readArrayDouble(tmp.getAbsolutePath() + ".y");
			//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			//Logger.DEBUG("R", "result.length: " + result.length);

		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error writing data: "  + e.getMessage());
			throw new RuntimeException("smooth spline: Error getting splines: "  + e);
		}

		return result;
	}
}

