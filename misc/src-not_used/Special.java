/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;
import java.util.HashMap;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class Special extends TestCase {

	/** Base path for filenames */
	private static final String basePath_HPCC = "data/test/hpcc/";
	private static final String basePath_CEMPCC = "data/test/cempcc/";
	private static String basePath = null;


	/** Test HPCC.hpcc() */
	public void test_hpcc() {
		basePath = basePath_HPCC;

		//helper_test_hpcc("data3d", false, 0);
		//helper_test_hpcc("data5d", false, 0);
		//helper_test_hpcc("arcs3d_noise", false, 0);
		//helper_test_hpcc("arcs3d_noise", true, 4);
	}


	/** Test CEMPCC.cem() */
	public void test_cem() {
		basePath = basePath_CEMPCC;

		helper_test_cem("curves2d_noforced");
		helper_test_cem("curves2d_forced6");
		helper_test_cem("data3d");
		helper_test_cem("data5d");
		helper_test_cem("arcs3d");
		helper_test_cem("arcs3d_noise");
	}


	/** Helper */
	private void helper_test_hpcc(String dataName, boolean force, int forcenum) {
		final String baseName = basePath + dataName + "-hpcc-";
		final String variety = (force ? ("force" + forcenum) : "noforce");
		final String baseNameResult = baseName + variety + "-";
		final String testName = "test_hpcc_" + dataName + "_" + variety + "()";

		// data
		double[][] points = UtilIO.readMatrixDouble(baseName + "in.x");
		int[] initialClustering = UtilIO.readArrayInt(baseName + "in.initclust");
		int degreesOfFreedom = UtilIO.readInt(baseName + "in.df");

		// expected result, final clustering
		int[] expectedResult = UtilIO.readArrayInt(baseNameResult + "out.finalclust");

		HPCC.TRACE = true;
		HPCC.Result result = HPCC.hpcc(points,
					       initialClustering,
					       HPCC.DEFAULT_ALPHA,
					       degreesOfFreedom,
					       force,
					       forcenum);

		UtilIO.writeArrayInt(baseNameResult + "out.finalclust-java_only", result.classes);

		// mafm: sometimes clusters are the same, but just have
		// different "names" (integer numbers), so we attempt to fix
		// this glitch

		// first we check that the number of clusters is the same
		int[] uniqueClustersExpected = Util.unique(expectedResult);
		int[] uniqueClustersObtained = Util.unique(result.classes);
		if (uniqueClustersExpected.length != uniqueClustersObtained.length) {
			Comparison.assertInt(testName + " number of clusters differ",
					     uniqueClustersExpected.length, uniqueClustersObtained.length);
		}

		// then we try to find the correspondence/mapping between
		// clusters: if expected cluster point[i] is 'n', then their
		// "alias" of the cluster in the result we assume that it's the
		// cluster of result point[i], hoping that it's always the same
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < result.classes.length; ++i) {
			Integer oldValue = map.put(expectedResult[i], result.classes[i]);
			if (oldValue != null && oldValue != result.classes[i]) {
				// ops, we had already mapped it, but had a
				// different value
				Comparison.assertInt(testName + " mapping of clusters are not exact",
						     oldValue.intValue(), result.classes[i]);
			} else {
				// we replace the value, hoping that it's a
				// perfect mapping between both results
				result.classes[i] = expectedResult[i];
			}
		}

		// now we make the comparison based on the "aliased" cluster
		// names: for all points, if point[i] has clusterA in the
		// expected result, then point[i] of the result must be the
		// alias clusterA'
		Comparison.assertArrayInt(testName + " result (possibly changing cluster numbers)", expectedResult, result.classes);
	}

	/** Helper */
	public void helper_test_cem(String dataName) {
		final String baseName = basePath + dataName + "-cempcc-";
		final String testName = "test_cem_" + dataName + "()";

		// error admitted depending on version of lower level functions
		// used
		double ERROR_ADMITTED = 0.0;
		if (PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD) {
			ERROR_ADMITTED = Comparison.Error.ONE_IN_HUNDRED_THOUSAND;
		} else {
			ERROR_ADMITTED = Comparison.Error.ONE_IN_MILLION;
		}

		// data
		double[][] points = UtilIO.readMatrixDouble(baseName + "in.x");
		int[] initialClustering = UtilIO.readArrayInt(baseName + "in.initclust");
		int degreesOfFreedom = UtilIO.readInt(baseName + "in.df");

		// expected result
		CEMPCC.Result expected = new CEMPCC.Result();
		expected.varBound = UtilIO.readDouble(baseName + "out.cem_varbound");
		expected.iterations = UtilIO.readInt(baseName + "out.cem_iterations");
		expected.clustering = UtilIO.readArrayInt(baseName + "out.cem_clustering");
		expected.bestClustering = UtilIO.readArrayInt(baseName + "out.cem_bestclustering");
		expected.clusteringLog = UtilIO.readArrayInt(baseName + "out.cem_clusteringlog");
		expected.likelihoodsAbout = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoodsabout");
		expected.likelihoodsAlong = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoodsalong");
		expected.likelihoods = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoods");
		expected.likelihoodsMix = UtilIO.readMatrixDouble(baseName + "out.cem_likelihoodsmix");
		expected.overallLogLikelihood = UtilIO.readArrayDouble(baseName + "out.cem_ovloglike");
		expected.allCurveVars = UtilIO.readMatrixDouble(baseName + "out.cem_allcurvevars");
		expected.allCurveLengths = UtilIO.readMatrixDouble(baseName + "out.cem_allcurvelengths");
		expected.allMeanAlong = UtilIO.readMatrixDouble(baseName + "out.cem_allmeanalong");
		expected.allVarAlong = UtilIO.readMatrixDouble(baseName + "out.cem_allvaralong");
		expected.allMixProp = UtilIO.readMatrixDouble(baseName + "out.cem_allmixprop");
		// expected result 2
		CEMPCC.CalculateLikelihoodResult expected2 = new CEMPCC.CalculateLikelihoodResult();
		expected2.score = UtilIO.readDouble(baseName + "out.calc_score");
		expected2.bic = UtilIO.readDouble(baseName + "out.calc_bic");
		expected2.bic2 = UtilIO.readDouble(baseName + "out.calc_bic2");

		// calculations
		CEMPCC.Result result = CEMPCC.cem(points,
						  degreesOfFreedom,
						  initialClustering,
						  CEMPCC.DEFAULT_MAX_ITERATIONS,
						  CEMPCC.DEFAULT_TRIM,
						  CEMPCC.DEFAULT_MIX,
						  CEMPCC.DEFAULT_VAR_BOUND);

		// calculations 2
		CEMPCC.CalculateLikelihoodResult result2 = CEMPCC.calculateLikelihood(result, true);

		UtilIO.writeArrayInt(baseName + "out.cem_bestclustering-java_only", result.bestClustering);
		/*
		UtilIO.writeArrayInt(baseName + "out.cem_clustering", result.clustering);
		UtilIO.writeArrayInt(baseName + "out.cem_clusteringlog", result.clusteringLog);

		UtilIO.writeDouble(baseName + "out.cem_varbound", result.varBound);
		UtilIO.writeInt(baseName + "out.cem_iterations", result.iterations);

		UtilIO.writeMatrixDouble(baseName + "out.cem_likelihoodsabout", result.likelihoodsAbout);
		UtilIO.writeMatrixDouble(baseName + "out.cem_likelihoodsalong", result.likelihoodsAlong);
		UtilIO.writeMatrixDouble(baseName + "out.cem_likelihoods", result.likelihoods);
		UtilIO.writeMatrixDouble(baseName + "out.cem_likelihoodsmix", result.likelihoodsMix);
		UtilIO.writeArrayDouble(baseName + "out.cem_ovloglike", result.overallLogLikelihood);

		UtilIO.writeMatrixDouble(baseName + "out.cem_allcurvevars", result.allCurveVars);
		UtilIO.writeMatrixDouble(baseName + "out.cem_allcurvelengths", result.allCurveLengths);
		UtilIO.writeMatrixDouble(baseName + "out.cem_allmeanalong", result.allMeanAlong);
		UtilIO.writeMatrixDouble(baseName + "out.cem_allvaralong", result.allVarAlong);
		UtilIO.writeMatrixDouble(baseName + "out.cem_allmixprop", result.allMixProp);

		UtilIO.writeDouble(baseName + "out.calc_score", result2.score);
		UtilIO.writeDouble(baseName + "out.calc_bic", result2.bic);
		UtilIO.writeDouble(baseName + "out.calc_bic2", result2.bic2);
		*/

		// compare results
		Comparison.assertInt(testName + " degreesOfFreedom",
				     degreesOfFreedom, result.degreesOfFreedom);
		Comparison.assertDouble(testName + " varBound",
					expected.varBound, result.varBound, ERROR_ADMITTED);
		Comparison.assertInt(testName + " iterations",
				     expected.iterations, result.iterations);

		Comparison.assertArrayInt(testName + " clustering",
					  expected.clustering, result.clustering);
		Comparison.assertArrayInt(testName + " bestClustering",
					  expected.bestClustering, result.bestClustering);

		// compare intermediate calculations only if we use svd() from
		// R, otherwise they differ because initial values of principal
		// curves et all are quite different
		if (!PrincipalCurve.USE_APACHE_COMMONS_MATH_SVD) {
			Comparison.assertArrayInt(testName + " clusteringLog",
						  expected.clusteringLog, result.clusteringLog);

			Comparison.assertMatrixDouble(testName + " likelihoodsAbout",
						      expected.likelihoodsAbout, result.likelihoodsAbout, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " likelihoodsAlong",
						      expected.likelihoodsAlong, result.likelihoodsAlong, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " likelihoods",
						      expected.likelihoods, result.likelihoods, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " likelihoodsMix",
						      expected.likelihoodsMix, result.likelihoodsMix, ERROR_ADMITTED);
			Comparison.assertArrayDouble(testName + " overallLogLikelihood",
						     expected.overallLogLikelihood, result.overallLogLikelihood, ERROR_ADMITTED);

			Comparison.assertMatrixDouble(testName + " allCurveVars",
						      expected.allCurveVars, result.allCurveVars, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allCurveLengths",
						      expected.allCurveLengths, result.allCurveLengths, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allMeanAlong",
						      expected.allMeanAlong, result.allMeanAlong, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allVarAlong",
						      expected.allVarAlong, result.allVarAlong, ERROR_ADMITTED);
			Comparison.assertMatrixDouble(testName + " allMixProp",
						      expected.allMixProp, result.allMixProp, ERROR_ADMITTED);
		}

		// compare results 2
		Comparison.assertDouble(testName + " score",
					expected2.score, result2.score, ERROR_ADMITTED);
		Comparison.assertDouble(testName + " bic",
					expected2.bic, result2.bic, ERROR_ADMITTED);
		Comparison.assertDouble(testName + " bic2",
					expected2.bic2, result2.bic2, ERROR_ADMITTED);
	}
}
