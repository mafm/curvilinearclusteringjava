/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;
import java.util.*;
import java.io.*;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class PrincipalCurveTest2 extends TestCase {

	/** Base path for filenames */
	private static final String basePath = "data/test/principalcurve/";


	/** Test PrincipalCurve.principalCurve() */
	public void test_principalCurve_data2d() {
		helper_test_principalCurve("data2d");
	}

	/** Helper */
	private void helper_test_principalCurve(String dataName) {
		Timer timer = new Timer();

		final String baseName = basePath + dataName + "-";
		final String testName = "test_principalCurve_" + dataName + "()";

		// basic data
		double[][] x = UtilIO.readMatrixDouble(baseName + "in.x");
		int degreesOfFreedom = UtilIO.readInt(baseName + "in.df");

		// expected result
		PrincipalCurve.Result expected = new PrincipalCurve.Result();
		expected.s = UtilIO.readMatrixDouble(baseName + "out.s");
		expected.lambda = UtilIO.readArrayDouble(baseName + "out.lambda");
		expected.tag = UtilIO.readArrayInt(baseName + "out.tag");
		expected.dist = UtilIO.readDouble(baseName + "out.dist");

		// result
		PrincipalCurve.Result result = PrincipalCurve.principalCurve(x,
									     degreesOfFreedom,
									     PrincipalCurve.DEFAULT_MAX_ITERATIONS);

		// compare result
		MainTestSuite.assertMatrixDouble(testName + " result.s",
						 expected.s, result.s, 0.0001);
		MainTestSuite.assertArrayDouble(testName + " result.lambda",
						expected.lambda, result.lambda, 0.0001);
		MainTestSuite.assertArrayInt(testName + " result.tag",
					     expected.tag, result.tag);
		assertEquals(testName + ", result.dist",
			     expected.dist, result.dist, 0.0001);


		int[] list = { 0, 1, 2, 3, 4, 5, 373, 374, 706, 707, 717, 718 };
		for (int i = 0; i < list.length; ++i) {
			System.err.println("expected.lambda sorted at " + list[i] + ": " + UtilDebug.getSorted(expected.lambda, expected.tag)[list[i]]);
			System.err.println("result.lambda   sorted at " + list[i] + ": " + UtilDebug.getSorted(result.lambda, result.tag)[list[i]]);
		}

		Logger.DEBUG("PrincipalCurve " + testName, "took " + timer.elapsed());
	}
}
