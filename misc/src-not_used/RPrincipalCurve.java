/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.io.*;
import java.text.DecimalFormat;


/** 
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class RPrincipalCurve {

	public static PrincipalCurve.Result getLambdaWrapper(double[][] x,
							     double[][] s,
							     int[] tag,
							     double stretch) {

		long start = System.currentTimeMillis();
		//Logger.DEBUG("R get.lam", "");

		PrincipalCurve.Result result = new PrincipalCurve.Result();

		try {
			File tmp = new File("toR-get.lam");

			PrintWriter writer = new PrintWriter(tmp + ".in.x");
			for (int i = 0; i < x.length; ++i) {
				for (int j = 0; j < x[i].length; ++j) {
					if (j > 0) {
						writer.print(" ");
					}
					writer.print(new DecimalFormat("#.######").format(x[i][j]));
				}
				writer.println();
			}
			writer.flush();
			writer.close();

			writer = new PrintWriter(tmp + ".in.s");
			for (int i = 0; i < s.length; ++i) {
				for (int j = 0; j < s[i].length; ++j) {
					if (j > 0) {
						writer.print(" ");
					}
					writer.print(new DecimalFormat("#.######").format(s[i][j]));
				}
				writer.println();
			}
			writer.flush();
			writer.close();

			if (tag == null) {
				tag = new int[s.length];
				for (int i = 0; i < tag.length; ++i) {
					tag[i] = i+1;
				}
			} else {
				// R indices start in 1...
				for (int i = 0; i < tag.length; ++i) {
					tag[i] += 1;
				}
			}
			writer = new PrintWriter(tmp + ".in.tag");
			for (int i = 0; i < tag.length; ++i) {
				writer.println(tag[i]);
			}
			writer.flush();
			writer.close();

			writer = new PrintWriter(tmp);
			writer.println("library(princurve)");
			//writer.println("debug(get.lam)");
			writer.println("x<-as.matrix(read.table(\"" + tmp.getAbsolutePath() + ".in.x\"))");
			writer.println("s<-as.matrix(read.table(\"" + tmp.getAbsolutePath() + ".in.s\"))");
			writer.println("tag<-as.matrix(read.table(\"" + tmp.getAbsolutePath() + ".in.tag\"))");
			writer.println("result<-get.lam(x, s, tag, stretch=" + stretch + ");\n"
				       + "write.table(result$s,file=\"" + tmp.getAbsolutePath() +".out.s\",row.names=F,col.names=F);\n"
				       + "write.table(result$tag,file=\"" + tmp.getAbsolutePath() +".out.tag\",row.names=F,col.names=F);\n"
				       + "write.table(result$lambda,file=\"" + tmp.getAbsolutePath() +".out.lambda\",row.names=F,col.names=F);\n"
				       + "write.table(result$dist,file=\"" + tmp.getAbsolutePath() +".out.dist\",row.names=F,col.names=F);\n");
			writer.println("print(result$dist)");

			writer.flush();
			writer.close();

			//Logger.DEBUG("R", "file: " + tmp.getAbsolutePath());

			// retrieve result
			String command = "(/usr/bin/R --vanilla --silent < "
				+ tmp.getAbsolutePath()
				+ ") > " + tmp.getAbsolutePath() + ".output";
			writer = new PrintWriter(tmp + ".command");
			writer.println(command);
			writer.flush();
			writer.close();

			Process p = java.lang.Runtime.getRuntime().exec("/bin/bash -- " + tmp + ".command");
			p.waitFor();
/*
			Logger.DEBUG("R", "stderr: ");
			for (int b = 0; b != -1; b = p.getErrorStream().read()) {
				System.err.print((char)b);
			}
			System.err.println();

			Logger.DEBUG("R", "stdout: ");
			for (int b = 0; b != -1; b = p.getInputStream().read()) {
				System.err.print((char)b);
			}
			System.err.println();
*/

			//Logger.DEBUG("R", "smee 1");


			// s
			{
				result.s = new double[x.length][x[0].length];

				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".out.s"));
				String line = data.readLine();
				int row = 0;
				while (line != null) {
					int column = 0;
					String[] tokens = line.split(" ");
					//Logger.DEBUG("R", "result tokens: " + Arrays.toString(tokens));
					for (String t: tokens) {
						if (t == null || t.isEmpty()) {
							continue;
						}
						result.s[row][column++] = Double.valueOf(t);
						//Logger.DEBUG("R", "result token: " + result[row-1]);
					}
					line = data.readLine();
					++row;
				}
				//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			}

			//Logger.DEBUG("R", "smee s");

			// tag
			{
				result.tag = new int[x.length];

				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".out.tag"));
				String line = data.readLine();
				int row = 0;
				while (line != null) {
					String[] tokens = line.split(" ");
					//Logger.DEBUG("R", "result tokens: " + Arrays.toString(tokens));
					for (String t: tokens) {
						if (t == null || t.isEmpty()) {
							continue;
						}
						
						result.tag[row++] = Integer.valueOf(t) - 1; // indices in R start in 1, not in zero

						//Logger.DEBUG("R", "result token: " + result[row-1]);
					}
					line = data.readLine();
				}
				//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			}

			//Logger.DEBUG("R", "smee tag");

			// lambda
			{
				result.lambda = new double[x.length];

				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".out.lambda"));
				String line = data.readLine();
				int row = 0;
				while (line != null) {
					String[] tokens = line.split(" ");
					//Logger.DEBUG("R", "result tokens: " + Arrays.toString(tokens));
					for (String t: tokens) {
						if (t == null || t.isEmpty()) {
							continue;
						}
						result.lambda[row++] = Double.valueOf(t);
						//Logger.DEBUG("R", "result token: " + result[row-1]);
					}
					line = data.readLine();
				}
				//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			}

			//Logger.DEBUG("R", "smee lambda");

			// dist
			{
				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".out.dist"));
				result.dist = Double.valueOf(data.readLine());
			}

//			Logger.DEBUG("GetLambda::getLambda()", "length of s: " + result.s.length);
//			Logger.DEBUG("GetLambda::getLambda()", "length of tag: " + result.tag.length);
//			Logger.DEBUG("GetLambda::getLambda()", "length of lambda: " + result.lambda.length);

		} catch (Exception e) {
			throw new RuntimeException("R PrincipalCurve() get.lam hack: "  + e);
		}

		Logger.INFO("R getLambda()", " - done in " + (System.currentTimeMillis() - start) + "ms");
		return result;
	}






	public static PrincipalCurve.Result principalCurve(double[][] x,
							   int degreesOfFreedom,
							   int maxIterations) {
		Logger.DEBUG("R", "R PrincipalCurve.principalCurve()");
		//Logger.DEBUG("R", "x: " + Arrays.toString(x));

		PrincipalCurve.Result result = new PrincipalCurve.Result();

		try {
/*
			writer.print("x<-rbind(");
			for (int i = 0; i < x.length; ++i) {
				writer.print("c(");
				for (int j = 0; j < x[i].length; ++j) {
					writer.print(new DecimalFormat("#.######").format(x[i][j]));
					if (j != (x[i].length-1)) {
						writer.print(", ");
					} else {
						writer.print(")");
					}
				}
				if (i != (x.length-1)) {
					writer.print(", ");
				} else {
					writer.print(")");
				}
			}
			writer.println(";\n print(x); library(princurve); ");
*/

			//File tmp = File.createTempFile("toR-princurve-", null);
			File tmp = new File("toR-princurve");

			PrintWriter writer = new PrintWriter(tmp + ".x");
			for (int i = 0; i < x.length; ++i) {
				for (int j = 0; j < x[i].length; ++j) {
					if (j > 0) {
						writer.print(" ");
					}
					writer.print(new DecimalFormat("#.######").format(x[i][j]));
				}
				writer.println();
			}
			writer.flush();
			writer.close();

			writer = new PrintWriter(tmp);
			writer.println("library(princurve)");
			writer.println("x<-as.matrix(read.table(\"" + tmp.getAbsolutePath() + ".x\"))");
			writer.println("result<-principal.curve(x, smoother=\"smooth.spline\", df="
				       + degreesOfFreedom + ", maxit=" + maxIterations + ", trace=TRUE);\n"
				       + "print(result)\n"
				       + "write.table(result$s,file=\"" + tmp.getAbsolutePath() +".s\",row.names=F,col.names=F);\n"
				       + "write.table(result$tag,file=\"" + tmp.getAbsolutePath() +".tag\",row.names=F,col.names=F);\n"
				       + "write.table(result$lambda,file=\"" + tmp.getAbsolutePath() +".lambda\",row.names=F,col.names=F);\n"
				       + "write.table(result$dist,file=\"" + tmp.getAbsolutePath() +".dist\",row.names=F,col.names=F);\n");

			writer.flush();
			writer.close();

			//Logger.DEBUG("R", "file: " + tmp.getAbsolutePath());

			// retrieve result
			String command = "(/usr/bin/R --vanilla --silent < "
				+ tmp.getAbsolutePath()
				+ ") > " + tmp.getAbsolutePath() + ".output";
			writer = new PrintWriter(tmp + ".command");
			writer.println(command);
			writer.flush();
			writer.close();

			Process p = java.lang.Runtime.getRuntime().exec("/bin/bash -- " + tmp + ".command");
			p.waitFor();
/*
			Logger.DEBUG("R", "stderr: ");
			for (int b = 0; b != -1; b = p.getErrorStream().read()) {
				System.err.print((char)b);
			}
			System.err.println();

			Logger.DEBUG("R", "stdout: ");
			for (int b = 0; b != -1; b = p.getInputStream().read()) {
				System.err.print((char)b);
			}
			System.err.println();
*/

			//Logger.DEBUG("R", "smee 1");


			// s
			{
				result.s = new double[x.length][x[0].length];

				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".s"));
				String line = data.readLine();
				int row = 0;
				while (line != null) {
					int column = 0;
					String[] tokens = line.split(" ");
					//Logger.DEBUG("R", "result tokens: " + Arrays.toString(tokens));
					for (String t: tokens) {
						if (t == null || t.isEmpty()) {
							continue;
						}
						result.s[row][column++] = Double.valueOf(t);
						//Logger.DEBUG("R", "result token: " + result[row-1]);
					}
					line = data.readLine();
					++row;
				}
				//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			}

			//Logger.DEBUG("R", "smee s");

			// tag
			{
				result.tag = new int[x.length];

				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".tag"));
				String line = data.readLine();
				int row = 0;
				while (line != null) {
					String[] tokens = line.split(" ");
					//Logger.DEBUG("R", "result tokens: " + Arrays.toString(tokens));
					for (String t: tokens) {
						if (t == null || t.isEmpty()) {
							continue;
						}
						
						result.tag[row++] = Integer.valueOf(t) - 1; // indices in R start in 1, not in zero

						//Logger.DEBUG("R", "result token: " + result[row-1]);
					}
					line = data.readLine();
				}
				//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			}

			//Logger.DEBUG("R", "smee tag");

			// lambda
			{
				result.lambda = new double[x.length];

				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".lambda"));
				String line = data.readLine();
				int row = 0;
				while (line != null) {
					String[] tokens = line.split(" ");
					//Logger.DEBUG("R", "result tokens: " + Arrays.toString(tokens));
					for (String t: tokens) {
						if (t == null || t.isEmpty()) {
							continue;
						}
						result.lambda[row++] = Double.valueOf(t);
						//Logger.DEBUG("R", "result token: " + result[row-1]);
					}
					line = data.readLine();
				}
				//Logger.DEBUG("R", "result: " + Arrays.toString(result));
			}

			//Logger.DEBUG("R", "smee lambda");

			// dist
			{
				BufferedReader data = new BufferedReader(new FileReader(tmp.getAbsolutePath() + ".dist"));
				result.dist = Double.valueOf(data.readLine());
			}

			//Logger.DEBUG("GetLambda::getLambda()", "length of s: " + result.s.length);
			//Logger.DEBUG("GetLambda::getLambda()", "length of tag: " + result.tag.length);
			//Logger.DEBUG("GetLambda::getLambda()", "length of lambda: " + result.lambda.length);

		} catch (Exception e) {
			throw new RuntimeException("R PrincipalCurve() hack: "  + e);
		}

		return result;
	}
}

