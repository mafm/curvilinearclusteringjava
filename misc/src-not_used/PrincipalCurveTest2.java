/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;
import java.util.*;
import java.io.*;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class PrincipalCurveTest2 extends TestCase {

	/** Base path for filenames */
	private static final String basePath = "data/test/principalcurve/";


	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test */
	private static boolean getLambdaWrapperTest(String subdirTest,
						    String subdirResults,
						    int rows,
						    int cols) {
		String subdirData = "getlam-test/" + subdirTest;
		subdirResults = subdirData + "/" + subdirResults;

		// x
		double[][] x = new double[rows][cols];
		try {
			BufferedReader data = new BufferedReader(new FileReader(subdirData + "/x"));
			for (int row = 0; row < rows; ++row) {
				String[] tokens = data.readLine().split(" ");
				for (int col = 0; col < cols; ++col) {
					x[row][col] = Double.valueOf(tokens[col]);
				}
			}
		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error reading data for 'x': "  + e.getMessage());
			return false;
		}

		// s
		double[][] s = new double[rows][cols];
		try {
			BufferedReader data = new BufferedReader(new FileReader(subdirData + "/s"));
			for (int row = 0; row < rows; ++row) {
				String[] tokens = data.readLine().split(" ");
				for (int col = 0; col < cols; ++col) {
					s[row][col] = Double.valueOf(tokens[col]);
				}
			}
		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error reading data for 's': "  + e.getMessage());
			return false;
		}

		// pcurve.s
		double[][] pcurve_s = new double[rows][cols];
		try {
			BufferedReader data = new BufferedReader(new FileReader(subdirResults + "/pcurve.s"));
			for (int row = 0; row < rows; ++row) {
				String[] tokens = data.readLine().split(" ");
				for (int col = 0; col < cols; ++col) {
					pcurve_s[row][col] = Double.valueOf(tokens[col]);
				}
			}
		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error reading data for 'pcurve.s': "  + e.getMessage());
			return false;
		}

		// pcurve.lambda
		double[] pcurve_lambda = new double[rows];
		try {
			BufferedReader data = new BufferedReader(new FileReader(subdirResults + "/pcurve.lambda"));
			for (int row = 0; row < pcurve_lambda.length; ++row) {
				String[] tokens = data.readLine().split(" ");
				pcurve_lambda[row] = Double.valueOf(tokens[0]);
			}
		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error reading data for 'pcurve.lambda': "  + e.getMessage());
			return false;
		}

		// pcurve.tag
		int[] pcurve_tag = new int[rows];
		try {
			BufferedReader data = new BufferedReader(new FileReader(subdirResults + "/pcurve.tag"));
			for (int row = 0; row < pcurve_tag.length; ++row) {
				String[] tokens = data.readLine().split(" ");
				pcurve_tag[row] = Integer.valueOf(tokens[0]);
			}
		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error reading data for 'pcurve.tag': "  + e.getMessage());
			return false;
		}

		// pcurve.dist
		double pcurve_dist = Double.NaN;
		try {
			BufferedReader data = new BufferedReader(new FileReader(subdirResults + "/pcurve.dist"));
			String[] tokens = data.readLine().split(" ");
			pcurve_dist = Double.valueOf(tokens[0]);
		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error reading data for 'pcurve.dist': "  + e.getMessage());
			return false;
		}

		// do call the computation function
		int[] tag = new int[rows];
		for (int row = 0; row < tag.length; ++row) {
			tag[row] = row;
		}
		PrincipalCurve.Result result = PrincipalCurve.getLambdaWrapper(x, s, tag, 2.0);

		// write results
		try {
			PrintWriter writer = null;

			// s
			writer = new PrintWriter(subdirData + "/mafm-java/pcurve.s");
			for (int i = 0; i < result.s.length; ++i) {
				for (int j = 0; j < result.s[i].length; ++j) {
					if (i > 0)
						writer.print(" ");
					writer.print(result.s[i][j]);
				}
				writer.println();
			}
			writer.flush();
			writer.close();

			// lambda
			writer = new PrintWriter(subdirData + "/mafm-java/pcurve.lambda");
			for (int i = 0; i < result.lambda.length; ++i) {
				writer.println(result.lambda[i]);
			}
			writer.flush();
			writer.close();


			// tag
			writer = new PrintWriter(subdirData + "/mafm-java/pcurve.tag");
			for (int i = 0; i < result.tag.length; ++i) {
				// mafm: +1 because of indices of arrays in both
				// languages, first in R is element 1, not 0
				writer.println(result.tag[i] + 1);
			}
			writer.flush();
			writer.close();


			// dist
			writer = new PrintWriter(subdirData + "/mafm-java/pcurve.dist");
			writer.println(result.dist);
			writer.flush();
			writer.close();

		} catch (Exception e) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     "Error writing data: "  + e.getMessage());
			return false;
		}

		Logger.INFO("PrincipalCurveTest::getLambdaWrapperTest()", "Comparing results...");
		if (!compareS(result.s, pcurve_s)) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - 's' test failed");
			return false;
		} else if (!compareDoubleArray(result.lambda, pcurve_lambda,
					       "result.lambda", "pcurve_lambda")) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - 'lambda' test failed");
			return false;
		} else if (!compareTag(result.tag, pcurve_tag)) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - 'tag' test failed");
			return false;
		} else if (null != allowedDifference(result.dist, pcurve_dist)) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - 'dist' test failed: "
				     + result.dist + " vs. " + pcurve_dist);
			return false;
		} else {
			Logger.INFO("PrincipalCurveTest::getLambdaWrapperTest()",
				    "Test OK");
			return true;
		}
	}

	/** Helper method to compare results */
	private static boolean compareS(double[][] s1, double[][] s2) {
		if (s1.length != s2.length || s1[0].length != s2[0].length) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - 's' dimensions mismatch");
			return false;
		} else {
			boolean successful = true;
			for (int row = 0; row < s1.length; ++row) {
				for (int col = 0; col < s1[0].length; ++col) {
					String s = allowedDifference(s1[row][col], s2[row][col]);
					if (s != null) {
						Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
							     " - 's' mismatch at [" + row + "][" + col + "]: " + s);
						successful = false;
					}
				}
			}
			return successful;
		}
	}

	/** Helper method to compare results */
	private static boolean compareDoubleArray(double[] array1, double[] array2,
						  String array1name, String array2name) {
		if (array1.length != array2.length) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - array dimensions mismatch: " + array1name + ", " + array2name);
			return false;
		} else {
			boolean successful = true;
			for (int row = 0; row < array1.length; ++row) {
				String s = allowedDifference(array1[row], array2[row]);
				if (s != null) {
					Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
						     " - arrays " + array1name + " and " + array2name
						     + " mismatch at element [" + row + "]: " + s);
					successful = false;
				}
			}
			return successful;
		}
	}

	/** Helper method to compare results */
	private static boolean compareTag(int[] tag1, int[] tag2) {
		if (tag1.length != tag2.length) {
			Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
				     " - 'tag' dimensions mismatch");
			return false;
		} else {
			boolean successful = true;
			for (int row = 0; row < tag1.length; ++row) {
				// mafm: +1 because of indices of arrays in both
				// languages, first in R is element 1, not 0
				if (tag1[row] != (tag2[row] - 1)) {
					Logger.ERROR("PrincipalCurveTest::getLambdaWrapperTest()",
						     " - 'tag' mismatch at [" + row + "]: "
						     + tag1[row] + " vs. " + tag2[row]);
					successful = false;
				}
			}
			return successful;
		}
	}

	/** Helper method to compare results */
	private static String allowedDifference(double var1, double var2) {
		if (Math.abs(var1-var2) > 0.002) {
			return "" + var1 + " vs. " + var2;
		} else {
			return null;
		}
	}

	/** Test PrincipalCurve.getLambdaWrapper() */
	public static void test_getLambdaWrapper() {
		//boolean successful = getLambdaWrapperTest("20090119-lmsb", "mafm-r", 1000, 2);
		//boolean successful = getLambdaWrapperTest("20090125-mafm", "mafm-r", 10, 2);
		boolean successful = getLambdaWrapperTest("20090226-lmsb", "mafm-r", 2002, 2);
		assertTrue("PrincipalCurve.getLambdaWrapper()", successful);
	}
}