/*
 * Curvilinear Clustering program
 *
 * Copyright (C) 2007-2010 Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import junit.framework.TestCase;


/** Test
 *
 * @author Manuel A. Fernandez Montecelo <manuel.montezelo@gmail.com>
 */
public class PointTest extends TestCase {

	/** Points serving as base for tests. */
	private final Point p1 = new Point(new double[] { 1,  2,  3});
	/** Points serving as base for tests. */
	private final Point p2 = new Point(new double[] { 3,  2,  1});
	/** Points serving as base for tests. */
	private final Point p3 = new Point(new double[] {-1, -2, -3});
	/** Points serving as base for tests. */
	private final Point p4 = new Point(new double[] {-3, -2, -1});


	/** Sets up the test fixture. Called before every test case method. */
	protected void setUp() {
		// nothing
	}

	/** Tears down the test fixture. Called after every test case method. */
	protected void tearDown() {
		// release objects under test here, if necessary
	}

	/** Test Point.difference() */
	public void test_difference() {
		assertEquals("p1.difference(p1)", new Point(new double[] { 0,  0,  0}), p1.difference(p1));
		assertEquals("p1.difference(p2)", new Point(new double[] {-2,  0,  2}), p1.difference(p2));
		assertEquals("p1.difference(p3)", new Point(new double[] { 2,  4,  6}), p1.difference(p3));
		assertEquals("p1.difference(p4)", new Point(new double[] { 4,  4,  4}), p1.difference(p4));

		assertEquals("p2.difference(p1)", new Point(new double[] { 2,  0, -2}), p2.difference(p1));
		assertEquals("p2.difference(p2)", new Point(new double[] { 0,  0,  0}), p2.difference(p2));
		assertEquals("p2.difference(p3)", new Point(new double[] { 4,  4,  4}), p2.difference(p3));
		assertEquals("p2.difference(p4)", new Point(new double[] { 6,  4,  2}), p2.difference(p4));

		assertEquals("p3.difference(p1)", new Point(new double[] {-2, -4, -6}), p3.difference(p1));
		assertEquals("p3.difference(p2)", new Point(new double[] {-4, -4, -4}), p3.difference(p2));
		assertEquals("p3.difference(p3)", new Point(new double[] { 0,  0,  0}), p3.difference(p3));
		assertEquals("p3.difference(p4)", new Point(new double[] { 2,  0, -2}), p3.difference(p4));

		assertEquals("p4.difference(p1)", new Point(new double[] {-4, -4, -4}), p4.difference(p1));
		assertEquals("p4.difference(p2)", new Point(new double[] {-6, -4, -2}), p4.difference(p2));
		assertEquals("p4.difference(p3)", new Point(new double[] {-2,  0,  2}), p4.difference(p3));
		assertEquals("p4.difference(p4)", new Point(new double[] { 0,  0,  0}), p4.difference(p4));
	}
}
