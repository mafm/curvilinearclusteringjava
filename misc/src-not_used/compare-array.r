compare=function(basefile, correcttag=0, ...)
{
# correcttag: for tag, arrays in R start at 1
aRfile=paste("/tmp/", basefile, "-R", sep="")
aJfile=paste("/tmp/", basefile, "-Java", sep="")

aR=as.matrix(read.table(aRfile))
aJ=as.matrix(read.table(aJfile))

# for tag, arrays in R start at 1
aJ=aJ+correcttag

cat(aRfile, "    is a matrix of: ", length(aR[,1]), "x", length(aR[1, ]), "\n")
cat(aJfile, " is a matrix of: ", length(aJ[,1]), "x", length(aJ[1, ]), "\n")
if ((length(aR) != length(aJ)) || (length(aR[1,]) != length(aJ[1, ]))) { cat("Arrays of different length\n") }

error.found=FALSE
for (i in 1:length(aR[,1])) {
    for (j in 1:length(aR[1, ])) {
        if (is.na(aR[i,j])) {
	   if (aJ[i,j] != "NaN") {
	      cat("Arrays different at element (", i, ",",j, "): aR=", aR[i,j], " != aJ=", aJ[i,j], "\n")
              error.found=TRUE
	   }
	} else if (abs(aR[i,j]-aJ[i,j]) > error.limit) {
           cat("Arrays different at element (", i, ",",j, "): aR=", aR[i,j], " != aJ=", aJ[i,j], "\n")
           error.found=TRUE
        }
    }
}

cat("Finished, error found?", error.found, "\n\n")
}
#---------------------------------------

error.limit=0.0000000001
#compare("1-princurve.in.x")
#compare("1-princurve.out.s")
#compare("1-princurve.out.lambda")
#compare("1-princurve.out.tag", correcttag=1)
#compare("1-princurve.out.dist")

#compare("4-sqdists")

#compare("5-distincurve")

#compare("5-lambdawrapper.in.xlambda")
#compare("5-lambdawrapper.in.curve_s")
#compare("5-lambdawrapper.in.curve_tag", correcttag=1)
#compare("5-lambdawrapper.out.s")
#compare("5-lambdawrapper.out.lambda")
#compare("5-lambdawrapper.out.tag", correcttag=1)
#compare("5-lambdawrapper.out.dist")

compare("7-distalong")

compare("88-likelihoodsabout")
compare("88-likelihoodsalong")
compare("88-likelihoods")
compare("88-likelihoodsmix")

compare("89-allcurvevars")
compare("89-allcurvelengths")
compare("89-allmeanalong")
compare("89-allvaralong")
compare("89-allmixprop")

compare("100-clusteringlog")
compare("100-overallloglikelihood")

compare("90-clustering_it0")
compare("90-clustering_it1")
compare("90-clustering_it2")
compare("90-clustering_it3")
compare("90-clustering_it4")
