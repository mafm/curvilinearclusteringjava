# height 1 includes TrD, mode pcurve_java_only times
height1=cbind(
c(3.132, 3.477, 6.245, 6.617, 0.842),
c(8.208, 9.092, 16.02, 17.256, 1.924),
c(2.569, 2.654, 4.044, 14.987, 126.088),
c(2.547, 2.708, 4, 14.856, 12.74),
c(4.008, 4.025, 6.405, 11.484, 34.072),
c(4.105, 4.011, 6.374, 11.517, 6.858),
c(21.321, 18.979, 30.109, 57.114, 38.171),
c(19.476, 19.148, 30.106, 57.395, 38.356),
c(25.667, 26.89, 42.954, 69.991, 113.933),
c(26.306, 26.865, 43.167, 69.444, 77.404),
c(117.339, 117.948, 189.676, 331.956, 450.395)
)


legnames=c("Or", "TrA", "TrB", "TrC")
datasets=c("curves2d (no force)", "curves2d (force 6)",
	   "data3d (no force)", "data3d (force 7)",
	   "data5d (no force)", "data5d (force 7)",
	   "arcs3d (no force)", "arcs3d (force 3)",
	   "arcs3d_noise (no force)", "arcs3d_noise (force 4)")
datasets=c("A", "B", "C", "D", "E", "F", "G", "H", "I", "J")
height2=cbind(
c(3.132, 3.477, 6.245, 6.617),
c(8.208, 9.092, 16.02, 17.256),
c(2.569, 2.654, 4.044, 14.987),
c(2.547, 2.708, 4, 14.856),
c(4.008, 4.025, 6.405, 11.484),
c(4.105, 4.011, 6.374, 11.517),
c(21.321, 18.979, 30.109, 57.114),
c(19.476, 19.148, 30.106, 57.395),
c(25.667, 26.89, 42.954, 69.991),
c(26.306, 26.865, 43.167, 69.444),
c(117.339, 117.948, 189.676, 331.956)
)
colh2=c("darkblue", "blue", "dodgerblue2", "steelblue1")
curves2d.rows=c(1:2)
data3d.rows=c(3:4)
data5d.rows=c(5:6)
arcs3d.rows=c(7:8)
arcs3d_noise.rows=c(9:10)
datasets.rows=c(1:10)
totals.rows=c(11)
limit=c(0, 75)

barplot.wrapper=function(data, rows, colors, data.name, sets.names, legnames, limit) {
	svg(paste("hpcc-", data.name, ".svg", sep=""))
	barplot(data[,rows], beside=TRUE, main=paste("HPCC performance with ", data.name, sep=""), font=8,
		col=colors, names.arg=sets.names[rows], ylim=limit, ylab="seconds", width=5)
	legend("top", fill=colors, legend=legnames, horiz=TRUE)
	dev.off()
}

#barplot.wrapper(data=height2[,c(1:10)], rows=curves2d.rows, data.name="curves2d", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)
#barplot.wrapper(data=height2[,c(1:10)], rows=data3d.rows, data.name="data3d", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)
#barplot.wrapper(data=height2[,c(1:10)], rows=data5d.rows, data.name="data5d", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)
#barplot.wrapper(data=height2[,c(1:10)], rows=arcs3d.rows, data.name="arcs3d", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)
#barplot.wrapper(data=height2[,c(1:10)], rows=arcs3d_noise.rows, data.name="arcs3d_noise", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)

barplot.wrapper(data=height2, rows=datasets.rows, data.name="datasets", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)

barplot.wrapper(data=height2, rows=totals.rows, data.name="totals", col=colh2, sets.names=rep("totals", 11), legnames=legnames, limit=c(0, max(height2[,totals.rows])*1.3))

