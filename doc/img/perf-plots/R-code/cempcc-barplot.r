legnames=c("Or", "TrA", "TrB", "TrC", "TrD")
datasets=c("A", "B", "C", "D", "E", "F")
height=cbind(
c(4.941, 173.183, 2.943, 2.866, 3.019),
c(6.657, 172.467, 4.690, 4.621, 3.505),
c(62.188, 985.652, 56.780, 57.938, 122.375),
c(241.807, 2752.429, 253.538, 259.409, 146.462),
c(16.216, 121.268, 16.811, 17.440, 19.783),
c(29.211, 263.087, 31.892, 32.954, 56.262),
c(361.020, 4468.086, 366.654, 375.228, 351.407)
)
colh2=c("darkblue", "blue", "dodgerblue2", "steelblue1", "lightskyblue1")
datasets.rows=c(1:6)
datasets_no_rwrapper.cols=c(1, 3:5)
totals.rows=c(7)
limit=c(0, 3200)
limit_no_rwrapper=c(0, 300)

barplot.wrapper=function(data, rows, colors, data.name, sets.names, legnames, limit) {
	svg(paste("cempcc-", data.name, ".svg", sep=""))
	barplot(data[,rows], beside=TRUE, main=paste("CEMPCC performance with ", data.name, sep=""), font=8,
		col=colors, names.arg=sets.names[rows], ylim=limit, ylab="seconds", width=5)
	legend("top", fill=colors, legend=legnames, horiz=TRUE)
	dev.off()
}

barplot.wrapper(data=height, rows=datasets.rows, data.name="datasets", col=colh2, sets.names=datasets, legnames=legnames, limit=limit)
barplot.wrapper(data=height[datasets_no_rwrapper.cols,], rows=datasets.rows, data.name="datasets_no_rwrapper", col=colh2[datasets_no_rwrapper.cols], sets.names=datasets, legnames=legnames[datasets_no_rwrapper.cols], limit=limit_no_rwrapper)
print(height[datasets_no_rwrapper.cols,])

barplot.wrapper(data=height, rows=totals.rows, data.name="totals", col=colh2, sets.names=rep("totals", 7), legnames=legnames, limit=c(0, max(height[,totals.rows])*1.2))
barplot.wrapper(data=height[datasets_no_rwrapper.cols,], rows=totals.rows, data.name="totals_no_rwrapper", col=colh2[datasets_no_rwrapper.cols], sets.names=rep("totals", 7), legnames=legnames[datasets_no_rwrapper.cols], limit=c(0, max(height[datasets_no_rwrapper.cols,totals.rows])*1.2))

