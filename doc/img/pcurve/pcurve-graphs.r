=============== projection ===============
library(princurve)

x=as.matrix(read.table("../data/test/curves2d"))
x=x[c(1:100),]
res=principal.curve(x, df=4)

plot(res, col="blue")
points(res$s, col="steelblue3",pch='°')
points(x, col="darkred",pch='°')
whiskers(x, res$s)

svg("pcurve-projection.svg")
plot(res, col="blue", xlab="x", ylab="y")
points(res$s, col="steelblue3",pch='°')
points(x, col="darkred",pch='°')
whiskers(x, res$s)
dev.off()



=============== deg. of freedom ===============

arcs3d=as.matrix(read.table("arcs3d"))

arcs3d=cbind(arcs3d[,1], arcs3d[,3])
arcs3d=arcs3d[c(1:334),]

pdf("/tmp/lala-2df.pdf"); res=principal.curve(arcs3d, plot.true=F, df=2); plot(arcs3d, col="darkred", xlab="", ylab="");lines(res, col="blue"); dev.off()

svg("/tmp/pcurve-df10.svg", width=20, height=10); res=principal.curve(arcs3d, df=10); plot(arcs3d, col="darkred", xlab="", ylab="", pch='°'); lines(res, col="blue"); dev.off()


mydf=10; svg(paste("/tmp/pcurve-df", mydf, ".svg", sep=""), width=10, height=5); res=principal.curve(arcs3d, df=mydf); plot(arcs3d, col="darkred", xlab="", ylab=""); lines(res, col="blue"); dev.off()

for f in *.svg; do convert -resize 80% -trim ${f} ${f/.svg/.png}; done


