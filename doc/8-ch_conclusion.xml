<?xml version="1.0" encoding="utf-8"?>
<chapter xmlns="http://docbook.org/ns/docbook" version="5.0" id="ch.Conclusion">

  <title>Conclusion</title>

  <blockquote>
    <attribution>Monty Python, in <emphasis>Monty Python’s Flying Circus</emphasis>, episode <emphasis>Whither Canada?</emphasis></attribution>
    <para><emphasis>Commentator: In 1945 Peace broke out. It was the end of the Joke. Joke warfare was banned at a special session of the Geneva Convention, and in 1950 the last remaining copy of the joke was laid to rest here in the Berkshire countryside, never to be told again.</emphasis></para>
  </blockquote>

  <para>
    This last chapter discusses the achievements of this project, and overviews
    its major contributions and aspects by summarising the contents of the
    previous chapters.
  </para>

  <para>
    Firstly, we start by explaining the unexpected side-effect of re-licensing
    the original software owing to our project; secondly, we discuss whether the
    goals set for this project were fulfilled and draw conclusions; and finally
    we consider open issues and outline possible directions of future research
    based on this project.
  </para>

  <sect1>
    <title>Re-licensing of the Original Software</title>

    <para>
      The original <methodname>Curvilinear Clustering</methodname> software,
      provided with the article <citetitle pubwork="article">Finding Curvilinear
      Features in Spatial Point Patterns: Principal Curve Clustering with
      Noise</citetitle> <citation>Stanford and Raftery, 2000</citation>, did not
      contain any information about the license terms under which the software
      was released.
    </para>

    <para>
      The software uses <methodname>NNClean</methodname> and the mentioned
      <methodname>Principal Curves</methodname>, which are themselves both
      released under the GNU General Public License (see
      <citation>GPL</citation>), version 2 or later.  This is a free/open-source
      software version, and implies that derived and afterwards distributed
      software (as in this case) has to be released under the same GPL license
      and the same version or, optionally, later versions.
    </para>

    <para>
      However, we decided to ask explicitly for permission and wrote to Adrian
      Raftery (one of the authors, since we could not contact the other one)
      explaining the case.  He agreed and gave us his explicit permission, but
      what is more, he decided to re-license the project and release it under
      the same GPL license, version 3 or later.
    </para>

    <para>
      Taking that into consideration, the development of our project had an
      interesting side-effect even for people who do not use our software, which
      is to add to the <foreignphrase>corpus</foreignphrase> of Free Software
      projects one interesting piece of software — the original implementation.
    </para>
  </sect1>


  <sect1>
    <title>Conclusions</title>

    <para>
      During the past chapters, we have already:

      <itemizedlist>
	<listitem>
	  <para>
	    introduced the software and described the goals set;
	  </para>
	</listitem>
	<listitem>
	  <para>
	    reviewed the theoretical basis upon which this project is built;
	  </para>
	</listitem>
	<listitem>
	  <para>
	    explained the methodology followed to develop the software (analysis
	    of the requirements, design, implementation);
	  </para>
	</listitem>
	<listitem>
	  <para>
	    analysed the results of the validation and verification processes,
	    and the performance of the original and translated software; and
	  </para>
	</listitem>
	<listitem>
	  <para>
	    discussed related work.
	  </para>
	</listitem>
      </itemizedlist>

      Now it is time for some final remarks.
    </para>

    <para>
      From our point of view, the software serves the purpose for which it was
      created.  We have proved that the software is useful for classifying
      observations with curvilinear features, even when the initial clustering
      is confusing for the algorithms, e.g., initially classifying observations
      as noise when they are not.  Also, the software works well, it works
      remarkably close to the original software and, beyond that, the results of
      both the original software and the translation are mostly satisfactory
      even when providing data for which the separation in different clusters is
      difficult and not clear.
    </para>

    <para>
      The work developed during this project involved translating the original
      software and we did not focus on performance but accuracy.  In fact, the
      system was translated into <methodname>Java</methodname> for reasons other
      than gaining in performance.  However, the results in the area of
      performance are not very different from the original software.  If a
      certain data set can be analysed with the original software, it can be
      analysed again in the translated system with the maximum guarantees about
      accuracy and performance.
    </para>

    <para>
      To sum up, not only are the results from the validation and verification
      processes satisfactory but the performance is satisfactory too. This
      proves the success of the project, since it met its goals.
    </para>
  </sect1>


  <sect1>
    <title>Enhancements</title>

    <para>
      This section contains an outline of possible directions of future research
      based on this project.
    </para>

    <para>
      This method for cluster analysis has a prominent characteristic, which is
      the fact that it is focused on data with curvilinear features.  Therefore,
      any work based on this project should maintain this characteristic;
      otherwise it would be better to use a different clustering method as a
      basis for that work.
    </para>

    <para>
      The two algorithms of this method complement each other, since one works
      merging clusters (a high level operation, performing large-scale
      modifications of the clustering) while the other considers to move points
      individually from one cluster to another (a fine-grained operation).  As a
      result, the combination of both is more powerful and
      <foreignphrase>omnibus</foreignphrase> or broad-gauged than the majority
      of the clustering methods commonly used.  The method is also remarkably
      flexible, since it allows to specify parameters to control the smoothness
      of the features, whether to favour thicker and gapless clusters or thinner
      clusters even if they have gaps, and the iterations and error tolerances
      admitted.
    </para>

    <para>
      Due to all of these reasons, we believe that any work based on this
      project should look forward to improve the way it works in terms of
      performance (or accuracy of the <varname>pcurve_java_only</varname> mode),
      but not to change its scope and fundamental principles.
    </para>

    <para>
      Nevertheless, there is a possible enhancement changing some fundamental
      principles while maintaining the spirit of the original software, which we
      considered implementing initially given the difficulty of translating
      principal curves.  This option would imply to use curved surfaces (two
      dimensions) instead of curved lines (one dimension), so we would use
      <emphasis>Principal Surfaces</emphasis> as a substitute for
      <emphasis>Principal Curves</emphasis>.
    </para>

    <para>
      Regarding improvements to performance, we suggest to look at the following
      possibilities:

      <itemizedlist>
	<listitem>
	  <para>
	    Since the implementation now includes a framework to check the
	    accuracy of the software (see <xref
	    linkend="sec.Method.Impl.Tests"/>) and the correctness of the
	    implementation was already proved, future developers should not be
	    afraid to improve the code even if it does not resemble the original
	    code anymore.
	  </para>

	  <para>
	    As one example of improvement, we suggest trying to avoid copying
	    large quantities of data from one place to another.  Another example
	    would be that, instead of creating a new matrix of data from a
	    previous set with only a changed row (as it happens in some places),
	    the related functions could be modified so they would always take
	    the same data with an additional row and the place where it should
	    be inserted.  Both these improvements would require to change a
	    moderately large amount of functions and blocks of code, even if the
	    changes are not of major importance from a functional point of view.
	  </para>
	</listitem>

	<listitem>
	  <para>
	    The implementation in <filename>QuicksortWithOrder.java</filename>
	    could also be improved.  Since it is used in every call to
	    <methodname>Principal Curves</methodname> functions, focusing on it
	    could bring huge benefits in terms of performance.
	  </para>

	  <para>
	    Possibly the best thing to do would be to dismiss this
	    implementation altogether and use the
	    <methodname>quicksort()</methodname> algorithm which comes in the
	    standard library of <methodname>Java</methodname> (sorting the array
	    <emphasis>in place</emphasis>).  But in order for this to be
	    achieved, the code has to be modified in numerous places across
	    different files and classes (which was the reason why this was
	    discarded in this project).
	  </para>
	</listitem>

	<listitem>
	  <para>
	    Following the previous idea of replacing
	    <methodname>QuicksortWithOrder</methodname> because it is widely
	    used, it would be also easy to measure (with the
	    <methodname>Timer</methodname> class and simple log messages) where
	    the execution spends more time, and try to improve those parts.
	    <filename>QuicksortWithOrder.java</filename> is an outstanding
	    example, but there are more places where it happens something
	    similar.  We already used this method to improve the performance of
	    <methodname>RWrapper</methodname>, and it was worth the effort.
	  </para>
	</listitem>

	<listitem>
	  <para>
	    Finally and beyond the previous examples, the algorithms can be
	    improved by using different threads, processes or even different
	    machines in computer clusters (e.g. the Grid).  There are several
	    spots in the algorithms which should not be very difficult to
	    parallelise, because they are operating in different portions of the
	    data.  One place where the calculations could be parallelised is the
	    place where <methodname>H-PCC</methodname> calculates the variance
	    of merging each cluster, which does not interfere with possible
	    mergers of other clusters.  <methodname>CEM-PCC</methodname> does
	    something similar concerning the probability for each point to
	    belong to each cluster.
	  </para>
	</listitem>
      </itemizedlist>
    </para>

    <para>
      As a summary, we can say that there are many opportunities to enhance the
      software, specially in the area of performance.  Bringing those
      enhancements together could decrease the time needed to analyse the data
      with this algorithm to a small fraction, and thus the system would be able
      to analyse much bigger data sets (perhaps with ten or hundred thousand
      points per computer node) in a reasonable amount of time, using commonly
      available hardware.
    </para>
  </sect1>

</chapter>
