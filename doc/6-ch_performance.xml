<?xml version="1.0" encoding="utf-8"?>
<chapter xmlns="http://docbook.org/ns/docbook" version="5.0" id="ch.Perf">

  <title>Performance Measurements</title>

  <blockquote>
    <attribution>Donald Knuth, in <emphasis><quote>Structured Programming with Goto Statements</quote></emphasis>, <emphasis>Computing Surveys 6:4</emphasis>, December 1974</attribution>
    <para><emphasis>Premature optimization is the root of all evil.</emphasis></para>
  </blockquote>

  <para>
    This chapter is concerned with the performance measurements of both the
    original and translated software.  It seems obvious that a software system
    is not very useful if it spends an unreasonable amount of time to produce
    results.  Moreover, its performance plays a key role since it determines to
    which sets of problems the system can be applied.
  </para>

  <para>
    In this chapter, a number of tables and graphs is provided in order to
    present the performance of the original and translated software when running
    the different data sets so as to compare both systems.  The computer
    hardware used for all of these tests was a laptop computer property of the
    author, with the following characteristics:

    <itemizedlist>
      <listitem>
	<para>
	  Manufacturer and Product Name: Dell Latitude D530;
	</para>
      </listitem>
      <listitem>
	<para>
	  CPU: Intel Core2 Duo T7250 @ 2.00GHz;
	</para>
      </listitem>
      <listitem>
	<para>
	  RAM: 2GB;
	</para>
      </listitem>
      <listitem>
	<para>
	  Disk: 120 GB;
	</para>
      </listitem>
    </itemizedlist>
  </para>

  <para>
    Each table shows the performance of each algorithm.  The code in the column
    headers means either the original software or the operation mode for the
    <methodname>Principal Curves</methodname> translation (see <xref
    linkend="sec.Valid.OpModes"/>, they are summarised in the next paragraphs
    for convenience).  The content of the cells contains the time elapsed in an
    execution of the algorithm with the respective mode and the given data sets,
    as well as the relative time (in percentage) compared to that of the
    original software.  To determine the elapsed times that are displayed in the
    tables and graphs, the algorithms were executed three times for each data
    set and mode, and then the average time corresponding to the three
    executions was calculated.
  </para>

  <para>
    The codes appearing in the headers are listed below:

    <itemizedlist>
      <listitem>
	<para>
	  Or: original software
	</para>
      </listitem>
      <listitem>
	<para>
	  TrA: <varname>pcurve_rwrapper</varname>
	</para>
      </listitem>
      <listitem>
	<para>
	  TrB: <varname>pcurve_java+r_svd+r_smoothing</varname>
	</para>
      </listitem>
      <listitem>
	<para>
	  TrC: <varname>pcurve_java+apache_svd+r_smoothing</varname>
	</para>
      </listitem>
      <listitem>
	<para>
	  TrD: <varname>pcurve_java_only</varname>
	</para>
      </listitem>
    </itemizedlist>
  </para>

  <para>
    Bear in mind that in the case of TrD the results are inconsistent: sometimes
    much quicker and sometimes much slower than the reference mode.  This is
    because although this mode of the translation is indeed quicker, sometimes
    it performs more mergers (<methodname>H-PCC</methodname>) or iterations
    (<methodname>CEM-PCC</methodname>) than the reference mode and that is the
    reason explaining the slower results.
  </para>

  <para>
    With respect to the graphs, they show the same information as the tables,
    but a visual representation was considered to be easier to understand from a
    global perspective.  Apart from that, the whole amount of information
    contained in the tables had to be divided in several graphs.  This was due
    to the fact that there was too much information per table to be represented
    in only one graph.
  </para>


  <sect1>
    <title>Performance of HPCC</title>

    <para>
      The tables and graphs in this subsection display the performance of the
      original and translated software running the first algorithm,
      <methodname>H-PCC</methodname>.
    </para>

    <para>
      <xref linkend="tab.PerformanceHPCC"/>, shows the time that each variation
      of the algorithm needs to produce results for each data set.  The column
      of the data set also contains the parameters regarding the mode of
      <methodname>H-PCC</methodname> — forcing or not forcing a specific number
      of clusters and, when forcing, the number of target clusters.  The column
      <quote>Id</quote> contains a code for the graphs in this subsection.
    </para>

    <para>
      The same information is shown in the graphs provided in the rest of this
      section (<xref linkend="fig.perf-hpcc-datasets"/>, and <xref
      linkend="fig.perf-hpcc-totals"/>).  However, the TrD mode is not included,
      since otherwise the graphs would not convey much information (in some
      cases the time spent was so high that the bars for the rest of the modes
      would be barely visible).
    </para>

    <table>
      <title id="tab.PerformanceHPCC">Performance with HPCC</title>

      <tgroup cols="7">
	<colspec colname="c1" align="center" width="1*"/>
	<colspec colname="c2" align="justify" width="2*"/>
	<colspec colname="c3" align="center" width="1*"/>
	<colspec colname="c4" align="center" width="2*"/>
	<colspec colname="c5" align="center" width="2*"/>
	<colspec colname="c6" align="center" width="2*"/>
	<colspec colname="c7" align="center" width="2*"/>

	<thead>
	  <row>
	    <entry namest="c1" align="center">Id</entry>
	    <entry namest="c2" align="center">Data Set</entry>
	    <entry namest="c3" align="center">Or</entry>
	    <entry namest="c4" align="center">TrA</entry>
	    <entry namest="c5" align="center">TrB</entry>
	    <entry namest="c6" align="center">TrC</entry>
	    <entry namest="c7" align="center">TrD</entry>
	  </row>
	</thead>

	<tbody>
	  <row>
	    <entry>A</entry>
	    <entry><filename>curves2d</filename> (no force)</entry>
	    <entry>3.132s</entry>
	    <entry>3.477s (111.02%)</entry>
	    <entry>6.245s (199.39%)</entry>
	    <entry>6.617s (211.27%)</entry>
	    <entry>0.842s (26.88%)</entry>
	  </row>

	  <row>
	    <entry>B</entry>
	    <entry><filename>curves2d</filename> (force 6)</entry>
	    <entry>8.208s</entry>
	    <entry>9.092s (110.77%)</entry>
	    <entry>16.020s (195.18%)</entry>
	    <entry>17.256s (210.23%)</entry>
	    <entry>1.924s (23.44%)</entry>
	  </row>

	  <row>
	    <entry>C</entry>
	    <entry><filename>data3d</filename> (no force)</entry>
	    <entry>2.569s</entry>
	    <entry>2.654s (103.31%)</entry>
	    <entry>4.044s (157.42%)</entry>
	    <entry>14.987s (583.38%)</entry>
	    <entry>126.088s (4908.06%)</entry>
	  </row>

	  <row>
	    <entry>D</entry>
	    <entry><filename>data3d</filename> (force 7)</entry>
	    <entry>2.547s</entry>
	    <entry>2.708s (106.32%)</entry>
	    <entry>4.000s (157.05%)</entry>
	    <entry>14.856s (583.27%)</entry>
	    <entry>12.740s (500.20%)</entry>
	  </row>

	  <row>
	    <entry>E</entry>
	    <entry><filename>data5d</filename> (no force)</entry>
	    <entry>4.008s</entry>
	    <entry>4.025s (100.42%)</entry>
	    <entry>6.405s (159.81%)</entry>
	    <entry>11.484s (286.53%)</entry>
	    <entry>34.072s (850.10%)</entry>
	  </row>

	  <row>
	    <entry>F</entry>
	    <entry><filename>data5d</filename> (force 7)</entry>
	    <entry>4.105s</entry>
	    <entry>4.011s (97.71%)</entry>
	    <entry>6.374s (155.27%)</entry>
	    <entry>11.517s (280.56%)</entry>
	    <entry>6.858s (167.06%)</entry>
	  </row>

	  <row>
	    <entry>G</entry>
	    <entry><filename>arcs3d</filename> (no force)</entry>
	    <entry>21.321s</entry>
	    <entry>18.979s (89.02%)</entry>
	    <entry>30.109s (141.22%)</entry>
	    <entry>57.114s (267.88%)</entry>
	    <entry>38.171s (179.03%)</entry>
	  </row>

	  <row>
	    <entry>H</entry>
	    <entry><filename>arcs3d</filename> (force 3)</entry>
	    <entry>19.476s</entry>
	    <entry>19.148s (98.32%)</entry>
	    <entry>30.106s (154.58%)</entry>
	    <entry>57.395s (294.70%)</entry>
	    <entry>38.356s (196.94%)</entry>
	  </row>

	  <row>
	    <entry>I</entry>
	    <entry><filename>arcs3d _noise</filename> (no force)</entry>
	    <entry>25.667s</entry>
	    <entry>26.890s (104.76%)</entry>
	    <entry>42.954s (167.35%)</entry>
	    <entry>69.991s (272.69%)</entry>
	    <entry>113.933s (443.89%)</entry>
	  </row>

	  <row>
	    <entry>J</entry>
	    <entry><filename>arcs3d _noise</filename> (force 4)</entry>
	    <entry>26.306s</entry>
	    <entry>26.865s (102.12%)</entry>
	    <entry>43.167s (164.10%)</entry>
	    <entry>69.444s (263.99%)</entry>
	    <entry>77.404s (294.24%)</entry>
	  </row>

	  <row>
	    <entry>-</entry>
	    <entry>TOTAL</entry>
	    <entry>117.339s</entry>
	    <entry>117.948s (100.52%)</entry>
	    <entry>189.676s (161.65%)</entry>
	    <entry>331.956s (282.90%)</entry>
	    <entry>450.395s (383.84%)</entry>
	  </row>
	</tbody>
      </tgroup>
    </table>

    <figure xml:id="fig.perf-hpcc-datasets">
      <title>Performance of H-PCC with Data Sets</title>
      <mediaobject>
	<imageobject>
	  <imagedata align="center" width="10.5cm"
		     fileref="img/perf-plots/hpcc-datasets.svg"/>
	</imageobject>
      </mediaobject>
    </figure>

    <figure xml:id="fig.perf-hpcc-totals">
      <title>Performance of H-PCC with Totals</title>
      <mediaobject>
	<imageobject>
	  <imagedata align="center" width="10.5cm"
		     fileref="img/perf-plots/hpcc-totals.svg"/>
	</imageobject>
      </mediaobject>
    </figure>

    <para>
      Here are some remarks concerning the performance results of this
      algorithm:

      <itemizedlist>
	<listitem>
	  <para>
	    The translation using <varname>pcurve_rwrapper</varname> has the
	    same performance as the original software for these data sets.  With
	    the exception of <varname>pcurve_java_only</varname> mode (which we
	    already explained that it was quicker but spent more time because it
	    merged many more clusters for some data sets), the more it is
	    translated, the slower the method is.  This happens consistently in
	    all the data sets, and the differences are very noticeable.
	  </para>
	  <para>
	    This is probably due to the fact that the translation of
	    <methodname>H-PCC</methodname> does not have a big influence on the
	    performance, but the translation of <methodname>Principal
	    Curves</methodname> does.  The original <methodname>Principal
	    Curves</methodname> is coded in <methodname>Fortran</methodname> and
	    the generated code is very close to machine code, whereas our
	    translation is written in <methodname>Java</methodname> and it is
	    interpreted in a virtual machine, and this is known to cause
	    performance hits for numerical code.
	  </para>
	  <para>
	    Also, the implementation of <methodname>svd()</methodname> from
	    <methodname>Apache Commons Math</methodname> is slower than invoking
	    the code directly from <methodname>R</methodname> (we had already
	    noticed this during development).  Moreover, since our
	    implementation of the sorting algorithm was not optimised at all, it
	    is also slower than the original.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    Even if the data sets are small, it takes a substantial amount of
	    time to process them.  But not everything depends on the number of
	    points or dimensions.  For example, <filename>arcs3d</filename> has
	    only 1000 points and the same number of dimensions as
	    <filename>data3d</filename> (which has 100 more points), yet the
	    time spent to process the former is around eight to ten times more.
	    The number of merged clusters has a big impact, since every time a
	    merger occurs, everything has to be recalculated in order to decide
	    if a new merger has to be performed next or the process has
	    finished.
	  </para>
	  <para>
	    This can be easily seen also in the case of
	    <filename>curves2d</filename>.  When merging is not forced, the
	    number of resulting clusters is 9 and takes 3.132s; but when forcing
	    6 mergers, the process continues until this number is reached and it
	    takes 8.208s.  Therefore the latter situation takes more than 250%
	    the time of the former.
	  </para>
	</listitem>
      </itemizedlist>
    </para>
  </sect1>


  <sect1>
    <title>Performance of CEM-PCC</title>

    <para>
      The tables and graphs in this subsection show the performance of the
      original and translated software when running the second algorithm,
      <methodname>CEM-PCC</methodname>.
    </para>

    <para>
      The column headers and contents of the cells are analog to the
      <methodname>H-PCC</methodname> case, except for the data sets.  In this
      case, the name of some of the data sets includes the parameters for the
      <methodname>H-PCC</methodname> algorithm with that data set, since it
      produced different results with each invocation.  That is,
      <methodname>H-PCC</methodname> produced different results when using
      <filename>curves2d</filename> and invoked with the ‘no force’ mode and
      when invoked with ‘force 6’ mode; so we use both resulting clusterings as
      initial clusterings for <methodname>CEM-PCC</methodname>.  In the rest of
      the data sets the results of the different invocations of
      <methodname>H-PCC</methodname> produced the same clustering so, naturally,
      we use them only once.
    </para>

    <table>
      <title id="tab.PerformanceCEMPCC">Performance with CEM-PCC</title>

      <tgroup cols="7">
	<colspec colname="c1" align="center" width="1*"/>
	<colspec colname="c2" align="justify" width="2*"/>
	<colspec colname="c3" align="center" width="1*"/>
	<colspec colname="c4" align="center" width="2*"/>
	<colspec colname="c5" align="center" width="2*"/>
	<colspec colname="c6" align="center" width="2*"/>
	<colspec colname="c7" align="center" width="2*"/>

	<thead>
	  <row>
	    <entry namest="c1" align="center">Id</entry>
	    <entry namest="c2" align="center">Data Set</entry>
	    <entry namest="c3" align="center">Or</entry>
	    <entry namest="c4" align="center">TrA</entry>
	    <entry namest="c5" align="center">TrB</entry>
	    <entry namest="c6" align="center">TrC</entry>
	    <entry namest="c7" align="center">TrD</entry>
	  </row>
	</thead>

	<tbody>
	  <row>
	    <entry>A</entry>
	    <entry><filename>curves2d noforced</filename></entry>
	    <entry>4.941s</entry>
	    <entry>173.183s (3505.01%)</entry>
	    <entry>2.943s (59.56%)</entry>
	    <entry>2.866s (58.00%)</entry>
	    <entry>3.019s (61.10%)</entry>
	  </row>

	  <row>
	    <entry>B</entry>
	    <entry><filename>curves2d forced6</filename></entry>
	    <entry>6.657s</entry>
	    <entry>172.467s (2590.76%)</entry>
	    <entry>4.690s (70.45%)</entry>
	    <entry>4.621s (69.41%)</entry>
	    <entry>3.505s (52.65%)</entry>
	  </row>

	  <row>
	    <entry>C</entry>
	    <entry><filename>data3d</filename></entry>
	    <entry>62.188s</entry>
	    <entry>985.652s (1584.95%)</entry>
	    <entry>56.780s (91.30%)</entry>
	    <entry>57.938s (93.16%)</entry>
	    <entry>122.375s (196.78%)</entry>
	  </row>

	  <row>
	    <entry>D</entry>
	    <entry><filename>data5d</filename></entry>
	    <entry>241.807s</entry>
	    <entry>2752.429s (1138.27%)</entry>
	    <entry>253.538s (104.85%)</entry>
	    <entry>259.409s (107.27%)</entry>
	    <entry>146.462s (60.56%)</entry>
	  </row>

	  <row>
	    <entry>E</entry>
	    <entry><filename>arcs3d</filename></entry>
	    <entry>16.216s</entry>
	    <entry>121.268s (747.82%)</entry>
	    <entry>16.811s (103.66%)</entry>
	    <entry>17.440s (107.54%)</entry>
	    <entry>19.783s (121.99%)</entry>
	  </row>

	  <row>
	    <entry>F</entry>
	    <entry><filename>arcs3d noise</filename></entry>
	    <entry>29.211s</entry>
	    <entry>263.087s (900.64%)</entry>
	    <entry>31.892s (109.17%)</entry>
	    <entry>32.954s (112.81%)</entry>
	    <entry>56.262s (192.60%)</entry>
	  </row>

	  <row>
	    <entry>-</entry>
	    <entry>TOTAL</entry>
	    <entry>361.020s</entry>
	    <entry>4468.086s (1237.62%)</entry>
	    <entry>366.654s (101.56%)</entry>
	    <entry>375.228s (103.93%)</entry>
	    <entry>351.407s (97.33%)</entry>
	  </row>
	</tbody>
      </tgroup>
    </table>

    <para>
      Again, the graphs of the rest of this section show the same information
      but in a more terse and intuitive form.  We provide different versions of
      the graphs: with and without the TrA mode (<varname>rwrapper</varname>).
      This is due to the fact that when this mode is included, the graphs are
      not very informative (in all cases, its results are extremely high and
      this means that the bars for the rest of the modes are hardly visible in a
      graphical representation).
    </para>

    <figure xml:id="fig.perf-cempcc-datasets">
      <title>Performance of CEM-PCC with Data Sets</title>
      <mediaobject>
	<imageobject>
	  <imagedata align="center" width="10.5cm"
		     fileref="img/perf-plots/cempcc-datasets.svg"/>
	</imageobject>
      </mediaobject>
    </figure>

    <figure xml:id="fig.perf-cempcc-datasets_no_rwrapper">
      <title>Performance of CEM-PCC with Data Sets, no rwrapper</title>
      <mediaobject>
	<imageobject>
	  <imagedata align="center" width="10.5cm"
		     fileref="img/perf-plots/cempcc-datasets_no_rwrapper.svg"/>
	</imageobject>
      </mediaobject>
    </figure>

    <figure xml:id="fig.perf-cempcc-totals">
      <title>Performance of CEM-PCC with Totals</title>
      <mediaobject>
	<imageobject>
	  <imagedata align="center" width="10.5cm"
		     fileref="img/perf-plots/cempcc-totals.svg"/>
	</imageobject>
      </mediaobject>
    </figure>

    <figure xml:id="fig.perf-cempcc-totals_no_rwrapper">
      <title>Performance of CEM-PCC with Totals, no rwrapper</title>
      <mediaobject>
	<imageobject>
	  <imagedata align="center" width="10.5cm"
		     fileref="img/perf-plots/cempcc-totals_no_rwrapper.svg"/>
	</imageobject>
      </mediaobject>
    </figure>

    <para>
      In this case, the things are reversed in comparison with
      <methodname>H-PCC</methodname>:

      <itemizedlist>
	<listitem>
	  <para>
	    The translation using <varname>pcurve_rwrapper</varname> has by far
	    the worst performance (in total, more than 10 times slower than the
	    original).  The rest of the modes, except
	    <varname>pcurve_java_only</varname>, have more or less the same
	    performance as the original software for these data sets.
	    <varname>pcurve_java_only</varname> is some times quicker and some
	    times slower, but not as divergent as it was with
	    <methodname>H-PCC</methodname>.  In this case, it seems that the
	    translation of <methodname>CEM-PCC</methodname> does not have a
	    significant performance gain or hit, except when using
	    <varname>pcurve_rwrapper</varname>.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    <methodname>CEM-PCC</methodname> invokes many more times functions
	    from <methodname>Principal Curves</methodname> than
	    <methodname>H-PCC</methodname> does — the number of calls ranges
	    from tens of thousands to hundreds of thousands in the data sets
	    analysed.
	  </para>
	  <para>
	    <methodname>CEM-PCC</methodname> calls these functions several times
	    for every point in the whole set, whereas
	    <methodname>H-PCC</methodname> does it only for whole clusters.  As
	    a result, in many calls there is only one point involved (in some
	    cases the input is only one row), and the time to compute the result
	    is very small.  Therefore, probably the overhead of copying data to
	    the <methodname>R</methodname> interpreter and back again, along
	    with synchronising processes to exchange the data, compared to the
	    time spent doing calculations, results in the severe performance
	    loss.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    The implementation of <methodname>svd()</methodname> from
	    <methodname>Apache Commons Math</methodname> seems to be slower than
	    invoking the code from <methodname>R</methodname>, since when using
	    this mode (<varname>pcurve_java+apache_svd+r_smoothing</varname>)
	    the performance is consistently worse than when using the mode with
	    the <methodname>R</methodname> counterpart for
	    <methodname>svd()</methodname> — except for the smallest data set
	    (<filename>curves2d</filename>), in which case it is quicker but
	    almost negligible.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    As well as with <methodname>H-PCC</methodname>, the data sets are
	    small, yet it takes a significant amount of time to process them,
	    and much more time than with <methodname>H-PCC</methodname>.  In
	    this case, the algorithm depends more on the size of the input data
	    than in the case of <methodname>H-PCC</methodname> (e.g.,
	    <filename>data3d</filename> <foreignphrase>versus</foreignphrase>
	    <filename>data5d</filename>), but it also depends on how well
	    classified the data is beforehand.  For example,
	    <filename>arcs3d</filename> and <filename>arcs3d_noise</filename>
	    are of the same size, but the second has many points (wrongly)
	    considered noise, thus <methodname>CEM-PCC</methodname> needs to
	    spend a lot of time correcting the initial given clustering, almost
	    doubling the time needed for the version without noise.
	  </para>
	</listitem>
      </itemizedlist>
    </para>
  </sect1>

  <sect1>
    <title>Conclusions about Performance</title>

    <para>
      Given the results about performance measurements explained in the previous
      sections, it might seem that the conclusions are discouraging and the
      project is not very well succeeded.  However, from our point of view,
      there are several reasons to be moderately optimistic and be proud of the
      work done:

      <itemizedlist>
	<listitem>
	  <para>
	    First of all, as the quote of Donald Knuth in the beginning of the
	    chapter says, premature optimisation is often not a good idea.  We
	    did not try to optimise the results: our main goal was just to make
	    the system work with the most possible accuracy, so the side-effect
	    of not gaining in performance is not surprising.
	  </para>
	  <para>
	    There are many opportunities for optimisation, starting from the
	    optimisation of individual sentences or blocks of code, to use
	    multi-threading in single computer machines, or even to employ
	    techniques of distributed computing (e.g., Grid Computing) in large
	    clusters of computer systems, such as those often used in High
	    Energy physics.  This project would have explored those
	    possibilities, as stated in the initial goals, if the process of
	    translation were smoother and quicker than it actually was.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    Secondly, even if performance is a critical aspect of a project, the
	    motivation to translate the software into
	    <methodname>Java</methodname> was not improving its performance but
	    allowing its integration in DPAC systems.  Furthermore, it is clear
	    that <methodname>Java</methodname>, being interpreted, is not the
	    best language for numerical applications; and the core of the
	    original software is coded in a much better language for this
	    purpose.  In fact, given these precedents, having almost the same
	    performance as the original software could be even considered a
	    success.
	  </para>
	</listitem>
	<listitem>
	  <para>
	    In third and last place, and leaving aside the case of
	    <varname>pcurve_java_only</varname> (because the results are not
	    always comparable to those of the rest of modes and the original
	    software), the results obtained in many of the modes are very
	    similar, performance wise.  One could combine the
	    <varname>pcurve_rwrapper</varname> mode for
	    <methodname>H-PCC</methodname> with some of the
	    <quote>hybrid</quote> modes for <methodname>CEM-PCC</methodname>,
	    and the differences between the original and translated software
	    would be negligible (both in terms of performance as well as in
	    cluster analysis results).
	  </para>
	  <para>
	    In fact, the time that <methodname>H-PCC</methodname> spends
	    compared to that of <methodname>CEM-PCC</methodname> is not very
	    high.  Choosing only the <quote>hybrid</quote> modes (which perform
	    similarly with <methodname>CEM-PCC</methodname> and are a bit slower
	    with <methodname>H-PCC</methodname> — always compared to the
	    original software) for both algorithms would probably mean to pay
	    only a small penalty, overall.
	  </para>
	</listitem>
      </itemizedlist>
    </para>

    <para>
      In summary, we conclude that even if the performance results could be
      better, there is no need to be overly pessimistic; the work developed
      during this project of translating the original software did not focus on
      performance but on accuracy, and the system was translated into
      <methodname>Java</methodname> for reasons other than gaining in
      performance.  Finally, the results are not so bad as to be considered an
      outright failure, at all.
    </para>

    <para>
      In the end, if one data set can be analysed with the original software, it
      can be analysed again in the new system with the maximum guarantees of
      accuracy and performance.  And that is not a small achievement.
    </para>
  </sect1>
</chapter>
