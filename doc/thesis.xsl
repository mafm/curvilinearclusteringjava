<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:ng="http://docbook.org/docbook-ng"
                xmlns:db="http://docbook.org/ns/docbook"
                exclude-result-prefixes="db ng exsl d"
                version='1.0'>

<xsl:include href="/usr/share/xml/docbook/stylesheet/docbook-xsl-ns/fo/docbook.xsl"/>

<xsl:param name="paper.type">A4</xsl:param>
<xsl:param name="page.margin.top">1.75cm</xsl:param>
<xsl:param name="page.margin.bottom">1.75cm</xsl:param>
<xsl:param name="page.margin.inner">2cm</xsl:param>
<xsl:param name="page.margin.outer">2cm</xsl:param>
<xsl:param name="body.margin.top">1.25cm</xsl:param>
<xsl:param name="body.margin.bottom">1.1cm</xsl:param>
<xsl:param name="body.start.indent">0.5cm</xsl:param>
<xsl:param name="body.end.indent">0.5cm</xsl:param>
<xsl:param name="header.column.widths">1 3 1</xsl:param>
<xsl:param name="header.rule">true</xsl:param>
<xsl:param name="footer.rule">true</xsl:param>
<xsl:param name="body.font.size">12pt</xsl:param>
<!-- xsl:attribute-set name="monospace.properties">
  <xsl:attribute name="font-size">0.85em</xsl:attribute>
</xsl:attribute-set -->
<xsl:attribute-set name="monospace.verbatim.properties" use-attribute-sets="verbatim.properties monospace.properties">
  <xsl:attribute name="wrap-option">no-wrap</xsl:attribute>
  <xsl:attribute name="font-size">0.833em</xsl:attribute>
</xsl:attribute-set>

<!-- xsl:param name="shade.verbatim">1</xsl:param -->

<xsl:attribute-set name="blockquote.properties">
<xsl:attribute name="margin-{$direction.align.start}">0.75cm</xsl:attribute>
<xsl:attribute name="margin-{$direction.align.end}">0.75cm</xsl:attribute>
<xsl:attribute name="space-after.optimum">1.0em</xsl:attribute>
<xsl:attribute name="space-after.minimum">0.8em</xsl:attribute>
<xsl:attribute name="space-after.maximum">1.5em</xsl:attribute>
<xsl:attribute name="line-height">1em</xsl:attribute>
<xsl:attribute name="font-size">0.85em</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="abstract.properties">
  <xsl:attribute name="start-indent">1cm</xsl:attribute>
  <xsl:attribute name="end-indent">1cm</xsl:attribute>
  <!-- xsl:attribute name="font-style">italic</xsl:attribute -->
</xsl:attribute-set>


<xsl:attribute-set name="informalequation.properties" use-attribute-sets="informal.object.properties">
  <xsl:attribute name="start-indent">2cm</xsl:attribute>
  <xsl:attribute name="end-indent">2cm</xsl:attribute>

  <xsl:attribute name="space-before.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-before.optimum">0.75em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1em</xsl:attribute>
  <xsl:attribute name="space-after.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-after.optimum">0.75em</xsl:attribute>
  <xsl:attribute name="space-after.maximum">1em</xsl:attribute>
</xsl:attribute-set>

<!-- xsl:param name="double.sided">true</xsl:param -->

<xsl:attribute-set name="normal.para.spacing">
  <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
  <xsl:attribute name="line-height">1.75em</xsl:attribute>
</xsl:attribute-set>


<!-- xsl:param name="title.margin.left">-4pc</xsl:param -->

<!-- xsl:param name="body.font.master">10</xsl:param -->
<!-- xsl:param name="body.font.size">
 <xsl:value-of select="$body.font.master"></xsl:value-of><xsl:text>pt</xsl:text>
</xsl:param -->

<!-- xsl:param name="generate.toc">
book      title,toc,figure,table,example,equation
</xsl:param -->

<xsl:param name="toc.section.depth">4</xsl:param>

<xsl:param name="chapter.autolabel">1</xsl:param>
<xsl:param name="section.autolabel">1</xsl:param>
<xsl:param name="section.label.includes.component.label">true</xsl:param>
<xsl:attribute-set name="component.title.properties">
  <xsl:attribute name="hyphenate">false</xsl:attribute>
  <xsl:attribute name="start-indent"><xsl:value-of select="$title.margin.left"></xsl:value-of></xsl:attribute>
  <xsl:attribute name="font-size">
    <xsl:value-of select="$body.font.master * 1.75"></xsl:value-of>
    <xsl:text>pt</xsl:text>
  </xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="section.title.properties">
  <xsl:attribute name="space-before.optimum">2em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">1.5em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">2.5em</xsl:attribute>
  <xsl:attribute name="hyphenate">false</xsl:attribute>
  <xsl:attribute name="start-indent"><xsl:value-of select="$title.margin.left"></xsl:value-of></xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="section.title.level1.properties">
  <xsl:attribute name="font-size">
    <xsl:value-of select="$body.font.master * 1.500"></xsl:value-of>
    <xsl:text>pt</xsl:text>
  </xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="section.title.level2.properties">
  <xsl:attribute name="font-size">
    <xsl:value-of select="$body.font.master * 1.333"></xsl:value-of>
    <xsl:text>pt</xsl:text>
  </xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="section.title.level3.properties">
  <xsl:attribute name="font-size">
    <xsl:value-of select="$body.font.master * 1.166"></xsl:value-of>
    <xsl:text>pt</xsl:text>
  </xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="section.title.level4.properties">
  <xsl:attribute name="font-size">
    <xsl:value-of select="$body.font.master * 1.100"></xsl:value-of>
    <xsl:text>pt</xsl:text>
  </xsl:attribute>
</xsl:attribute-set>

<!-- Functions

  <xsl:attribute name="space-before.optimum"><xsl:value-of select="concat($body.font.master, 'pt * 2')"></xsl:value-of></xsl:attribute>
  <xsl:attribute name="space-before.minimum"><xsl:value-of select="concat($body.font.master, 'pt * 1.6')"></xsl:value-of></xsl:attribute>
  <xsl:attribute name="space-before.maximum"><xsl:value-of select="concat($body.font.master, 'pt * 2.5')"></xsl:value-of></xsl:attribute>
-->

<xsl:attribute-set name="figure.properties">
  <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1.5em</xsl:attribute>
  <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-after.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-after.maximum">1.5em</xsl:attribute>
  <xsl:attribute name="hyphenate">false</xsl:attribute>
  <xsl:attribute name="text-align">center</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="example.properties">
  <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1.5em</xsl:attribute>
  <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-after.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-after.maximum">1.5em</xsl:attribute>
  <xsl:attribute name="hyphenate">false</xsl:attribute>
  <xsl:attribute name="text-align">center</xsl:attribute>
  <xsl:attribute name="border">0.5pt solid black</xsl:attribute>
  <xsl:attribute name="background-color">#E0E0E0</xsl:attribute>
  <xsl:attribute name="padding-left">8pt</xsl:attribute>
  <xsl:attribute name="padding-right">8pt</xsl:attribute>
</xsl:attribute-set>
<xsl:attribute-set name="table.properties">
  <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1.5em</xsl:attribute>
  <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
  <xsl:attribute name="space-after.minimum">0.5em</xsl:attribute>
  <xsl:attribute name="space-after.maximum">1.5em</xsl:attribute>
  <xsl:attribute name="hyphenate">false</xsl:attribute>
  <xsl:attribute name="text-align">center</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="list.block.properties">
  <xsl:attribute name="provisional-label-separation">0.2em</xsl:attribute>
  <xsl:attribute name="provisional-distance-between-starts">1.5em</xsl:attribute>
  <xsl:attribute name="start-indent">3em</xsl:attribute>
</xsl:attribute-set>

<xsl:param name="glossterm.auto.link">1</xsl:param>
<!-- xsl:param name="bibliography.style">normal</xsl:param -->
<xsl:param name="bibliography.style">iso690</xsl:param>

<xsl:attribute-set name="olink.properties">
  <xsl:attribute name="font-family">{$monospace.font.family}</xsl:attribute>
  <xsl:attribute name="hyphenate">false</xsl:attribute>
  <xsl:attribute name="wrap-option">no-wrap</xsl:attribute>
</xsl:attribute-set>

<xsl:param name="highlight.source">1</xsl:param>

</xsl:stylesheet>
