source("hpcc.r")
source("cempcc.r")
library(princurve)

hpcc.wrapper<-function(base.filename, hpcc.force, hpcc.forcenum) {
        start=Sys.time()

	data=as.matrix(read.table(paste(base.filename, "-hpcc-in.x", sep="")))
	df=as.integer(read.table(paste(base.filename, "-hpcc-in.df", sep="")))
	initclust=as.matrix(read.table(paste(base.filename, "-hpcc-in.initclust", sep="")))

	result=hpcc(x=data, df=df, clust.init=initclust, force=hpcc.force, forcenum=hpcc.forcenum)

	if (hpcc.force == TRUE) {
	   mode = paste("force", hpcc.forcenum, sep="")
	} else {
	   mode = "noforce"
	}

	write.table(result$classes, paste(base.filename, "-hpcc-", mode, "-originalR.out.finalclust", sep=""),row.names=F,col.names=F)

        timespent=difftime(Sys.time(), start)
	cat("\n"); print(timespent); cat("\n\n")
}

cempcc.wrapper<-function(base.filename) {
        start=Sys.time()

	data=as.matrix(read.table(paste(base.filename, "-cempcc-in.x", sep="")))
	df=as.integer(read.table(paste(base.filename, "-cempcc-in.df", sep="")))
	initclust=as.matrix(read.table(paste(base.filename, "-cempcc-in.initclust", sep="")))

	result=cem(x=data, df=df, clust.init=initclust)

	write.table(result$var.bound,file=paste(base.filename, "-cempcc-originalR.out.cem_varbound", sep=""),row.names=F,col.names=F)
	write.table(result$iterations,file=paste(base.filename, "-cempcc-originalR.out.cem_iterations", sep=""),row.names=F,col.names=F)

	write.table(result$clust,file=paste(base.filename, "-cempcc-originalR.out.cem_clustering", sep=""),row.names=F,col.names=F)
	write.table(result$clust.best,file=paste(base.filename, "-cempcc-originalR.out.cem_bestclustering", sep=""),row.names=F,col.names=F)
	write.table(result$clust.log,file=paste(base.filename, "-cempcc-originalR.out.cem_clusteringlog", sep=""),row.names=F,col.names=F)

	write.table(result$like.about,file=paste(base.filename, "-cempcc-originalR.out.cem_likelihoodsabout", sep=""),row.names=F,col.names=F)
	write.table(result$like.along,file=paste(base.filename, "-cempcc-originalR.out.cem_likelihoodsalong", sep=""),row.names=F,col.names=F)
	write.table(result$likelihoods,file=paste(base.filename, "-cempcc-originalR.out.cem_likelihoods", sep=""),row.names=F,col.names=F)
	write.table(result$likelihoods.mix,file=paste(base.filename, "-cempcc-originalR.out.cem_likelihoodsmix", sep=""),row.names=F,col.names=F)
	write.table(result$overall.loglikelihood,file=paste(base.filename, "-cempcc-originalR.out.cem_ovloglike", sep=""),row.names=F,col.names=F)

	write.table(result$all.curvevars,file=paste(base.filename, "-cempcc-originalR.out.cem_allcurvevars", sep=""),row.names=F,col.names=F)
	write.table(result$all.curvelengths,file=paste(base.filename, "-cempcc-originalR.out.cem_allcurvelengths", sep=""),row.names=F,col.names=F)
	write.table(result$all.meanalong,file=paste(base.filename, "-cempcc-originalR.out.cem_allmeanalong", sep=""),row.names=F,col.names=F)
	write.table(result$all.varalong,file=paste(base.filename, "-cempcc-originalR.out.cem_allvaralong", sep=""),row.names=F,col.names=F)
	write.table(result$all.mixprop,file=paste(base.filename, "-cempcc-originalR.out.cem_allmixprop", sep=""),row.names=F,col.names=F)

        timespent=difftime(Sys.time(), start)
	cat("\n"); print(timespent); cat("\n\n")
}


hpcc.wrapper("hpcc/curves2d", hpcc.force=FALSE, hpcc.forcenum=0)
hpcc.wrapper("hpcc/curves2d", hpcc.force=TRUE, hpcc.forcenum=6)
hpcc.wrapper("hpcc/data3d", hpcc.force=FALSE, hpcc.forcenum=0)
hpcc.wrapper("hpcc/data3d", hpcc.force=TRUE, hpcc.forcenum=7)
hpcc.wrapper("hpcc/data5d", hpcc.force=FALSE, hpcc.forcenum=0)
hpcc.wrapper("hpcc/data5d", hpcc.force=TRUE, hpcc.forcenum=7)
hpcc.wrapper("hpcc/arcs3d", hpcc.force=FALSE, hpcc.forcenum=0)
hpcc.wrapper("hpcc/arcs3d", hpcc.force=TRUE, hpcc.forcenum=3)
hpcc.wrapper("hpcc/arcs3d_noise", hpcc.force=FALSE, hpcc.forcenum=0)
hpcc.wrapper("hpcc/arcs3d_noise", hpcc.force=TRUE, hpcc.forcenum=4)


cempcc.wrapper("cempcc/curves2d_noforced")
cempcc.wrapper("cempcc/curves2d_forced6")
cempcc.wrapper("cempcc/data3d")
cempcc.wrapper("cempcc/data5d")
cempcc.wrapper("cempcc/arcs3d")
cempcc.wrapper("cempcc/arcs3d_noise")

