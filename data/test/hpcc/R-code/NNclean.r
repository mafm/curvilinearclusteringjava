"NNclean" <- 
function(data, k, distances = NULL, edge.correct = F, wrap = 
	0.10000000000000001, convergence = 0.001, plothist = FALSE)
{
	#
	# Terms of use: GNU General Public License version 2 or newer
	#
	# Function to perform the Nearest Neighbour cleaning of features 
	# in a  point process as in 
	# Byers and Raftery (1998), "Nearest neighbor clutter removal for 
	#      estimating features in spatial point processes", 
	#      Journal of the American Statistical Association 93:577-584.
	# Available as Technical Report 305 (1996) from
	#    http://www.stat.washington.edu/www/research/reports
	#
	# To use this function, execute the S-PLUS command
	#       source("NNclean.q")
	#
	# NOTE: this function will execute much faster with the 
	#       S+SPATIALSTATS module. The S-PLUS command
	#             module(spatial) 
	#       will give you access to the spatial module if it is
	#       available on your system.
	#
	# mail to byers@stat.washington.edu or 
	#         raftery@stat.washington.edu
	#
	# WE CAN TRY TO HELP WITH PROBLEMS BUT CANNOT GUARANTEE
	# ACCURACY OR RESULTS.
	#
	# Input:
	#  data         - a matrix of points in the process
	#  k            - number of nearest neighbors
	#  distances    -  distances between points in data, e.g.
	#                 the result of dist(data). This is optional
	#                 and will make the performance a bit more
	#                 efficient when the spatial module is not
	#                 available. It is not used if the spatial module 
	#                 is available. 
	#  edge.correct - whether or not to use toroidal edge correction when
	#                 data is 2-dimensional (default T)
	#  wrap         - fractional degree of edge correction (default 0.1)
	#  convergence  - covergence tolerance (default 0.001)
	#  plothist     - whether or not to plot the histogram of the
	#                 nearest-neighbor distances
	#
	# The output is a list with the following components:
	#  z      - classification (1= in cluster. 0= in noise)
	#  probs  - probabilities from which the classifiation is derived
	#  lambda1, lambda2, p - describe the Poisson process mixture 
	#     modeling the distance to the kth nearest neighbor
	#  kthNND - kth nearest neighbor distances
	#
	# written originally in Splus 3.3
	#
	# Permission is hereby given to StatLib to redistribute this software.
	# The software can be freely used for non-commercial purposes, and can
	# be freely distributed for non-commercial purposes only. 
	#
	if(!exists("quad.tree") || !exists("find.neighbor")) cat(
			"would execute faster with the S-PLUS spatial module...\n"
			)
	data <- as.matrix(data)
	d <- dim(data)[2.]
	n <- dim(data)[1.]
	if((d == 2.) && (edge.correct == TRUE)) {
		r1 <- diff(range(data[, 1.]))
		r2 <- diff(range(data[, 2.]))
		tran2 <- matrix(c(rep(0., n), rep(r2, n)), byrow = FALSE, nrow
			 = n)
		tran1 <- matrix(c(rep(r1, n), rep(0., n)), byrow = FALSE, nrow
			 = n)
		aux.dat <- rbind(data + tran1, data - tran1, data + tran2,
			data - tran2, data + tran1 + tran2, data - tran1 + 
			tran2, data - tran1 - tran2, data + tran1 - tran2)
		aux.dat <- aux.dat[aux.dat[, 1.] < (max(data[, 1.]) + wrap *
			r1) & aux.dat[, 1.] > (min(data[, 1.]) - wrap * r1) &
			aux.dat[, 2.] < (max(data[, 2.]) + wrap * r2) & aux.dat[
			, 2.] > (min(data[, 2.]) - wrap * r2),  ]
		full.data <- rbind(data, aux.dat)
	}
	else {
		full.data <- data
	}
	#
	# Find modules on the system
	#
	#
	dDk <- function(x, lambda, k, d, alpha.d)
	{
		(exp( - lambda * alpha.d * x^d) * 2. * (lambda * alpha.d)^
			k * x^(d * k - 1.))/gamma(k)
	}
	if(exists("quad.tree") && exists("find.neighbor")) {
		useful.tree <- quad.tree(full.data, bucket.size = 5.)
		allNND <- matrix(find.neighbor(full.data, useful.tree, k = k +
			1.)[, 3.], nrow = dim(data)[1.], ncol = k + 1., byrow
			 = TRUE)
		kthNND <- apply(allNND, 1., max)
	}
	else {
		#
		# no spatial module do it the slow way
		#
		#  This next part sorts through the Splus distance object 
		#  and forms kthNND, kth nearest neighbour distance, 
		#  for each point.
		#
		if(is.null(distances)) {
			distances <- dist(full.data)
		}
		kthNND <- rep(0., n)
		Labels <- 1.:(n - 1.)
		kthNND[1.] <- sort(distances[Labels])[k]
		Labels[(2.):(n - 1.)] <- Labels[(2.):(n - 1.)] + (n - 1. - 1.)
		for(i in 2.:n) {
			kthNND[i] <- sort(distances[Labels])[k]
			Labels[1.:(i - 1.)] <- Labels[1.:(i - 1.)] + 1.
			Labels[(i + 1.):(n - 1.)] <- Labels[(i + 1.):(n - 1.)] +
				(n - i - 1.)
		}
	}
	kthNND <- kthNND[1.:n]
	#
	# Now use kthNND in E-M algorithm, first get starting guesses.
	#
	alpha.d <- (2. * pi^(d/2.))/(d * gamma(d/2.))
	delta <- probs <- rep(0, n)
	delta[kthNND > (min(kthNND) + diff(range(kthNND))/3.)] <- 1.
	p <- 0.5
	lambda1 <- k/(alpha.d * mean((kthNND[delta == 0.])^d))
	lambda2 <- k/(alpha.d * mean((kthNND[delta == 1.])^d))
	loglik.old <- 0.
	loglik.new <- 1.
	#
	# Iterator starts here, 
	#
	Z <- !kthNND
	while(abs(loglik.new - loglik.old)/(1 + abs(loglik.new)) > convergence
		) {
		# E - step
		# M - step
		delta1 <- dDk(kthNND[!Z], lambda1, k = k, d = d, alpha.d = 
			alpha.d)
		delta2 <- dDk(kthNND[!Z], lambda2, k = k, d = d, alpha.d = 
			alpha.d)
		delta[!Z] <- (p * delta1)/(p * delta1 + (1 - p) * delta2)
		delta[Z] <- 0
		p <- sum(delta)/n
		lambda1 <- (k * sum(delta))/(alpha.d * sum((kthNND^d) * delta))
		lambda2 <- (k * sum((1. - delta)))/(alpha.d * sum((kthNND^
			d) * (1. - delta)))
		loglik.old <- loglik.new
		loglik.new <- sum( - p * lambda1 * alpha.d * ((kthNND^d) * 
			delta) - (1. - p) * lambda2 * alpha.d * ((kthNND^d) *
			(1. - delta)) + delta * k * logb(lambda1 * alpha.d) +
			(1. - delta) * k * logb(lambda2 * alpha.d))
		print(loglik.new)
	}
	if(plothist) {
		hist(kthNND, nclass = 20., axes = T, ylab = 
			"Estimate of Mixture", xlim = c(0., max(kthNND)), 
			probability = TRUE, xlab = paste("Distance to", eval(
			k), "th nearest neighbour"))
		box()
		support <- seq(0., max(kthNND), length = 200.)
		lines(support, (p * dDk(support, lambda1, k = k, d = d, alpha.d
			 = alpha.d) + (1. - p) * dDk(support, lambda2, k = k,
			d = d, alpha.d = alpha.d)))
	}
	delta1 <- dDk(kthNND[!Z], lambda1, k = k, d = d, alpha.d = alpha.d)
	delta2 <- dDk(kthNND[!Z], lambda2, k = k, d = d, alpha.d = alpha.d)
	probs[!Z] <- delta1/(delta1 + delta2)
	probs[Z] <- 1
	#
	# z will be the classifications. 1= in cluster. 0= in noise.
	#
	list(z = round(probs), probs = probs, lambda1 = lambda1, lambda2 = 
		lambda2, p = p, kthNND = kthNND)
}

