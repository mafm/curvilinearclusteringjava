#This file has two functions: cem and calc.likelihood

cem<-function(x,df=5,clust.init=rep(0,(length(x[,1]))),trace=F,
	max.iter=12,plot.me=F,trim=0,mix=T,var.bound=-1)  {

#x is the data in two columns (each row is a data point)
#clust.init is the initial clustering; it has the same length as the
#   number of rows in x, with each entry giving the number of the cluster
#   to which that point initially belongs.  The feature clusters are 
#   numbered by consecutive integers starting with 1; 0 indicates the
#   noise cluster. 
#trace=T causes debugging information to be printed
#max.iter is the number of EM iterations to do
#plot.me=T causes results to be plotted
#trim can be used to compute a robust estimate of the variance about the
#   curve for any feature cluster.  The trim amount should be between
#   0 and 0.4.
# mix determines whether to use the likelihood or the mixture
#   likelihood (recommended) for each point when forming the new
#   clustering at each iteration.  The default value T uses the
#   mixture likelihood.
#var.bound is a lower bound on the estimate of the variance about the
#   curve for any feature cluster.  If var.bound < 0, it is computed
#   automatically based on the range of the data.  Setting var.bound
#   equal to zero removes any constraint from the variance estimate.


n<-length(x[,1])
#CHANGED BY LSB
hypervolume <- 1.0
p<-length(x[1,])
max.range <- 0
for (i in 1:p)
  {
    tmp <- diff(range(x[,i]))
    hypervolume <- hypervolume * tmp
    if (tmp > max.range) {max.range <- tmp }
  }
#area<-(max(x[,1])-min(x[,1]))*(max(x[,2])-min(x[,2]))
#END CHANGE
done<-F
iterations<-0

# initialize the clustering and the variables which will keep track of
# results at each iteration
clust<-clust.init
clust.log<-rep(0,max.iter)
overall.loglikelihood<-rep(0,max.iter)
all.curvevars<-matrix(rep(0,max.iter*length(unique(clust))),
	                                   ncol=length(unique(clust)))
all.varalong<-matrix(rep(0,max.iter*length(unique(clust))),
	                                   ncol=length(unique(clust)))
all.meanalong<-matrix(rep(0,max.iter*length(unique(clust))),
	                                   ncol=length(unique(clust)))
all.curvelengths<-matrix(rep(0,max.iter*length(unique(clust))),
	                                   ncol=length(unique(clust)))
all.mixprop<-matrix(rep(0,max.iter*length(unique(clust))),
	                                   ncol=length(unique(clust)))

#compute the variance bound, if needed
if (var.bound<0)
  { 
#CHANGED BY LSB
#    var.bound<- (max(c(range(x[,1])[2]-range(x[,1])[1],
#                       range(x[,2])[2]-range(x[,2])[1]))/4000)^2
    var.bound <- (max.range/4000)^2
#END CHANGE
  }
#mafm
#        cat("  - hypervolume=",hypervolume,"\n")
#        cat("  - var.bound=",var.bound,"\n")
#/mafm
	


### MAIN PROGRAM LOOP ###	
while (done==F)
  {

#initialize loop variables
last.clust<-clust
classes<-unique(clust)
num.of.classes<-length(classes)  
curvelengths<-rep(0,num.of.classes)
curvevars<-rep(0,num.of.classes)
varalong<-rep(0,num.of.classes)
meanalong<-rep(0,num.of.classes)
sqdists<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
distalong<-matrix(rep(NA,n*num.of.classes),ncol=num.of.classes)
likelihoods<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
likelihoods.mix<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
like.along<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)
like.about<-matrix(rep(0,n*num.of.classes),ncol=num.of.classes)

if (trace) {cat("Clustering\n",clust,"\n")}
if (plot.me) {	plot(x,xlab="X",ylab="Y",main="CEM Clustering",pch=" ")}
	

#------------------------------------------------------
#fit curve and get parameter estimates for each cluster
	
for ( j in 1:num.of.classes)
  {

    if (classes[j]>0)
      {
        #this is a feature cluster
        filter<-(clust==classes[j])
        n.curve <- length(x[filter, 1])
	if (n.curve!=sum(filter) || sum(filter) == 0)
          {
            stop("Filter error.")
          }
#mafm
#        print(c("Dimension of x[filter] = ",dim(x[filter,])))
#/mafm

#mafm
#write.table(x[filter,],file="/tmp/1-princurve.in.x-R",row.names=F,col.names=F)
#/mafm
        curve <- principal.curve(x[filter,],maxit=5, smoother=
                                 "smooth.spline",df=df)
#mafm
#write.table(curve$s,file="/tmp/1-princurve.out.s-R",row.names=F,col.names=F)
#write.table(curve$lambda,file="/tmp/1-princurve.out.lambda-R",row.names=F,col.names=F)
#write.table(curve$tag,file="/tmp/1-princurve.out.tag-R",row.names=F,col.names=F)
#write.table(curve$dist,file="/tmp/1-princurve.out.dist-R",row.names=F,col.names=F)
#/mafm
	if (plot.me)
          {
	    plot(x,xlab="X",ylab="Y")
	    points(x[clust==classes[j],],pch=classes[j]+3)
	    points(x[clust!=classes[j],])
	    lines(curve)
          }
        
        curvelengths[j] <- (curve$lambda[curve$tag])[n.curve]
        
	if (trace)
          {
            cat("\n","j= ",j," \n")
            cat("classes[j]= ",classes[j]," \n")
            cat("n.curve= ",n.curve," \n")
            cat("curvelengths[j]= ",curvelengths[j]," \n")
            cat("length(curve$lambda) ",length(curve$lambda)," \n")
            cat("length(curve$tag)",length(curve$tag)," \n")
#mafm
#            cat("curve$lambda[curve$tag]",curve$lambda[curve$tag]," \n")
#/mafm
          }

     #naive variance estimate: curvevars[j] <- (curve$dist)/(n.curve - 1)
     #or robust estimate from trimmed variance
        temp.dists<-x[filter,]
#CHANGED BY LSB ** To be implemented in java by calling vdist**
#        temp.dists<-((temp.dists[,1]-curve$s[,1])^2)+
#                    ((temp.dists[,2]-curve$s[,2])^2)
        nr<-length(temp.dists[,1])
        nc<-length(temp.dists[1,])
        total <- array(0,nr)

        for(i in 1:nr)
          {
            total[i]<-0
# Tell MFM
            for(ii in 1:nc) {total[i]<-total[i]+((temp.dists[i,ii]-curve$s[i,ii])^2) }   
          }

# Tell MFM
        temp.dists<-total[order(total)]

#END CHANGE

        if (trim>0)
          {
            trimlow<-ceiling((trim/2)*length(temp.dists))
            trimhigh<-floor((1-(trim/2))*length(temp.dists))
            temp.dists<-temp.dists[c(trimlow:trimhigh)]
          }	
#mafm
#write.table(temp.dists,file="/tmp/2-tmpdists-trimmed-R",row.names=F,col.names=F)
#/mafm
        
        #check variance bound
        curvevars[j] <-sum(temp.dists)/(length(temp.dists)-1)
	if (curvevars[j]<var.bound) { curvevars[j]<-var.bound }
#mafm
#        cat("  - curvevars[j] ",curvevars[j],"\n")
#write.table(curvevars,file="/tmp/3-curvevars-R",row.names=F,col.names=F)
#/mafm
	
# Loop over all points and (still) cluster j
        for (i in 1:n)
          {
#CHANGED BY LSB            
#            sqdists[i,j]<-get.lam(cbind(x[i, 1], x[i, 2]), 
#                                  s = curve$s, tag = curve$tag,stretch=0)$dist

# rbind needed for matrix input to get.lam # Comment by LSB
            sqdists[i,j]<-get.lam(rbind(x[i, ]), 
                                  s = curve$s, tag = curve$tag,stretch=0)$dist

#END CHANGE
          }
#mafm
#write.table(sqdists,file="/tmp/4-sqdists-R",row.names=F,col.names=F)
#/mafm
        
# distincurve stores distances along principal curve
	distincurve<-rep(NA,(n.curve-1))
        
	for (i in 2:n.curve)
          {
            distincurve[(i-1)]<-(curve$lambda[curve$tag])[i] -
              (curve$lambda[curve$tag])[i-1]
          }
	#ad hoc method to deal with distincurve at endpoints
	distincurve<-c(distincurve,distincurve[(n.curve-1)])
	
	meanalong[j]<-mean(distincurve)
        varalong[j]<-var(distincurve)
#mafm
#write.table(distincurve,file="/tmp/5-distincurve-R",row.names=F,col.names=F)
#        cat("  - distincurve ",distincurve,"\n")
#        cat("  - meanalong[j] ",meanalong[j],"\n")
#        cat("  - varalong[j] ",varalong[j],"\n")
#/mafm
        
	for (i in 1:n)
          {
          #find order on curve
            temp.filter<-filter
            filter.ind<-0
            if (temp.filter[i]) {filter.ind<-1}
#mafm
#        cat("  - temp.filter ",temp.filter,"\n")
#	stop("stop at filter");
#/mafm
            temp.filter[i]<-F
            this.proj<-NA
#mafm
#write.table(rbind(x[i,],x[temp.filter,]),file="/tmp/5-lambdawrapper.in.xlambda-R",row.names=F,col.names=F)
#write.table(curve$s,file="/tmp/5-lambdawrapper.in.curve_s-R",row.names=F,col.names=F)
#write.table(curve$tag,file="/tmp/5-lambdawrapper.in.curve_tag-R",row.names=F,col.names=F)
#/mafm
            this.proj<-get.lam(rbind(x[i,],x[temp.filter,]),s=curve$s,
                               tag=curve$tag,stretch=0)

#mafm
#write.table(this.proj$s,file="/tmp/5-lambdawrapper.out.s-R",row.names=F,col.names=F)
#write.table(this.proj$lambda,file="/tmp/5-lambdawrapper.out.lambda-R",row.names=F,col.names=F)
#write.table(this.proj$tag,file="/tmp/5-lambdawrapper.out.tag-R",row.names=F,col.names=F)
#write.table(this.proj$dist,file="/tmp/5-lambdawrapper.out.dist-R",row.names=F,col.names=F)
#       cat("  - this.proj$dist ",this.proj$dist,"\n")



#for (i.stable in 2:length(this.proj$tag)) {
#if (this.proj$tag[i.stable] == 1) {
#       cat("  - fixed stability between ",i.stable," and ", (i.stable-1), "\n")
#       cat("    - tag ",this.proj$tag[i.stable]," and ", this.proj$tag[(i.stable-1)], "\n")
#       cat("    - lambda ",this.proj$lambda[this.proj$tag[i.stable]]," and ", this.proj$lambda[this.proj$tag[(i.stable-1)]], "\n")
#	if (this.proj$lambda[this.proj$tag[i.stable]] == this.proj$lambda[this.proj$tag[i.stable-1]]
#            && this.proj$tag[i.stable] < this.proj$tag[i.stable-1]) {
#		aux=this.proj$tag[i.stable]
#		this.proj$tag[i.stable]=this.proj$tag[i.stable-1]
#		this.proj$tag[i.stable-1]=aux
#		cat("  - fixed stability between ",i.stable," and ", (i.stable-1), "\n")
#		cat("    - tag ",this.proj$tag[i.stable]," and ", this.proj$tag[(i.stable-1)], "\n")
#		cat("    - lambda ",this.proj$lambda[this.proj$tag[i.stable]]," and ", this.proj$lambda[this.proj$tag[(i.stable-1)]], "\n")
#	    }
#}
#}
#stop ("forced stop")
#/mafm
          #find index of this point. It is point i, but it will be the 
          #first point in this.proj 
            this.n<-sum(temp.filter)+1
            this.point<-(c(1:this.n))[this.proj$tag==1]
#mafm
#        cat("  - this.point calculation 1: ",this.proj$tag==1,"\n")
#        cat("  - this.point calculation 2: ",(c(1:this.n))[this.proj$tag==1],"\n")
#/mafm
            if (plot.me)
              {
                segments(x[i,1],x[i,2],this.proj$s[1,1],
                         this.proj$s[1,2])
              }
            if (this.point==1)
              {
	    #now we know that point i projects to the first endpoint
	    #we want to extend the line from the last few projection points
	    #to get distalong and sqdists.  This would be hard and
	    #computationally expensive, so we approximate:
                distalong[i,j]<-sqrt(sqdists[i,j])
	    #this will err on the side of making the curve too
	    #conservative at the endpoints
                if (trace)
                  {
                    cat("Point ",i," first in cluster ",j,
                        " distalong: ",distalong[i,j],"\n")
                  }
              }
            
            else
              {
                if (this.point==this.n)
                  {
            #now we know that point i projects to the last endpoint
	    #we want to extend the line from the last two projection points
	    #to get distalong and sqdists

                    distalong[i,j]<-sqrt(sqdists[i,j])
                    if (trace)
                      {
                        cat("Point ",i," last in cluster ",j,
                            " distalong: ",distalong[i,j],"\n")
                      }
                  }
	
                else
                  {
                #now we know that point i projects inside curve j
		
                #put the lambdas in order along the curve
                    current.lams<-this.proj$lambda[this.proj$tag]
                    dist1<-current.lams[this.point+1]-current.lams[this.point]
                    dist2<-current.lams[this.point]-current.lams[this.point-1]
                    distalong[i,j]<-dist1
               
                    if (trace)
                      {
                        cat("Point ",i," is ",this.point," in cluster ",j,
                            " distalong: ",distalong[i,j],"\n")
#mafm
#                        cat("  - sorted lambda thispoint+1= ",current.lams[this.point+1],"\n")
#                        cat("  - sorted lambda thispoint  = ",current.lams[this.point],"\n")
#                        cat("  - sorted lambda dist1      = ",dist1,"\n")
#/mafm
                      }
                  }#end of else
              }#end of else
#mafm
#                        cat("  - Point ",i," is ", x[i,],"\n")
#write.table(distalong,file="/tmp/7-distalong-R",row.names=F,col.names=F)
#			if (i == 92) { cat("\n\n\n\n\n"); stop("stop point ", i); }
#			if (j == 6) { cat("\n\n\n\n\n"); stop("stop class ", j); }
#stop("stop in 1st loop");
#/mafm
          }

#mafm
#write.table(distalong,file="/tmp/7-distalong-R",row.names=F,col.names=F)
#stop("stop - end of feature cluster");
#/mafm
      } #end of feature cluster routine

  
    if (classes[j]==0)
      { #now deal with noise cluster
        curvelengths[j]<-0
        curvevars[j]<-0
        for (i in 1:n)
          {
            sqdists[i,j]<-0
          }
        if (plot.me) { points(x[clust==classes[j],],pch=classes[j]+3)}
      } #end of noise cluster routine

  } #end of for loop

#mafm
#write.table(distalong,file="/tmp/7-distalong-R",row.names=F,col.names=F)
#stop("stop - end of for loop");
#/mafm

#---------------------------------------------------------
#calculate likelihood of each point being in each cluster

for (i in 1:n)
  {
    for (j in 1:num.of.classes)
      {
        if(classes[j]>0)
          {

### uniform*normal method:
            like.about[i,j]<-(1/(sqrt(2*pi*curvevars[j])))*
              (exp((-1/2)*(sqdists[i,j]/curvevars[j])))
            like.along[i,j]<-(1/curvelengths[j])
            mix.prop<-(sum(clust==classes[j]))/n
            likelihoods.mix[i,j]<- mix.prop*like.about[i,j]*like.along[i,j]
            likelihoods[i,j]<- like.about[i,j]*like.along[i,j]
          }

        if (classes[j]==0)
          {
#mafm
#            mix.prop<-(sum(clust==classes[j]))/nA
            mix.prop<-(sum(clust==classes[j]))/n
#/mafm
#CHANGED BY LSB Watch! area should be hypervolume above
#            likelihoods[i,j]<-1/area
            likelihoods[i,j]<-1/hypervolume
#            likelihoods.mix[i,j]<-mix.prop/area
            likelihoods.mix[i,j]<-mix.prop/hypervolume
#END CHANGE
            like.along[i,j]<-0
            like.about[i,j]<-0
            
          }	                   
      }
  }
#mafm
#write.table(like.about,file="/tmp/88-likelihoodsabout-R",row.names=F,col.names=F)
#write.table(like.along,file="/tmp/88-likelihoodsalong-R",row.names=F,col.names=F)
#write.table(likelihoods,file="/tmp/88-likelihoods-R",row.names=F,col.names=F)
#write.table(likelihoods.mix,file="/tmp/88-likelihoodsmix-R",row.names=F,col.names=F)
#stop("stop calc likelihoods");
#/mafm
#---------------------------------------------------------
#save all parameter estimates

#mafm
#    cat(" - all.curvevars[", iterations+1, ",] length is ",length(all.curvevars[iterations+1,]))
#    cat("   and curvevars length is",length(curvevars))
#    cat("   and curvelengths length is",length(curvelengths))
#    cat("   and meanalong length is",length(meanalong))
#    cat("   and varalong length is",length(varalong))
#    cat("   and mix.prop length is",num.of.classes, "\n")
#all.curvevars[iterations+1,]<-curvevars
#all.varalong[iterations+1,]<-varalong
#all.meanalong[iterations+1,]<-meanalong
#all.curvelengths[iterations+1,]<-curvelengths
#mix.prop<-rep(NA,num.of.classes)
#for (j in 1:num.of.classes) {mix.prop[j]<-(sum(clust==classes[j]))/n}
#all.mixprop[iterations+1,]<-mix.prop
all.curvevars[iterations+1,1:length(curvevars)]<-curvevars
all.varalong[iterations+1,1:length(varalong)]<-varalong
all.meanalong[iterations+1,1:length(meanalong)]<-meanalong
all.curvelengths[iterations+1,1:length(curvelengths)]<-curvelengths
mix.prop<-rep(NA,num.of.classes)
for (j in 1:num.of.classes) {mix.prop[j]<-(sum(clust==classes[j]))/n}
all.mixprop[iterations+1,1:length(mix.prop)]<-mix.prop
#mafm
	
#---------------------------------------------------------
# make new clustering

if (trace)
  {
    for (j in 1:num.of.classes)
      {
        cat("Likelihoods for cluster ",j,"\n",round(likelihoods[,j],dig=4),"\n\n")
      }
    cat("\n\nVar along: ",varalong,"\n")
    cat("\nVar about: ",curvevars,"\n")
    cat("\nMean along: ",meanalong,"\n")
  }
	
if (mix)
  {
    for (i in 1:n)
      {
        largest<-max(likelihoods.mix[i,])
        for (j in 1:num.of.classes)
          { 
#mafm
            if (likelihoods.mix[i,j]==largest) {clust[i]<-classes[j]}
#            if (likelihoods.mix[i,j]==largest) {
#		    clust[i]<-classes[j]
#		    cat("------------------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> clustering[", i-1, "]=", classes[j], "\n")
#		}
#/mafm
          }
      }
  }

if (!mix)
  {
    for (i in 1:n)
      {
        largest<-max(likelihoods[i,])
        for (j in 1:num.of.classes)
          { 
            if (likelihoods[i,j]==largest) {clust[i]<-classes[j]}
          }
      }
  }

#mafm
#write.table(clust,file=paste("/tmp/90-clustering_it",iterations,"-R", sep=""),row.names=F,col.names=F)
#/mafm
#---------------------------------------------------------
# test for convergence of clustering or max number of iterations

iterations<-iterations+1
sameness.counter<-0
for(i in 1:n)
  {
    if (last.clust[i]==clust[i]) {sameness.counter<-sameness.counter+1}
  }
clust.log[iterations]<-sameness.counter
if(sameness.counter==n) {done<-T}
if(iterations>=max.iter) {done<-T}
temp<-0
for (i in 1:n)
  {
    temp<-temp+log(sum(likelihoods.mix[i,]))
  }
overall.loglikelihood[iterations]<-temp
if
 (overall.loglikelihood[iterations]>=max(overall.loglikelihood[1:iterations])) 
  { clust.best<-clust }
	
#mafm
            cat("iteration ",(iterations-1),"\n")
#/mafm
} # end of while loop -- END OF MAIN PROGRAM LOOP
#---------------------------------------------------------
#mafm
#write.table(all.curvevars,file="/tmp/89-allcurvevars-R",row.names=F,col.names=F)
#write.table(all.curvelengths,file="/tmp/89-allcurvelengths-R",row.names=F,col.names=F)
#write.table(all.meanalong,file="/tmp/89-allmeanalong-R",row.names=F,col.names=F)
#write.table(all.varalong,file="/tmp/89-allvaralong-R",row.names=F,col.names=F)
#write.table(all.mixprop,file="/tmp/89-allmixprop-R",row.names=F,col.names=F)
#write.table(clust.log,file="/tmp/100-clusteringlog-R",row.names=F,col.names=F)
#write.table(overall.loglikelihood,file="/tmp/100-overallloglikelihood-R",row.names=F,col.names=F)
#stop("stop calc all* vars");
#/mafm


#mafm
#return(varalong,meanalong,curvelengths,curvevars,likelihoods,likelihoods.mix,
#	clust,iterations,clust.log,x,df,var.bound,like.along,like.about,
#	distalong,
#	sqdists,clust.best,overall.loglikelihood,
#	all.curvevars,all.varalong,all.meanalong,all.curvelengths,all.mixprop) 

structure(list(varalong=varalong,meanalong=meanalong,curvelengths=curvelengths,curvevars=curvevars,likelihoods=likelihoods,likelihoods.mix=likelihoods.mix,
	clust=clust,iterations=iterations,clust.log=clust.log,x=x,df=df,var.bound=var.bound,like.along=like.along,like.about=like.about,
	distalong=distalong,
	sqdists=sqdists,clust.best=clust.best,overall.loglikelihood=overall.loglikelihood,
	all.curvevars=all.curvevars,all.varalong=all.varalong,all.meanalong=all.meanalong,all.curvelengths=all.curvelengths,all.mixprop=all.mixprop),
    class="cem")
#/mafm
}

#---------------------------------------------------------
#---------------------------------------------------------
calc.likelihood<-
function(cem.obj, mix.best = T)
{
#cem.obj is the output from cem().  it includes:
#x is the data
#df is the degrees of freedom
#clust is the classification (0 indicates the noise cluster)
#likelihoods is a matrix of likelihoods (one column per cluster)
#if trim was used in cem, then the likelihood calculated here will
#reflect it

#mix.best is used to indicate if the best overall loglikelihood (over
#all iterations of CEM that were run) should be used

  n <- length(cem.obj$x[, 1])
  if(mix.best == F)
    {
      num.clusters <- length(unique(cem.obj$clust))
    }
  if(mix.best == T)
    {
      num.clusters <- length(unique(cem.obj$clust.best))
    }
  cluster.sizes <- rep(0, num.clusters)
  score <- 0
  if(mix.best == F)
    {
      for(i in 1:num.clusters)
        {
          cluster.sizes[i] <- sum(cem.obj$clust == unique(cem.obj$clust)[i])
        }

#now the entries of cluster.sizes correspond to each column of the
#likelihood matrix (regardless of ordering)
      for(i in 1:n)
        {
          this.lik <- 0
          for(j in 1:num.clusters)
            {
              this.lik <- this.lik + ((cluster.sizes[j]/n) * 
                                      cem.obj$likelihoods[i, j])
            }
          score <- score + log(this.lik)
        }
    }
  if(mix.best == T)
    {
      score <- max(cem.obj$overall.loglikelihood[cem.obj$
                                                 overall.loglikelihood != 0])
    }
#assume n-1 curve clusters, 1 noise cluster.
#estimate curves, mixture proportions, noise
  num.parameters <- ((num.clusters - 1) * (cem.obj$df + 2)) + (
                num.clusters - 1) + 1
  bic <- (2 * score) - (num.parameters * log(n))
#TOCHANGE But experimental Important: see changes if real dimension introduced
  bic2 <- (2 * score) - (2 * num.parameters * log(n))
#2= num of dimensions (bic2 is experimental)
#mafm
#  return(score, bic, bic2)
  structure(list(score=score, bic=bic, bic2=bic2), class="calculatelikelihood")
#/mafm
}


