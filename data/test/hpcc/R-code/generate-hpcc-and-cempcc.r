#from example_princlust.txt
generate.hpcc.and.cempcc <- function(filename, nc.mclust, nc.hpcc, nc.hpcc.force, df.hpcc, df.cempcc) {

sine.data <- as.matrix(read.table(filename))

# Source file to define noise cleaning functions
#remove estimated noise using NNclean 
source("NNclean.r")
sine.clean<-NNclean(sine.data,k=2)
#sine.clean<-data

#make initial clustering (into six clusters) using hclust
#data.dist = dist(sine.data[sine.clean$z==1,],method="eu")
#clust.hclust.wa <- hclust(data.dist, method="wa")
#curve1.hclust <- cutree(clust.hclust.wa, k=6)

#make initial clustering (into six clusters) using kmeans
#curve1.kmeans <- kmeans(sine.data[sine.clean$z==1,],9)

#make initial clustering (into six clusters) using mclust
library("mclust")
ncG<-nc.mclust
#mafm
#curve1.mclust<-Mclust(sine.data[sine.clean$z==1,],G=ncG)
curve1.mclust<-Mclust(sine.data,G=ncG)
#/mafm

#HPCC cluster down from 6 clusters to 1
#we can specify degrees of freedom here and again in cem, or
#approximate by using the default value for degrees of freedom 
#in hpcc
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.classes6$classification,force=T,forcenum=1,
#  plot.iter=F,plot.result=F)
source("hpcc.r")
library(princurve)
nchpcc<-nc.hpcc
nchpccforce<-nc.hpcc.force
dfhpcc <- df.hpcc
#mafm
curve1.hpcc<-hpcc(sine.data,
  clust.init=curve1.mclust$classification,force=nchpccforce,forcenum=nchpcc,
  plot.iter=F,plot.result=F,df=dfhpcc)
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.mclust$classification,force=nchpccforce,forcenum=nchpcc,
#  plot.iter=F,plot.result=F,df=dfhpcc)
#/mafm
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.hclust,force=T,forcenum=nchpcc,
#  plot.iter=F,plot.result=F,df=dfhpcc)
#curve1.hpcc<-hpcc(sine.data[sine.clean$z==1,],
#  clust.init=curve1.kmeans$cluster,force=T,forcenum=nchpcc,
#  plot.iter=F,plot.result=F,df=dfhpcc)

#make raw classification by combining hpcc and nnclean results
#(with only 1 feature, we can skip hpcc and use curve1.raw<-sine.clean$z)
curve1.raw<-rep(NA,length(sine.data[,1]))
counter<-0
for (i in 1:length(sine.data[,1])) {
if (sine.clean$z[i]==0) {curve1.raw[i]<-0}
if (sine.clean$z[i]==1) {counter<-counter+1
                     curve1.raw[i]<-curve1.hpcc$classes[counter]}
}
temp<-unique(curve1.hpcc$classes)
curve1.raw[curve1.raw==temp[1]]<-1

#mafm
write.table(sine.data, paste(filename, "-hpcc-in.x", sep=""),row.names=F,col.names=F)
write.table(curve1.mclust$classification, paste(filename, "-hpcc-in.initclust", sep=""),row.names=F,col.names=F)
#write.table(df.hpcc, paste(filename, "-hpcc-in.df", sep=""),row.names=F,col.names=F)
write.table(curve1.hpcc$classes, paste(filename, "-hpcc-noforce-out.finalclust", sep=""),row.names=F,col.names=F)
#quit()
#/mafm

source("cempcc.r")

#refine the clustering with CEM
dfcempcc <- df.cempcc
curve1.cem<-cem(x=sine.data,clust.init=curve1.hpcc$classes,
   max.iter=12,plot.me=F,trace=F,trim=0,df=dfcempcc)

#mafm
write.table(sine.data, paste(filename, "-cempcc-in.x", sep=""),row.names=F,col.names=F)
write.table(curve1.hpcc$classes, paste(filename, "-cempcc-in.initclust", sep=""),row.names=F,col.names=F)
write.table(df.cempcc, paste(filename, "-cempcc-in.df", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$var.bound,file=paste(filename, "-cempcc-out.cem_varbound", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$iterations,file=paste(filename, "-cempcc-out.cem_iterations", sep=""),row.names=F,col.names=F)

write.table(curve1.cem$clust,file=paste(filename, "-cempcc-out.cem_clustering", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$clust.best,file=paste(filename, "-cempcc-out.cem_bestclustering", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$clust.log,file=paste(filename, "-cempcc-out.cem_clusteringlog", sep=""),row.names=F,col.names=F)

write.table(curve1.cem$like.about,file=paste(filename, "-cempcc-out.cem_likelihoodsabout", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$like.along,file=paste(filename, "-cempcc-out.cem_likelihoodsalong", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$likelihoods,file=paste(filename, "-cempcc-out.cem_likelihoods", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$likelihoods.mix,file=paste(filename, "-cempcc-out.cem_likelihoodsmix", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$overall.loglikelihood,file=paste(filename, "-cempcc-out.cem_ovloglike", sep=""),row.names=F,col.names=F)

write.table(curve1.cem$all.curvevars,file=paste(filename, "-cempcc-out.cem_allcurvevars", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.curvelengths,file=paste(filename, "-cempcc-out.cem_allcurvelengths", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.meanalong,file=paste(filename, "-cempcc-out.cem_allmeanalong", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.varalong,file=paste(filename, "-cempcc-out.cem_allvaralong", sep=""),row.names=F,col.names=F)
write.table(curve1.cem$all.mixprop,file=paste(filename, "-cempcc-out.cem_allmixprop", sep=""),row.names=F,col.names=F)
#/mafm

#compute BIC value
#mafm
#calc.likelihood(curve1.cem,mix.best=T)$bic
calc<-calc.likelihood(curve1.cem,mix.best=T)
calc.score.filename<-paste(filename, "-cempcc-out.calc_score", sep="")
calc.bic.filename<-paste(filename, "-cempcc-out.calc_bic", sep="")
calc.bic2.filename<-paste(filename, "-cempcc-out.calc_bic2", sep="")
write.table(calc$score,file=calc.score.filename,row.names=F,col.names=F)
write.table(calc$bic,file=calc.bic.filename,row.names=F,col.names=F)
write.table(calc$bic2,file=calc.bic2.filename,row.names=F,col.names=F)
#/mafm

#print(curve1.hpcc$classes)
#print(curve1.hpcc$totalvar.track)
#clust.init.filename<-paste(filename, "-initclust", sep="")
#clust.final.filename<-paste(filename, "-finalclust", sep="")
#write.table(curve1.kmeans$cluster,file=clust.init.filename,row.names=F,col.names=F)
#write.table(curve1.hpcc$classes,file=clust.final.filename,row.names=F,col.names=F)


}