library(princurve)

generate.princurve <- function(filename, degreesOfFreedom) {
    x=as.matrix(read.table(filename))

    pcurve.result=principal.curve(x, df=degreesOfFreedom)

    # arrays in Java start in 0, not in 1
    pcurve.result$tag = pcurve.result$tag-1

    write.table(degreesOfFreedom, paste(filename, "-in.df", sep=""), row.names=F, col.names=F)
    write.table(pcurve.result$s, paste(filename, "-out.s", sep=""), row.names=F, col.names=F)
    write.table(pcurve.result$lambda, paste(filename, "-out.lambda", sep=""), row.names=F, col.names=F)
    write.table(pcurve.result$tag, paste(filename, "-out.tag", sep=""), row.names=F, col.names=F)
    write.table(pcurve.result$dist, paste(filename, "-out.dist", sep=""), row.names=F, col.names=F)
}

generate.smaller.data <- function(filename, reduction) {
    x=as.matrix(read.table(filename))

    filter=(c(1:length(x[,1])) %% reduction == 0)
    x.new=x[filter,]

    write.table(x, paste(filename, "-old", sep=""), row.names=F, col.names=F)
    write.table(x.new, filename, row.names=F, col.names=F)
}

data2d.filename="data2d"
data2d.df=4

data3d.filename="data3d"
data3d.df=2

data5d.filename="data5d"
data5d.df=7

generate.smaller.data(data3d.filename, 10)
generate.smaller.data(data5d.filename, 10)


generate.princurve(data2d.filename, data2d.df)
generate.princurve(data3d.filename, data3d.df)
generate.princurve(data5d.filename, data5d.df)
